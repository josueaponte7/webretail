from django.conf.urls import include, url
from django.contrib import admin
import settings
from django.views.generic import TemplateView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^negocio/', include('apps.negocio.urls', namespace='negocio')),
    url(r'^fidelizacion/', include('apps.fidelizacion.urls')),
    url(r'^local/', include('apps.local.urls',)),
    url(r'^categoria/', include('apps.categoria.urls')),
    url(r'^coleccion/', include('apps.coleccion.urls')),
    url(r'^banner/', include('apps.banner.urls')),
    url(r'^productos/', include('apps.productos.urls')),
    url(r'^$', TemplateView.as_view(template_name="index.html")),
]
