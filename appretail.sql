--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.9
-- Dumped by pg_dump version 9.4.9
-- Started on 2016-12-13 07:05:47 VET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1 (class 3079 OID 11861)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2279 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 180 (class 1259 OID 226109)
-- Name: auth_group; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


--
-- TOC entry 179 (class 1259 OID 226107)
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2280 (class 0 OID 0)
-- Dependencies: 179
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- TOC entry 182 (class 1259 OID 226119)
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


--
-- TOC entry 181 (class 1259 OID 226117)
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2281 (class 0 OID 0)
-- Dependencies: 181
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- TOC entry 178 (class 1259 OID 226101)
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


--
-- TOC entry 177 (class 1259 OID 226099)
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2282 (class 0 OID 0)
-- Dependencies: 177
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- TOC entry 184 (class 1259 OID 226127)
-- Name: auth_user; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


--
-- TOC entry 186 (class 1259 OID 226137)
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


--
-- TOC entry 185 (class 1259 OID 226135)
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2283 (class 0 OID 0)
-- Dependencies: 185
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- TOC entry 183 (class 1259 OID 226125)
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2284 (class 0 OID 0)
-- Dependencies: 183
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- TOC entry 188 (class 1259 OID 226145)
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


--
-- TOC entry 187 (class 1259 OID 226143)
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2285 (class 0 OID 0)
-- Dependencies: 187
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- TOC entry 197 (class 1259 OID 237048)
-- Name: banner_banner; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE banner_banner (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    descripcion text,
    status boolean NOT NULL,
    img_ban character varying(100) NOT NULL,
    img_ban_bin text,
    date_create timestamp with time zone,
    date_update timestamp with time zone,
    negocio_id integer
);


--
-- TOC entry 196 (class 1259 OID 237046)
-- Name: banner_banner_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE banner_banner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2286 (class 0 OID 0)
-- Dependencies: 196
-- Name: banner_banner_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE banner_banner_id_seq OWNED BY banner_banner.id;


--
-- TOC entry 199 (class 1259 OID 237065)
-- Name: categoria_categoria; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE categoria_categoria (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    descripcion text,
    orden integer,
    status boolean NOT NULL,
    img_cat character varying(100) NOT NULL,
    img_cat_bin text,
    date_create timestamp with time zone,
    date_update timestamp with time zone,
    negocio_id integer
);


--
-- TOC entry 198 (class 1259 OID 237063)
-- Name: categoria_categoria_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE categoria_categoria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2287 (class 0 OID 0)
-- Dependencies: 198
-- Name: categoria_categoria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE categoria_categoria_id_seq OWNED BY categoria_categoria.id;


--
-- TOC entry 201 (class 1259 OID 237082)
-- Name: ciudad_ciudad; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE ciudad_ciudad (
    id integer NOT NULL,
    ciudad text NOT NULL
);


--
-- TOC entry 200 (class 1259 OID 237080)
-- Name: ciudad_ciudad_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ciudad_ciudad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2288 (class 0 OID 0)
-- Dependencies: 200
-- Name: ciudad_ciudad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ciudad_ciudad_id_seq OWNED BY ciudad_ciudad.id;


--
-- TOC entry 203 (class 1259 OID 237093)
-- Name: coleccion_coleccion; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE coleccion_coleccion (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    descripcion text,
    orden integer,
    status boolean NOT NULL,
    img_col character varying(100) NOT NULL,
    img_col_bin text,
    date_create timestamp with time zone,
    date_update timestamp with time zone,
    negocio_id integer
);


--
-- TOC entry 202 (class 1259 OID 237091)
-- Name: coleccion_coleccion_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE coleccion_coleccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2289 (class 0 OID 0)
-- Dependencies: 202
-- Name: coleccion_coleccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE coleccion_coleccion_id_seq OWNED BY coleccion_coleccion.id;


--
-- TOC entry 190 (class 1259 OID 226205)
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


--
-- TOC entry 189 (class 1259 OID 226203)
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2290 (class 0 OID 0)
-- Dependencies: 189
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- TOC entry 176 (class 1259 OID 226091)
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


--
-- TOC entry 175 (class 1259 OID 226089)
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2291 (class 0 OID 0)
-- Dependencies: 175
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- TOC entry 174 (class 1259 OID 226080)
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


--
-- TOC entry 173 (class 1259 OID 226078)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2292 (class 0 OID 0)
-- Dependencies: 173
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- TOC entry 191 (class 1259 OID 226234)
-- Name: django_session; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


--
-- TOC entry 211 (class 1259 OID 237247)
-- Name: fidelizacion_fidelizacion; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE fidelizacion_fidelizacion (
    id integer NOT NULL,
    moneda double precision,
    puntos integer NOT NULL,
    date_create timestamp with time zone,
    date_update timestamp with time zone,
    negocio_id integer
);


--
-- TOC entry 210 (class 1259 OID 237245)
-- Name: fidelizacion_fidelizacion_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fidelizacion_fidelizacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2293 (class 0 OID 0)
-- Dependencies: 210
-- Name: fidelizacion_fidelizacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE fidelizacion_fidelizacion_id_seq OWNED BY fidelizacion_fidelizacion.id;


--
-- TOC entry 205 (class 1259 OID 237110)
-- Name: local_local; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE local_local (
    id integer NOT NULL,
    nombre character varying(250) NOT NULL,
    correo character varying(254) NOT NULL,
    direccion text,
    date_create timestamp with time zone,
    date_update timestamp with time zone,
    ciudad_id integer,
    negocio_id integer
);


--
-- TOC entry 204 (class 1259 OID 237108)
-- Name: local_local_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE local_local_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2294 (class 0 OID 0)
-- Dependencies: 204
-- Name: local_local_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE local_local_id_seq OWNED BY local_local.id;


--
-- TOC entry 195 (class 1259 OID 237031)
-- Name: negocio_negocio; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE negocio_negocio (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    pagina_facebook character varying(200) NOT NULL,
    status boolean NOT NULL,
    logo character varying(100) NOT NULL,
    logo_ba text,
    date_create timestamp with time zone NOT NULL,
    date_update timestamp with time zone NOT NULL,
    tipo_negocio_id integer NOT NULL
);


--
-- TOC entry 194 (class 1259 OID 237029)
-- Name: negocio_negocio_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE negocio_negocio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2295 (class 0 OID 0)
-- Dependencies: 194
-- Name: negocio_negocio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE negocio_negocio_id_seq OWNED BY negocio_negocio.id;


--
-- TOC entry 207 (class 1259 OID 237133)
-- Name: productos_productos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE productos_productos (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    descripcion text,
    orden integer,
    status boolean NOT NULL,
    destacado boolean NOT NULL,
    precio_real double precision,
    precio_lista double precision,
    fidelizacion boolean NOT NULL,
    img_pro character varying(100) NOT NULL,
    img_pro_bin text,
    date_create timestamp with time zone,
    date_update timestamp with time zone,
    categoria_id integer,
    coleccion_id integer,
    monto double precision,
    puntos integer NOT NULL
);


--
-- TOC entry 206 (class 1259 OID 237131)
-- Name: productos_productos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE productos_productos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2296 (class 0 OID 0)
-- Dependencies: 206
-- Name: productos_productos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE productos_productos_id_seq OWNED BY productos_productos.id;


--
-- TOC entry 209 (class 1259 OID 237156)
-- Name: telefono_local_localtelefono; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE telefono_local_localtelefono (
    id integer NOT NULL,
    telefono character varying(100) NOT NULL,
    date_create timestamp with time zone,
    date_update timestamp with time zone,
    local_id integer
);


--
-- TOC entry 208 (class 1259 OID 237154)
-- Name: telefono_local_localtelefono_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE telefono_local_localtelefono_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2297 (class 0 OID 0)
-- Dependencies: 208
-- Name: telefono_local_localtelefono_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE telefono_local_localtelefono_id_seq OWNED BY telefono_local_localtelefono.id;


--
-- TOC entry 193 (class 1259 OID 237020)
-- Name: tipo_negocio_tiponegocio; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tipo_negocio_tiponegocio (
    id integer NOT NULL,
    tipo_negocio text NOT NULL
);


--
-- TOC entry 192 (class 1259 OID 237018)
-- Name: tipo_negocio_tiponegocio_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tipo_negocio_tiponegocio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2298 (class 0 OID 0)
-- Dependencies: 192
-- Name: tipo_negocio_tiponegocio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tipo_negocio_tiponegocio_id_seq OWNED BY tipo_negocio_tiponegocio.id;


--
-- TOC entry 2012 (class 2604 OID 226112)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- TOC entry 2013 (class 2604 OID 226122)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- TOC entry 2011 (class 2604 OID 226104)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- TOC entry 2014 (class 2604 OID 226130)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- TOC entry 2015 (class 2604 OID 226140)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- TOC entry 2016 (class 2604 OID 226148)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- TOC entry 2021 (class 2604 OID 237051)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY banner_banner ALTER COLUMN id SET DEFAULT nextval('banner_banner_id_seq'::regclass);


--
-- TOC entry 2022 (class 2604 OID 237068)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY categoria_categoria ALTER COLUMN id SET DEFAULT nextval('categoria_categoria_id_seq'::regclass);


--
-- TOC entry 2023 (class 2604 OID 237085)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ciudad_ciudad ALTER COLUMN id SET DEFAULT nextval('ciudad_ciudad_id_seq'::regclass);


--
-- TOC entry 2024 (class 2604 OID 237096)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY coleccion_coleccion ALTER COLUMN id SET DEFAULT nextval('coleccion_coleccion_id_seq'::regclass);


--
-- TOC entry 2017 (class 2604 OID 226208)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- TOC entry 2010 (class 2604 OID 226094)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- TOC entry 2009 (class 2604 OID 226083)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- TOC entry 2028 (class 2604 OID 237250)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY fidelizacion_fidelizacion ALTER COLUMN id SET DEFAULT nextval('fidelizacion_fidelizacion_id_seq'::regclass);


--
-- TOC entry 2025 (class 2604 OID 237113)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY local_local ALTER COLUMN id SET DEFAULT nextval('local_local_id_seq'::regclass);


--
-- TOC entry 2020 (class 2604 OID 237034)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY negocio_negocio ALTER COLUMN id SET DEFAULT nextval('negocio_negocio_id_seq'::regclass);


--
-- TOC entry 2026 (class 2604 OID 237136)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY productos_productos ALTER COLUMN id SET DEFAULT nextval('productos_productos_id_seq'::regclass);


--
-- TOC entry 2027 (class 2604 OID 237159)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY telefono_local_localtelefono ALTER COLUMN id SET DEFAULT nextval('telefono_local_localtelefono_id_seq'::regclass);


--
-- TOC entry 2019 (class 2604 OID 237023)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tipo_negocio_tiponegocio ALTER COLUMN id SET DEFAULT nextval('tipo_negocio_tiponegocio_id_seq'::regclass);


--
-- TOC entry 2241 (class 0 OID 226109)
-- Dependencies: 180
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2299 (class 0 OID 0)
-- Dependencies: 179
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- TOC entry 2243 (class 0 OID 226119)
-- Dependencies: 182
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2300 (class 0 OID 0)
-- Dependencies: 181
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- TOC entry 2239 (class 0 OID 226101)
-- Dependencies: 178
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (4, 'Can add permission', 2, 'add_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (5, 'Can change permission', 2, 'change_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (6, 'Can delete permission', 2, 'delete_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (7, 'Can add user', 3, 'add_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (8, 'Can change user', 3, 'change_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (9, 'Can delete user', 3, 'delete_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (10, 'Can add group', 4, 'add_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (11, 'Can change group', 4, 'change_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (12, 'Can delete group', 4, 'delete_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (13, 'Can add content type', 5, 'add_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (14, 'Can change content type', 5, 'change_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (15, 'Can delete content type', 5, 'delete_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (16, 'Can add session', 6, 'add_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (17, 'Can change session', 6, 'change_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (18, 'Can delete session', 6, 'delete_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (19, 'Can add categories', 7, 'add_categories');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (20, 'Can change categories', 7, 'change_categories');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (21, 'Can delete categories', 7, 'delete_categories');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (22, 'Can add tienda', 8, 'add_tienda');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (23, 'Can change tienda', 8, 'change_tienda');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (24, 'Can delete tienda', 8, 'delete_tienda');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (25, 'Can add direccion', 9, 'add_direccion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (26, 'Can change direccion', 9, 'change_direccion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (27, 'Can delete direccion', 9, 'delete_direccion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (28, 'Can add categoria', 10, 'add_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (29, 'Can change categoria', 10, 'change_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (30, 'Can delete categoria', 10, 'delete_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (31, 'Can add ciudad', 11, 'add_ciudad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (32, 'Can change ciudad', 11, 'change_ciudad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (33, 'Can delete ciudad', 11, 'delete_ciudad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (34, 'Can add tipo negocio', 12, 'add_tiponegocio');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (35, 'Can change tipo negocio', 12, 'change_tiponegocio');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (36, 'Can delete tipo negocio', 12, 'delete_tiponegocio');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (37, 'Can add categoria', 13, 'add_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (38, 'Can change categoria', 13, 'change_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (39, 'Can delete categoria', 13, 'delete_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (40, 'Can add coleccion', 13, 'add_coleccion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (41, 'Can change coleccion', 13, 'change_coleccion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (42, 'Can delete coleccion', 13, 'delete_coleccion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (43, 'Can add banner', 14, 'add_banner');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (44, 'Can change banner', 14, 'change_banner');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (45, 'Can delete banner', 14, 'delete_banner');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (46, 'Can add categoria', 15, 'add_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (47, 'Can change categoria', 15, 'change_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (48, 'Can delete categoria', 15, 'delete_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (49, 'Can add productos', 15, 'add_productos');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (50, 'Can change productos', 15, 'change_productos');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (51, 'Can delete productos', 15, 'delete_productos');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (52, 'Can add local', 16, 'add_local');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (53, 'Can change local', 16, 'change_local');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (54, 'Can delete local', 16, 'delete_local');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (55, 'Can add local telefono', 17, 'add_localtelefono');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (56, 'Can change local telefono', 17, 'change_localtelefono');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (57, 'Can delete local telefono', 17, 'delete_localtelefono');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (58, 'Can add cors model', 18, 'add_corsmodel');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (59, 'Can change cors model', 18, 'change_corsmodel');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (60, 'Can delete cors model', 18, 'delete_corsmodel');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (61, 'Can add negocio', 19, 'add_negocio');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (62, 'Can change negocio', 19, 'change_negocio');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (63, 'Can delete negocio', 19, 'delete_negocio');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (64, 'Can add fidelizacion', 20, 'add_fidelizacion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (65, 'Can change fidelizacion', 20, 'change_fidelizacion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (66, 'Can delete fidelizacion', 20, 'delete_fidelizacion');


--
-- TOC entry 2301 (class 0 OID 0)
-- Dependencies: 177
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_permission_id_seq', 66, true);


--
-- TOC entry 2245 (class 0 OID 226127)
-- Dependencies: 184
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (1, 'pbkdf2_sha256$30000$8LQ3YzQPChnS$RGaa2op3LzD8BH5ArSAeKc81/FuzYxPd0WoGivLgIbY=', '2016-11-27 07:28:54.111921-04', true, 'administrador', '', '', '', true, true, '2016-11-27 07:28:07.38805-04');


--
-- TOC entry 2247 (class 0 OID 226137)
-- Dependencies: 186
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2302 (class 0 OID 0)
-- Dependencies: 185
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- TOC entry 2303 (class 0 OID 0)
-- Dependencies: 183
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_user_id_seq', 1, true);


--
-- TOC entry 2249 (class 0 OID 226145)
-- Dependencies: 188
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2304 (class 0 OID 0)
-- Dependencies: 187
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- TOC entry 2258 (class 0 OID 237048)
-- Dependencies: 197
-- Data for Name: banner_banner; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2305 (class 0 OID 0)
-- Dependencies: 196
-- Name: banner_banner_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('banner_banner_id_seq', 1, false);


--
-- TOC entry 2260 (class 0 OID 237065)
-- Dependencies: 199
-- Data for Name: categoria_categoria; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO categoria_categoria (id, nombre, descripcion, orden, status, img_cat, img_cat_bin, date_create, date_update, negocio_id) VALUES (2, 'Categoria 1', 'hjjgjhgjg', NULL, true, 'categoria/22cf7f54435c787087bb3061617b7852.jpg', '/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0a
HBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIy
MjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAHQAc4DASIA
AhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQA
AAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3
ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWm
p6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEA
AwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSEx
BhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElK
U1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3
uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3aGJG
jViuSc9z61J5EX9z9TRb/wCoX8f51T1PU00yON3jZw7YwpxihsRb8iP+5+po8iP+5+prBPi6AD/j
1l/76FKPFsBH/HtJ+YpXQuZG95EX9z9TR5EX9z9TWCPFkGf+PaT/AL6FOHiqA/8ALtJ/30KLoOZG
55EX9z9TR5EX9z9TWH/wlUPa2k/76FOHiiE/8u0n/fQougujZ8iL+5+po8iL+5+prH/4SaH/AJ93
/wC+hS/8JLF/z7v/AN9Ci6HdGx5EX9z9TR5EX9z9TWR/wkcX/PB/zFH/AAkUX/PB/wAxS5kLmRr+
RF/c/U0eRF/c/U1lf8JDF/zwf8xSjX4v+eD/AJii6DmRqeRF/c/U0eRF/c/U1l/29H/zwf8AMUv9
vR/88H/MUcyDmRp+RF/c/U0eRF/c/U1mHXoh/wAsX/MUDXom6Qv+Yp8yDmj3NPyIv7n6mjyIv7n6
msttfjX/AJYP+Ypy64jD/UN+Youg5kaXkRf3P1NHkRf3P1NZ/wDbSH/li/5il/tlP+eLfmKLoOZF
/wAiL+5+po8iL+5+pqh/bCf88m/MUv8Aa6f88m/MUuZBdF3yIv7n6mjyIv7n6mqJ1hR/yxb8xQNY
Vv8Ali35ii6DmiX/ACIv7n6mjyIv7n6ms86zGDjyX/MU5dYjbpE35inzIOaPcveRF/c/U0eRF/c/
U1ROrxjrG350DVkP/LJvzpXQcyL3kRf3P1NHkRf3P1NUv7WT/nk350h1dB/yyb8xRzILoveRF/c/
U0eRF/c/U1RXWEP/ACyb8xS/2un/ADyb8xRzLuCkmXfIi/ufqaPIi/ufqapf2un/ADyb8xSf2xH/
AM8m/MUcy7jui95EX9z9TR5EX9z9TVH+2I/+eTfmKT+2Y/8Ani/5ijmXcLov+RF/c/U0eRF/c/U1
Q/tlP+eL/mKP7Zjx/qX/ADFHMu4XRf8AIi/ufqaPIi/ufqaz/wC2ov8Ani35il/tmP8A54t+Yo5l
3GX/ACIv7n6mjyIv7n6ms863GP8Ali35ik/tyM/8sW/MUcy7gaPkRf3P1NHkRf3P1NZ39ux/88H/
ADFIdej/AOeD/mKOZdwsaXkRf3P1NHkRf3P1NZh8QRf88H/76FJ/wkMX/Pu/5ijmXcLGp5EX9z9T
R5EX9z9TWSfEUQH/AB7v/wB9Ck/4SSL/AJ93/wC+hRzLuFjX8iL+5+po8iL+5+prH/4SWH/n3k/7
6FIfE0P/AD7yf99CjmXcLGz5EX9z9TR5EX9z9TWN/wAJRD/z7Sf99CmHxVAP+XaT/voUcy7hY3PI
i/ufqaPIi/ufqaw/+Eqg/wCfaT/voUf8JVB/z7Sf99CjmXcDc8iL+5+po8iL+5+pqHT71b+1E6IV
BYjBOelW6pMCGD/UL+P86wvFv/Hrbf75/lW7B/qF/H+dYPi4Ztbf/fP8qUtiZfCcqCDQBTEUgmpg
tZma1GBST7VMFpAKeBQwSsKFp4FNAJ+lPC0hpgBTwtAWlH3sUA2KFp23inBacFoAjVTu9qlApQKQ
N82MU9ydEBwvWhWBOMU5k3LTUiOc0JKxLbuK6/LSRgg1MF45p20dqE9LA1rcaVB7UoFOC0H5RnFA
X6hilxgGmh+eRUhXcpxRbuCknsRhst7VMFyKiSNmbAzVkRlVFErII3YwLQFp+KcEJ6CpKSvsVzFk
08LgAYqwIueacUGDwKXMUqb3KLqdxzT0X5amK+tKEPYVV9CFGzITjpQVB+lP8lieelO8sqPaloNJ
vdEQUAUbal28UbaRSIsUhWlfIOKQcY9KdiebWwhWkxUu3ioyecUJFNpDcUbfapQmRk0uzFTcuMWy
AjHWkx6GpXXKnjmmKhoB3TsRlSaTbUxXHFIVouWkRFaYVqYrTSKBkJWmlalIppWgCBl5pCtSlRnr
TSBQIhK0wrUxWmlaBkJFRsKnK1EVpoTI+9B607FIRQJLQ7Dw3/yCF/32rYrI8Of8glf99q162jsB
DB/qF/H+dYXi3/j2tv8AfP8AKt2D/UL+P86xPFYzbW/++f5U5bEy+E5ULTwKQCngVmQhAnNSBaQL
z1qQLQJABTgvNKBSgEnihA2KFpwUZpVFPApAAFOAoC08CgBoHNKU3fWgqQQRUgGRTJvfcRVwKcBQ
QccClQHHPWkK/QAvFL1FOxSpGST60wb1shoFOC5HtUwgPFSqgAHFS5JFqDe5RMRzxVqKIBRkc0kp
CsMCnxPleacm2rigoqVmPCBeRSHp7VKMH0pki8YFQt9TWTSWhAuGJ4qwgytMVAo6c04ZU02rmcZW
3H7aGG1SaA4NJJ8wIHFJLU0lNW0IgMmpgoxUSKQOQamU4GDRJEQkuom0UjL8pqQAE5pj88dqEtTS
T0IcUEVIF5oMYNMz16EJj3CoyhDYxVxVxxTXUZHFCl0CUFa5XK4FRbcnOKsleKQqBTTE1caq/KKC
tLnb3o3e1S0aRkthjDim7eKkxlqCtBS1dyLbz0pCvpUpWmGgbaREV4qNh+VTnpUZWhCkyErzSGpc
UwrQTfQiK0wrUxFMIpgR00ipCKaRSZaZERTCtTEUwigZCVphFTEUwigDrPDn/IJX/fatfvWR4d/5
BS/77Vr963WyJZFB/qV/H+dYfir/AI9rf/fP8q3IP9Sv4/zrE8U/8e1v/vn+VN7Ey2OYFPAoAp4F
ZmYBTnrTwOKAKeBQxABTwKAKeBSAAKeFoAp4FADCpyCKkA46UoFOA5oEIFp2KUCl46UCYgANPApB
Gd2RUqoSelDsCuxgFSRABqf5OFzUJ3K/ehag7xd2XAtLt4qNXIAzUhfK9KjlZsqisVZVLv7U9V2r
UqoWqVYeOaty6GMYtu6KYLhuCfpVhVdgCanEK+gzUgXAHFTKSexpGk1uyARGnCLjmpgtAHJpXZpy
RRCsAUml8tc1MFoCc0rhypbIh8oUhi4qY8ClAyKLsOWOxX8sgetIVIq0VpCtO4nBFXGO1LtzU5jB
phQr0ovcXK0N2YqNxk0rueRSICRQl1Jc09EN21G6t2qxjHanBQe1F7DUb6FIxmhR2Iq2UqLZ81O9
xOLTGKvFBWpdvFIVqWzaKsiB1O04qMKcc1ZK1Gy+lNMmS6kJHpTCtTFaaVpkkJWm7c/SpStG3ikx
xVyErimFamIphFBoQlR6U0rUpFNIoAhIphFSkUxqAuREVGRUpU00qaLCudR4fH/EqX/fatasrQON
LX/fatWt1shEVv8A6lfx/nWL4o/1Fv8A75/lW1b/AOpX8f51jeJv+PeD/fP8qb2JlscyAdwqYCmg
D8aeBWZiOA9acBxSAU/bkYoGyIsc8dKmiO4c1EUKnpxU0S8U2lYzTdyXgCgMCcUx+uKEHNCjoEpa
2LAFOAoUcCngUrFXG9KUKGp23P0pwXHTpRcRNCvy+pqULjtUKNt+lTqwOKzaNoyVrAcBTnpUO0Mc
1NL8y4HWkjiOPaqWiIk23ZDCjMMCpoosDnrUqqBwBUgX2qXLoVGCTuxoUDpTwtAU5z2pxZR1pWNV
JCBadimeYO1Bc44FHKyXUiPC0BcdqYjnnIqUMDQ00OM0xMUuKXHOaWkUmMKA9aNvFKzYo3CnZk8y
uMw27pxTiPanAg+lIWwelAXshpFIRxShuadtBFGwKVyqUDduaAhVfarW32ppWnzE8i3KhODilHsa
meIHtUQTaTT0ZNpJgelMC/NUhFAWlsir3YwimMD2qYrTStJFtkW3j3phXipiKYeaBpohZcUwipyu
ajK4ppkSViEikI4qQikIoY0REUwipiKYRSLISKYRUxFMIpgQMOKZtqZxzTSKBdSEimEVMRTCKBnS
aFxpi/75rU71maH/AMgxf981p963WyJZDB/qF/H+dY/iX/j3g/3z/KtiD/UL+P8AOsnxJ/qIP98/
ypvYmXwnOAVIBTQKkArMwGlto9aVHPfpSOvzZp6xk44qklYlt3JQAwp6rjpTUXaKlAqSkNKbm56V
IqAdqAMDOM03cewppNktpEwFPVS3AqBGIarcXUHtSkmhxabFERqdEAUZFPC8UoXms7m6ikROgB4F
IBz70+TlgB0p8ceTkiqvpqZNXlZAkeSCanCjFOC46CgZ3VLdzaKSQBcGnEhaGbaKhYkgmhK5Mppa
IcWJ6U0ruBHempu5zUuw4zV2sY3chsaEDFTCMYojHFSVMpGsIK2ozYKaUx0qWg8UrstxRGpOcGnk
1GTycUmSTRa5ClbQVgcmkpQaD+lBL11EyR0pSc0lFMEwqRelRHkGnxggc0nsVF62JKKKTNSahtqN
kzUmaM0xOzK5XB6UqjipDtagLgU2yYrXQhfIHFRhie1WStRMmO3FCsOV73RCeTSAVIVoC0MUbtkR
FRstSSMFqIOWYjFFuo3JJ2GNhe9NDBs4pJo23ZGcURJtXmnZWIUnzWsIzKvemAhhkUskJJJByKUJ
tXFJpWLUpNkbEDqaaRxTHV92DmpDhVGaGrBGTbd0QutNI4p7sM4pCOKLFJpvQiNMIqQimkUDTOh0
T/kGr/vmtPvWbov/ACDl/wB81pd63WyEQwf6hfx/nWV4i/1EH++f5Vqwf6hfx/nWV4i5gg/3j/Km
9iZfCc+BUgFNH15qQCszAAKfkL1oApj/AHqcVcUnYer5YDtU4FQRL81WCPloa1Ji3a4q4bpSmLPI
61CuQRVtDuXpQ1bUE76MjSLnmrAXGKAKlVC3ak3fcqMew5HIHPSpN421EVIPIqRF3MAATU2RSlJa
Com9snpVlV59qVIyo+6akCn0qXdm0Y2Q0ClOFGacVOKZIpxgAmhRY22kQs2W5NFI0ZbsR+FOCMAB
gn8K0sctpN6iVIjgDBpu1v7p/Kja390/lS5Wy48yehOCKWoAWB6H8qmUkjkGpcWjaMri0xzgU/n0
NMkUnHBpJO45baEVLRg+h/Knqh7g1TRgotsaAaMEdal247UbcjpS17GnIRUU4qQeAaTaff8AKizI
5WhBUg6VHj2/SpADjpQ0y4gTgVEHbceDipSpI4FM2n0P5UJClzX0DOTTskr703afQ08A9MGhpjjc
Zg596eBxS7fajB9KGmUo2GDnNIRmpNvtSHPoaVmHqQuAASarPLzgVbOScbTj6VE0GDnafyppd0RK
7+EjKB15FRbUj6mnPJ2HSoDlj3pqL6kSmum4SS9cVAXY1ZEGetHlKvYU7pCtJ6lQu/qaaXYd6ulF
x0FQSRDBIFCkinGSV7kHmnPIoLK4x0NOCK3fmo3jKg4osgUpJEYjO7OeKVmUAg9aaGKnrxSOm85B
5oa11HGVl7ogO4GgjinKu1eetJkHgHmoZrF6anQaN/yD1/3zWjWfo/8Ax4L/AL5rQrdbIZDB/qV/
H+dZfiEgQQZ/vH+VakH+pX8f51l+IV3QQ/7x/lTlsTL4WYG35gQalApqKR1qQCoMEOA/Ol2AnkUA
c1IBSC1wRApGOtSFNy4pAKkAouKyIhERVmFOAKAKkT5Wocm0EYpO5MIhUqLtAFCYIqQdKybZ0JRW
qIJlO4YFTW6YcNTD87Yq1GuMVadrIiMbybJ6KKK3NQooooAKKKKACiiigAooooAKKKKACiiigAoo
ooAKKKKACiiigAooooAKKKKACiiigAooooAKD0ooPSgDGK806NQFLGpzB8uSeaBF8uCeKyckzljB
32KjuSeOlNUs7D0qaWHaMjpUKPsJyKLJrQLtS94lPpVd2wxBp7SZORULEsSalR7lyqdEREc5pytu
G080hpg4celU1ciMmmRyoVbimKSp9qtTAbfeqxHtQtUOStLQSVWIBB4qONSCSc1NG3ODRIyj61N+
hokn71ze0j/jwH++a0azdGO6wU/7RrSrVbGid9SG3/1Kfj/OszXxmGD/AHj/ACrTt/8AUp+P86zd
e/1EP+8f5U5bCl8LMQCnjgUwg44609fmHPWs7HPfoO57CpF6dMU1RxUoFACgVIBSAU7bkjmgGPAp
4FNQdAKmCN6cUgEGcggmpg5xilWHIHNIV2nFK6Y7SSJIVy2atKOlRQrhc1MOtSn7yN4K0R9FFFdJ
QUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAF
dgMVA8oU4FWG+6aouPmNc8VfcwqScdhXlDKRjmqpXJ6VM3SljUYLEVdrIyu5PUg8tsdKcsOAc9ac
8pzxT0fePelJuxcFC9irLFtBIquR+daD4xyOKqy7e1EWxzilsQFjkZ6USoCoYUoXcRT3XEeMUPRh
FXTuVBw2ac8YYZHWkNODER8US8hwa1TN7R122Cj3NX6z9HJNiM9dxrQrSOxsttCKD/Ur+P8AOs7X
f9TD/vH+VaNv/qU/H+dZ2ukCGHP94/ypy2FL4WYwFPAxTV5p4rMwHgU8CmgUhYhhQlcTdicCngGk
XoKeBSAkhUBhnrVsCqSht2QcVbRxjnrUyXU0pySVmSgGmuvzU7eqjJNMDh2GKUUxzktiwowBTweR
TRwKUD5s04/EaLRElFFFdAwooooAKK5PxDr9xF4h0nw3p0yxX2ol5JJioYwQqCSQp4LEgqMggYJI
NXZ9Ha1ie5Gv6jA0aFnnmmVowBySysu0D1wBx0IoA36Kx9F1Ke8+0W14sYu7ZgHaL/VyqwysiZJI
VhngkkEEZIwTsUAFFFFABRRWR4h1VtF0S4u4oTPcjEdvAOssrEKi/ixGfbJoA16Ky9CTVo9IhGtz
wS37DdKYE2opP8I5OcdM961KACivK/HvijxN4c8XaNZWt/EbLUplUILddygOqsNzZzww5wOteqUA
FFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFB6UUHpQBWjYMlVpRtc0RNtbFPnHeskrM5ZO8fQrsKcg
yhBpCKkRAV96ctiY6shaIhc5pkZCk5qy6nYapspU8iktSpLlaaB3wSAaiCM/NOKkninq5xjHND0W
gRd3qMCBfrTZOhHapsHOTUZ+Ymsze2lkUSKfGPlPpQ6FSaFU7DjrVvYyhpI3tLGLIY/vGrtUNIBF
kM/3jV+tI7G62IoP9Sv4/wA6z9cUNDDnsx/lWhB/qV/H+dUNb/1MX+8f5U5bCl8LMUDipFHAqMOA
wFTKKg57j1FPCg9aaoqRaVwHKKeCPXmkFNdCWyBQlcTdtidcHpUgFQwROTntVtIjxmlKyHFN9BoX
IwadGgVhipxGNuKjxtYCkpXG4NNNkpOMU9c5FJjinDrUx3R0JD6KKK6RhRRRQB4z8XtL1vSvEWne
NdG8z/RIhFIyLu8khmIZh/dYMVPb161Dpfxo0vW9Ok0vxTZPaidDG9zaksnPfH3lx143V6pqGryW
96LGz0+e+ujGJHSNlRY0JIUuzEAZKkADJODxgZrkbz4f+FPG9hJfDTX0y9LyRO8OEaOVGKsGCkq2
GB5HUd6AOr0CCw+xG+sL99RF3tdrt5A5kAGAMgAAAcYAHOc8kmsCLxpceIPGV34c8PiCNLFSbu+u
EMgDA7SiIGXJycbiccHg8Z5j4b6Zqngjx/qXhS6n8+ymtPtkDqMK2GVQwB+6SCykZ6qOowawvhA2
pL4u8QwWz28d0QWk+0ozY2yEEABhzlu9AHo+geK79vGmo+E9ZSFru3Tz7e6hUqs8ZweVJO1sHscc
H05zPGHxIu9BnstOTQr+3vL2RVjeYxMMbgG2hWbcewB29QarQ6Mz/Fy31261uA3EZNs0ENlKkTt5
TDYJGJXdg7tuSfyqD4q7B488BF8Bft3zH282KgDqYvE1/H4ghsbprPc8whmtYo23QFozIMSlsSEK
AWwqgBuvHNPRPGF7431TUYtAe3tNMsWVPtk8JleZjnG1dwCrwTk5OMcDPHSa7aJ/Y2q3EFvH9tay
lRZFQbz8pwM4yee1eUfA5tSfRtWi0+WyjKzozmeNnPKkDG1l4+U0Ad54L8XXWt3+raNqsEUOq6VL
5cpgBEcq5IDKCSR06ZPUfQdpXl/g7RltfiRqetNrMdxLfRyqYUtJY0ch13GN2+VwpXB2k4+leoUA
eT/F8JBrngu9ZZHEGoHckYBZhuibAHc/LwPeuv1HX9W0W0Gqapp9rFpqsBOIp2eaFWIAYjaFbBI3
AHgZILY55D4yTiC+8Iuxwq6gXP4FPxrsfiGY1+HuumTp9jcDnuRgfrigCXxD4mbQNNm1AaTeXtpF
D5xmtmi2Ae+XBxjByAeD7GuV0Hx1qut6EmqP9n0+G4vGhhM8JmLH5iEUKy8KqlmcnswA4qWzeaX4
AM1zkv8A2JIBn+6EYL/46Fqf4T21rd/CvSY54I5k3TFkkUMNwmcg4I69KAH678R4dC8MaXfva+dq
WpoDb2iNwx4yd2Pu8jBxk5HviPxN4l8R+C7Ox1fUmsb6ykmEV5DbwNG0O4ZDIxZt2MEcjnjpnji/
jK7wePvDcu0CFI0ZNwO3csuSOPbb0rrviBpGpa3oUWl6lqlnAk06sgtbCaWVioJOFVmJAGSeOMda
AJvG3j/UvB0UF4NDhvdNuCFiukuypDEZAZdhxkZIwTwK6tptU/sNpn+wWt+q7my7SwKAck7sKxG3
2GD6jrzWp+G18Q/CaHR4blLyZbCI206jAkkRQVIz03YxzyAxzWJ4M8SN4i8AWmh3POoC4XTJ42yG
MQGWJHUfuldcn+JaANe58dX+heFRr3iGxtYYp1H2S2t5WaWV2GVB3KAoIBJ6kDrzxUeq+IvFel+D
YvFTrp8iBI55tNWFgUibHAkLZLAMMnaB14455z9oASDS9E2g+UJ5QxA4DbV2j8t1dFrdvqWq/D5b
e51HT7azvraKNfKtJZJDuClVUKxLMemApzQBqa546h0nwLB4ptrGW8gnWNkiDbNobuxwdoB46HnA
71teHdY/t/w/Y6qLWW1+0x7xFL95Rkjr3BxkHuCDWV8P7CPSvBdnp63ovVgaVDJ5TRkHzGJUq3Kk
ZwQemK6voKACiiigAoPSig9KAMsHkVLLzGKiHLYqaU4QCs3ujjjsyEDJxUp2xqPWogcUjsWHNDVx
RlYeJgxwRxTXVGHJFREcUKjOeM4pOKWpam3o1cHZEBwOagQtuJAqc259aURhVpXSRSjJvsVHkYnF
MDlRinOpDmgRFhnNOySEnJvQgdix5p6fKvNJKu1gM0rplAB1pPYqN022belkGzBH941crP0gEWQz
/eNaFaR2Nk7q5Fb/AOpX8f51Q1r/AFUX+8f5VoQf6lfx/nWbrpIhhx13H+VN7CnpFmQEBOalJ2rw
OaYvQZ61KuDWdznt2Ei3McmpywUU1RinhQeoovqJKyHpyKkFNAwKeBxSAngkU8Z5qynNZ6RlW3Zq
5G4A5pSiuhpTn0kTk4FQZy2ac77hgVGKSQTld2RZB4pR1pqnIp460o/EbLYfRRRXSMKKKKAOY1nS
pnvbi4WybULK8iiS4to5vJlVo2LI8bFlGcnkFlIKqQeop1rqa2FpHaWPhnVk8vhYFjjUAkkkli+0
8kkncck55NdLRQBz2k6VdHWrnX9TSNLyaFbeGBG3C3hDFsFv4mYnJI4GABnGTz914HvNJ8cP4s8N
SQeZcBlvbG4YosoOCxVgDhiVB5GM85xxXoNFAHM2mi3U+ppeXFvDYwLObk20UzSmWYqV3sSAFABJ
wo5OCT2rlvHnhHxT4s8QaXdWKabbQaW5eF552JkYspyQqHA+ReOe/Nen0UAZ+mT6jNAx1KzgtZlb
aFgnMqsMD5gSqkc5GCO1cXpfgnUPB/ii91Pw4befTL8fv7CdzEyNkkFGCkEAlsA44OMng16JRQBz
mj6HLb3kd3PFFbRwrItvZwyNIsZkYM7FmA5OAAqgKoLYznjo6KKAOG8a+Az421fTmurx7axs4pSD
Cf3hlYrt4IxtAXJ79vcM1/SvEHi7SYPD97aixgMiHUL0SqyzqpziFQSwLEBvmC7cY+au8ooA5jxR
pmoT+Ep9D0K0tgJ7Y2g86UxrDGV28AKSeMgDisr4faF4j8J6Rb6LqNvYT2qyu/2m3um3RhstgoyD
dz3Dd+nFd5RQByXjrwTbeNtHS2mlNvdwMXt5wudpIwQR3BwM4weAe2KZb2XiW4trGLUbXThqFmCq
6kly5GSu0sIwqliR1UsFJ5zwBXYUUAUtNsY9M022sYmLJBGqBm6tgck+5PJ+tctongSHSPiDrPiQ
Muy8A8iMdUZuZSeO7Dj2Zq7aigDB8V+GbPxdoM2lXpKKxDxSqMtG46MAevUgjuCRx1rC0jQvENho
NroV/a6ffrZMv2S9W7kiKqp+QsqrkMo4wDgjgnkmu7ooA4vxF4W1m78GyaXourC31KW4M810xaIS
FmLMAVyUGSMAZ4ABJ5Nb3h2wvdM8P2NlqN617eQxBJbgkkufqeTgYGTycZPNa1FABRRRQAUHpRQe
lAGdEuPmNOZd5yTxTXbA2imZPrWVm9TjbS0Bl2nHUUhHFBJPWpgoMXHWqbsSlfYrkUqPtBFPMbN2
4p2wImT1qZNbFRT3GCQYOTioTMBkU1+WzTfLZue1HKupanJ6IhkO5iaUTbVwRT3RVByearNRZMm7
TuGd8me1DyAcCnxLxmkeIE5zzSbV7GkVK10bGkktZAnrk1eqnpi7bMY/vGrlarY2W2pHB/qV/H+d
Z+tf6qL/AHj/ACrQg/1K/j/OqGtf6uL/AHj/ACoewpfCZIFKilWzmo921gMVOvIrPVHNo2PGBipV
9qi2hqlQYFAyQdBT1pg7VItIBxJAyOaYXb8KlXmpVgDD5hTTS3JcXLYiibcMd6kqVIFQ5ApzKMdq
ltX0NI0nbUajcYpyNlgKjTqRU6KBikviRcLtElFFFdBqFFFFABRRSHofpQA3cPUfSk3KTwRn615x
4R8KXF0tzf6nq2pyRG4lS3gF5IoCq5AJIbJJx64x6545rwlG998TNS067uLme2iNwio07/KFfAwc
54FYuq1a63PShgIT9pyzvyK70/4J7aGU8ZGaU8151pXh678N+P4CNRubnTruGQRrNKzGNhtO05PP
AJB64Bz0yfQsheCf1rSLbvdWOOvSjTa5JcyavfYloopCcAk9qoxG8DHakDAgkEV51pmtXPjrxTe2
8NxLb6JY4DLDIVa4YkgEsvIXgnAIyMZ64HRav4eI02V9Glksr6MF4XjkOCRzhlJ2sD05B9ahTurp
aHTUw3spqnUdm7fK/f8ApnS0ZrkfAni0eKtKZpkEd9bkJcIOmT0I9AcHjsQRXXcetOMlKKkuplWp
TozdOas0J6VX+125nSH7RF5j52puGTjrgd8d6n6rjI6V534b+G66F4xm1Q3PmW0OTaKTlgWBDbvo
CQPXOeMUpSaaSV7l0adKUZOpKzS0Vr3fbyPSKKQkDqaAQRwQaswFoqMsocKSASCQCeSB1/nUgOaA
CikyPUUtABRRRQAUUUUAFB6UUHpQBnmEkk5FRspU8il8xs9alB8yPkc1lqjjsnsQjqKsIoA46VXA
+bFWSQiUS10HBbtg7Kq1VdmkbA6UrsWJJp0WNpPei1tR83M7dBnlBQSaYXU5UcGpXcFSM1SP3s0k
m9xyklpEjcEMc0zbk1PMOh702NctntTvoQleVhTlIxiq+9marTsoGDUe1eoFRc35b6Jmvpv/AB6D
/eNXKp6b/wAeg/3jVytlsjUhg/1K/j/OqGs/6qL/AHj/ACq/B/qV/H+dUdZGYYx/tH+VOWxMvhZk
gA88VIOnHWo412in79rAYNQc3QcisDk9KsDtUa8gVItJu4JWHjrUi1GDz1qQZ2nFIGODgMB3q7Gw
K1mKpZunNW1yqj1pyigpzady3THbApgkI601m3H2rNR1NpVFbQQHBzVhDkiq1TQtk4q7aomm7OxY
ooorY3CiiigAooooAgjiSGPZGioozgAYHPJ/WvHfAuW+L+tHB+9c/wDo0V7BdXMNnbPNcTJFEoyz
uwVQPcmvFPh/qdq3xR1C5MqiO9M/ksTjdukDDr3IBrnrNc0V5nr5dGToYhpX939T1vxDfjSdCvNS
EavJawvJGCP4scfT0rz/AEKOx1vwLeanrdyk2p3PmuJ5WHmRYJC7M/dAK5AGBXoeu6eutaDfaerh
ftELRq3UAkcH86898GyLp3hjUNDvlt7HWLVnKC7wuQeQwJBBGc88jgHnNOd+dJ7Wf3k4XkWFk18S
kvXl/wAr7/I6f4caxNrXg62muZGluImaGR25LEHgk9zjHNb+sPJHot60XDrA5XHrg4qn4bEq2lwS
S8HnHyGZArMmBknAAPzbsHAyuDz1Oy6CRWVsFSCCD3rSKfKk2cVeUfbylFWV72/Q8p+CZH2bWAez
xH9Gr1UjgjjGOleVeHLb/hXXifVIdTDxaTdKGt7oqSh2klVJGcNhiOepHHUVvyfEPStTglstHmkl
1OUFLaJ4ym5iDg5YAADrzzjtmsaMlCCjJ2aPRzKjPFYqVajFuLs7rZaLf069jlPh1am88SeJkgml
hgLEbom2kfvGK4PbgEfStn4VzXNzba39snmmnFxsaSRyW4BHXrXQ+CfCaeFNIaFpFlupjvnkHAJ7
Ad8D+pPfFc54Wubfwz4g8W21/KsR+0C4iU9ZFbJG0fxHkDA7nFKEHDl5vP8AE2xGJhifb+zV9I27
6NJ/eWvAN5c3fijxVb3F5cTx206xQpLMzhF3SDjJ/wBkc9aqaMZ4PjHqOnG8upLSC33xRSzvIFJW
Mn7xPqfzqh4Bu10Txfr0esyxWVxcBJ2WWQDBYltuT1I3jpUlnqdgnxs1C4+1wLC0GwyFwFLBEBGS
cZ4x+BqVK8Y3/m/zKnQUa1VRV17NNW2vaO34lm01ZPEfxG1K01SVP7K05WjitnIEbOGVSWB4Y8HG
enGKm8M3gt/HmveHbC7dLBoxPbGIhlhOFLBMgqBljxjHHSqUWnJ4W+KNzdaggGl6mreXcSAFFkYh
sMTwp3AgZ9RXVaUZG16SSOaGWJTL5jQqpjRMr5Y3AAhsdRk9CeBiripN67psxrypxi1FXjKCt2T0
19bp39TmLK31LUviRrFhdajNdpZWxEImICsWCMAQoA25xkADOADnkG5rmr3/AIK8OGGNIV1C9nCW
sUfKRDaoJAwB1BOAMAt35zR0bXdOh+MGuTPfW6200IVZmkUKWUIMA5x2P5VsfE3Qr3VNLtL7T4zP
NYS+aYl+8ynGcepBAOPr9KSv7OTjvd/mVLl+s0qdVJRcYvsr2/V7mtH4bmt9FBXUbwaqsZY3RnYg
ydeUJ2lc8bSMAdMdaXwN4mPijw8t3KipcxOYpwvTcADkexBB9unarT+JdM/sP7cl1E2+LKRhgXZi
OFC9S2eMdc8Vm/Dnw7N4e8MrHdArd3DmaVT/AAZAAX6gAZ9ya1XxJLaxwOzoTdT4rq343+Wx2VFF
FanCFFFFABQelFB6UAZNWol2pz1qJEwxz2qbcMHGKyk76I5IK2rK7ZDn1pxdnXbikkYMRgc01G2t
ntTtdE3s7CHpTdxXoamZQ/IPNCRYOTQ3pqOMW3ZFYqSwBzTmjUDp0qZyinPFVpZC3A6VN2yrKK1I
pG3Nx0p6rhKaiZbNK8uCQBzRLshwsveZC8bbu+KcRhQKerFlyaQ1Db2NoxS1Rqab/wAeg/3jVyqe
m/8AHoP941crdbIsht/9Sn4/zqjrP+qi/wB4/wAqvW/+pT8f51R1kgRRf7x/lTlsTL4WZQqQAelR
inANuBBrM52TrTsZX3pop4pARjdnqatREleaYAAT0qRSD0pt3RKViVQAc0x5DnAp4oMatQmr6hJP
oETM5xj8alKNkUQgLxViplLXQ1pwTjqRhQAM01SBKAO9SMOKiQZnB9M0R1ZTVmki3RRRW5qFFFFA
BRRRQBSuNMsruVZbi0gllUYV3jDED0BI4pj6Np0iFJLG2ZW6q0SkH68VfzRSshqTWiZBb20NrAsN
vEkMS/dRFCqO/AHAp8kMchBdFJU5BIzg1JRTFd7ijpRRRQBTu7OG+tntrhC0TjBAYqevUEYIPfIO
RXHeMvAMWtWcU+mnyNTtQPKlZiS4HQMxySeOCcnP1ru+3akJ46iplBSVmjajiKlCSnTdrfccT4fi
8RXl9a3GrWk9pPD8s7mdWilUKwAVVJwSxDEkduD2HYNawPKszwxtIv3XKgkfQ1OOB1pD0oSsrbk1
arqS5kreSITa25eRzDGWkGHJUZYYxgnvxTlt4UUKsSKo4ACgCpqKdkZ3fca6K6kMAQeCCM0ixoi7
FUBQMYAwKkophdkAtodpTyk2nqNowamwAMY4opaAu3uV0tLeOUyrDGsjcs4UAn6mp6KWgG29wooo
oAKKKKACg9KKD0oAzX3bsmldflBFLJ9wZ601H2jB5FZeaON72Y3BPQHNOETEdKlR0J4ABqShyaHG
CfUpkFTjkGmlm9TU8qHJPao0ALc07q1ybNOxCQx5wcVGeTirkm1V7VXRdzE4pc2hTjZ2FC7FFRsi
OSRTp1bPGcVHEpUHNT0uaLflsOI2jFMPWnmmEc1JsvI1dO/49B/vGrdVNO/49B/vGrdbrZDIYP8A
Ur+P86ztd4ih/wB4/wAq0YP9Sv4/zrN14Zhi/wB4/wAqoip8JlxPuAqcVSRiDVtG3AYrOSsc0WTi
nioxUgqSgkY9B0p8IOc9qAATyKkXp0p82hPLrcV2IxinxuW4PSjaGGDT0QKKLq1gs73FFSrJgc0z
FB6VOjLTa1Q8yKQQDzTY/wDWioFz5nerUa/MCapJJocZObRYooorU6AooooAKKKKACimhlbOCDg4
OD0NOoAKKoatE0umT7Z5YWRGcPE+1sgHv6VzPwqu7m/+HmnXd5cS3FxK8xeSVyzMfNYck/SgDtaK
KKACiqV3qNlYqrXV3DCGcIN8gUlicAAdySelWRIjMyh1JXqAeR9fSgCSiopZoogplkVNxwu5gMn0
Ge9S0AFFQiaIsVEqFgxTAYZ3YyR9cc4pZZoogplkVNxwu5gMn0Ge9AEtFFFABRTSQMZIGeKRnQMq
llBPQE8n6UAPoqlqL3SadcGwRGu9hEIfhdx4BPsDyfYUml2k1jp0FvcXk15Og+e4lADOx5JwOAM9
B2HFAF6iiigAooooAKD0ooPSgDLdy5oCsx4BpD8rc1ZVkxxis27bHFFXerK5Uq3NWY23JmoJG3Mc
dKmgHy0paq7KhpKyHNgrVJuCcVPK5BK9qrnrRFCnK7ELM+BzTyyxrinRL3IpJUD/AFpSavYuMWlz
LcYH35pCKVE2A0hqHa+htG9tSJutMbNPI5zTDRcaNXTf+PQf7xq5VPTv+PQf7xq5W62RRDb/AOpT
8f51R1nmGL/eP8qvW/8AqU/H+dUda/1MX+8f5U5bEy+FmOEBNSooUYFMFSKKzuc9iUVItRggDmpF
IpDsSqOakWoxTi5UjFIHoTCpBUSHIBNShhnFAAOTQSAacKaVyQaB2Y5VXOanFQCpI2OQKS1aNINL
QmoooroNAooooAKw/FaalL4X1FNHXdfmE+UobaW5GQCCCCVyAQRz3FblUdUtprvTbiG3lMU0iYR9
xXn0yOQD0yORnI5oA878OeL/AA/q2u2FhNps3h7WoZPmtXh2LMdjAISMZwSCNwHK4HWp/Ak48dya
xr+sILmEXbWlnayfMluiqGyo6bzuGW68cEDitR/CbahcaF9ptnR9JuRcfbLmdZJnALMI1IHK7ivX
bgKAB6S6F4bu/CF7qcWlxpdaZfT/AGlIml2NbuRhhyCGU4GD1GMYPWgDO0bVbseJ/FfhGWWSaCzh
E1o8hLsiPGpKFjywBYYyScZGTxjktM8UXXhn4A2VzYHbdyXElrFIQD5ZaR2LYPGcA49yK9DtNAvN
Nl1jVkghuNa1ZxvAkxFCqrtRdxGSAAMkLkk9MCud0n4dXkvw9fwhrawIFdpoby2lLhX3ZX5WVT3b
PqO+TwAa+seH49B8D3d3ZSyR6vYWbT/b1Y+bLIg3Eux++GKnIORg4A6Vu+EtZbxB4V03VZECSXMA
Z1AwAw4bHtkHHtWfeWOvax4cfRLuO3geeAW9zeJMXDKRtZkXaCSRng4Az1OOd7TrG30rTbawtU2W
9tGsUa+igYGfU+/c0AeefFLSrRtS8J3CW6RXEuswxPNEoV2DEZ+YDOflGD2pvieCDw18TfB0+k28
Vob55LW4SBQglTKgbgOuN2cnuB6Vb+KpkEvhLyQpl/tuHYHJClu2SOQM46VsyaBdaz4y07W9TgW3
g0qJxbwhw5eZ8BnyBwoAGM4JPJAxyAQm50+TxhqVqWi1SUxDzbRLTzJIshQFaRvlVcKx2kjJbOM5
Jxvht5+reBb+zurm7jFjfzwW7JO0boqqCBuVskAsRgkjgegrR0PwzrfhvxF4gns0sbq01a4+0rNP
MySQsSSQyhTuALHGGHTqM8WdD8P6t4ZtNUtLRba8juLqW6jkmlKM7OFG1gFwuCGJIzkAADngA5n4
c6A+teBdPv5rxjPNcTSTySp5ruN5A2sTlSCu4EZ+Y5IJArpTc6fJ4w1K1LRapKYh5tolp5kkWQoC
tI3yquFY7SRktnGckp8O9A1rwroEWi6mlm6Rs7pNbzs33iDtKsi+p5BNQaH4Z1vw34i8QT2aWN1a
atcfaVmnmZJIWJJIZQp3AFjjDDp1GeACL4RXlzceE7u3uXkIstRmtoRIclUUKQucnoWI6+1dN4ot
Bd+GtQBlmhkjt5JI5YJWjZGCnBBUg/h0PcVn+FPD174be9tPMims57yW7E5Y+YxcL8pUAAEEE5yR
jGBzxp+Io7y40C9tLK2M9zcQPCnzqoUsrAMxJ6A46ZPtQBwfgzw5p2qfCm3utVh+13EkM8izyDMk
Pzscox5U5G7I5JPPpVG2tU1L4GPrN9uuNWW2eZL5zumRo5CE2v8AeXAUdD6k8k56jw1pGu6T8PDo
N1Ywm8ht3hieO4G2TcWwSSMrgEZ4PtVC08K+IYvhU/hE29ml2UeH7Qbg+UVZixbhS2cErjHX2oAy
vHk8uofBPTdWuJG+3NBbM0qsVLFgu7OOoPWvU9OCLploqDCCFAo9BgYrz/WvCniTVPhza+E1t9Oj
lhigia6N2xQiPHQeXnJ2jjGOetbxsPFhuNAFre2NnZWuF1GHmVplAAG0lR1AP93Gc89KAOtooooA
KKKKACg9KKD0oAoSx7vmHWoeQe9WUcMvaoZAN3FZRb2ZxzS3QyrETAL2qvRz702rkxdncdKwZvao
1XcwFBqRF2rk9aHohx1lqJK5QADpUKO7N14qYurErxmmbQucVHQ11b0eghJ9abnNBqPkN7VLRomK
1RmnmozSNEa2nf8AHoP941cqnp3/AB6D/eNXK6FsgIYP9Sv4/wA6z9cbbFF/vH+VaEH+pX8f51na
4MxRf7x/lTexE/hMuNt3apwKrxcCrC1k9zGOwP2p8T4IB6UhXK0gGKad1YUk0y2tSAZxxUETcAVO
tS9ClqSClVOc5pB2qQGlcbimKTgUDkZp3WgClcdtQ4pyffFRYOetSxjDCqW6HFtsnooorc1Ciiig
AooooAKKKKACiiigAooooA5fxL4SHie90+W41Ge2SwlW4gSBVz5oOQxLA5HA4wO/Xt0FtHLFbxpN
OZ5APmkKgFj9BwKsUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABQelFB6UAZO70pRUxgU9
DTWhYdBxUcyOJxkPijVhk0kyqo4xTFZk9aQsXI9amzvcd1y2tqEa7mHpT5U3LxS58pfeovNZmxSb
bd0XFRS5XuyNYyGJIpTUxqF+tS3dmiiktBjVGTilZhnHemNQCFJyKjajODQelJmkdUa2l82Yz/eN
XKp6b/x6D/eNXK3WyGR2/wDqU/H+dZ2uf6qL/eP8q0YP9Sv4/wA6ztc/1UP+8f5U3sRP4WZKHBqw
DxVRanjbsayZlF20ZYU04KD2pg+tSLjipuXa49QBUy9qjGKkDClcOWxIAakC+9Qb9tSq/FGoXWxI
g5PXFO4FMDGlI3etAbbDlwRTlI3UykjJMo9KaWqBS2RaoooroNQooooAKKKKACiiigAooooAKKKK
AGM23rVS61O1swDNOiA/3mp19cLbW0krA4VCx+grmorOMsZ7lFnuW5d3QHB9FBHT/JyamUrGtKlz
nSWmoW96m+3lWROm5TkVbBBA5zmuLuVj0mT+0bdViRceeqABWT+IkDjIHOcVrr4it2X/AEeOe5A7
wpkZ/EilGSa1KnRad0b1LWXpurW2phzC58xGw8bAhkPoRWnVJ3MWmtGLRRRTEFFFFABRRRQAUUUU
AFFFFABQelFBoAz4lYtk5qdpFXgmnADFRyQ7jkdawumzBRajoIpEg6UhVEOcinKmxcVXlRixIzQt
XuTLRXa1JC6uOOlNCKDkCmIu1TnrUnak9NEaR11YhqF6lNQSNiki5bEZA3VHJxmnscDmmkh14pkJ
XViOlNG3FNNJs0jGy1NjTP8Aj0H+8au1T03/AI8x/vGrlbrZDIYP9Qv4/wA6z9c/1UP+8f5VoQf6
hfx/nWdrp/cw/wC8f5U3sTL4THBp4NQg1IDWZz3Jwx9akRyDz0quDUgNFg5mW1bIHNSA1WiYA1OD
UNGidx59aVHwfakHIo2c8U0yJJ3uiyjA45qQGqyAjFTg0miovTUfSp98U0U5PvinHdFLcsUUUVua
hRRRQAUUUUAFFFFABRRRQAUUUUAUr+Bbm1lt2zh0Irm9Pnee3ZZf9fExjl/3gcE/j1rriASQa5bW
bUadqK6lHlYJMR3CjoD0V/w7+1ZzR0YeaTsSSxxzQtHIoZGGGU9CKSKNEi2KoRRwAvT0pXdVjyzA
DHJPSs2XV1QtFZxyXdweFSIZXPux4A/Os9kdztYs6bGU8W+bEdoktm84euGUKa7AVznh+wngWS6v
tpupTyEPyovZR/nk10Q+6K2imlqefiHFzdh1FFFUYhRRRQAUUUUAFFFFABRRRQAUHpRQelAFdT2p
9RA4qQHIrmZnF3EbpULKRUpNMJpJlNXISOaD+lOJphNNsErDSahk6ipGNQueTQglsRtyDTEGM0jO
dxpQeKb0REbNiM2KbnIpx60w0jbW5taX/wAeY/3jV2qOl82Q/wB41erdbIRDb/6lPx/nWZr/APqY
f94/yrTt/wDUp+P86zPEH+oh/wB8/wAqctiZfCzFB96kFQBqkDVBzXJgaeD71CGpwaiwrk4PNWUc
YFUQ1PDUONwUrF8H3qQNVJJyuM1MswNS4stTTLQanhqrh8jinBzSsHMWA4BAJqRD84qg7EMKntpc
yqp6/wD1qpR2YRn71maNFFFanSFFFFABRRRQAUUUUAFFFFABRRRQA2q91ClxA0TqGVhtYEZBB9as
0EA0WC9ndHO2nhuFVC3kst0F4XedqgDjoOv41tW9rBbxiOGGONR2RQo/IVPgZz3paVinNvcQKoHA
Ap1FFMkKKKKACiiigAooooAKKKKACiiigAoPSig9KAKh6HmmqSuc0jlg/HSkLY6msDm6km7NMJpm
734o5qWjaMrgTTCacTUZNIoY3WopDzUhNMPNO4mrkBAJ6c0pyBTzgdqjLUNhGNhoNMJp5NRseKDR
G5pR/wBCH+8avVQ0k5sR/vGr9brZEkMH+pX8f51leIziCD/fP8q1YP8AUr+P86x/Exxbwf75/lVE
T+FmEGp4aq4anB/elY5blkNTg9Vw/vTw3SixLZOGp4aq4b8KUNTsK5ZD08P71WDU8PTsJsvwPkYz
zU4as6KXa3XiritkDBrOUbMuMtCV13j3pbQEXaZ6c/yNMDVYtT/pC/j/ACpxZUVeSNOijNGao7Ao
ozRQAUUUUAFFGaM0AFFGaKACijNGaACijNGaACijNFABRRmjNABRRRQAUUUZoAKKKKACijNFABRR
RQAUUUGgCq2GFVpVY9KnZtophYMK507Gcop6EAyF5p4JxSkDNNJpN3HGNhCaYTQSc00mkUMdvSmE
nvTmYDvUW7dnmmC0YpNRmnGmk0ixjGonbHFPZsVXLZNNImTtodFo3NgP941ois/RxiwH+8a0BW62
QkQwf6lf896xfFJxbW/++f5VtW/+pX8f51h+LDi1t/8AfP8AKrIqfCzmw9ODVAGpwanY42ywG96c
G96gDU8NRYm5ODTw1Vw1PDGiwrk4bnrTwagDU8NVCuTg1PFKV69KqBqeGqWrjuaSSqwHSpA3FZgb
B4NSrKw71Lj2KUu5pBqcDmqcMhbIJqwG96lqxSdxJXKsOafHODgGmSLuHvUByp5FUkmrEttM0A3F
Lmq0MhztNT1Eo2ZSk2OLAdTSBw3SmldwwaRVCjilZDuyTNFVmlbfgdKsA8ChxsClca8m3oOacjbh
kik2hj0GadtODijSwK4hw3FKo2jApiIwY56VLtFJ6DWpGE+bOc04Z7U/ikLD1ouO1hOT1zSdCM0u
8VGAWYk0IGy3GQRTiMmq6sR0p4m9etQ4voaxmrWYrnC4plIxyc0qYzVbIhvmYoU+lOEW4c1LgUtT
zGqgupTaHawx0qUMVA5p0mKjzVXutTNrlehKJPWlLAiqzsccUDcFyamxUZssk8UwmofMI70GTI9D
SszRSTHthh1ph4poPfNBagad9Rpb5jTSaD1ppNA0NJPaomc5NPYmoWU5JoQpX6DWbPem5OeKCeaU
cDJ602TFNsCeDmo2604mo3bANI1eiI5X7ZqFjxkGlb5s+tM6A1SVkYtts6bRTu09Seu41o9qzdD/
AOQcP941pdq1WxotiKD/AFCf571g+Lzi1tv98/yreg/1Cf571z3jE4tbX/fP8qtbkVfhZywbinhv
pUAang1ZwkwanhveoQaeDTsImDGnhj61ADTwaLCJw9SBqrg08NRYCwGpwaoAx9acGJ70WAsA04Go
VLYpwzSEWYn2tnPFXUYMARWaPrU8Uu0jPSplG+xUZW0ZfDUvB6iokdWAOaeHHtWeppuSBQOgqQVC
HxThIaTTDQlOB1NKAMVTnkYc5OKSO5I4NPkbVxcyTsXNiBt3GaVmG0gVErq/Q0u72pW7jv2FjJXO
etD3G0gUCmlFYjPWnZX1Fd20H7yQDS7jTDkLxTY93OaLIdyQkgZqFGdnOc1KXVTgnmlGOoAo2Qnq
LTh0ptKKkaFqGRyG4NTZGcZ5qOSPccjrRHfUcttB0bbl5p+cHrUaLsU561C8hJPJxT5bvQV7I0Fl
HAp+8YzmqETHByalLVEo6m0ajsMmmbdgUsCs2STxTWUMORzT4mCLtpt6WRMY3leRIVUEc04kYqIt
uah3Crkms2dEbK5G5wxppao/NDMaUZPSratuYrV6AX2jOaaJsj2pWTIOaYVCqcdaWhdpLYcJQ3ek
3A96rKeD60iMwJocRxm+pZJphNMMnrSbwamxqpJgcU0mkduKjLY70WC6THFuKru25qe7cVXL7Wpx
RMpLYeV9TTSAKUPuppNJtlRS3R0uif8AIOX/AHzWlWZon/INH++a063WyAhg/wBQn+e9c74z/wCP
S2/3z/Kuig/1Cf571zvjT/j0tv8AfP8AKrW5nV+FnIA08GogaeDVnCSg08H3qIGnhqBEoNPBqENT
g9AE4NOBqAN708NTEWARTgwFQBqcG96LAywJKcHPtVcNTg1FgJw5pwY+pqAMO55pwb3oEWFkZRwT
inpO6kc8VWD+9ODU7ILtGjHOrcHrU4aslX2kGr0UgZRzWUo2Li76MndQ6kd6qsjKehqwGp2Qe1JS
sNxuQRFw3GcVcDHbz1qMEDtShveiUrglYlDc04GoCwUZNKsqsODUtDuiUsFPQ04HpUec+lBJ2nHW
kMeyKxGetOGAAKgjZ8nNP3heppvsJdyXNNZsLxyaQMCODQWqR2IN7bieasoxK89ajwuegprTBeO9
U3fRAlbcnLZqMopOaTfuXINVjKwbFKKY20i4CAMCgt71Cj7l680u6pZSY5mIHApA3embwSRQTkHm
gESCQjvUczsy4FR5ZT1pS3FFrO5XM2rCINvJqYMCOKhLUgfb3qZalQdictUbtwaYZhkDPNRSSZNS
os0clbQDxnFITQp3GnnmqbFGNyMqSKic7TyamLYqnO/zDnmiOrHNKKHlzTTIR1FRq+eKkK8cmm9N
yY3ewwtnNVnPzGreABUMijrikpDlBtESMymnGSmFgBTS3pTtcItrQ6zQDnTAf9pq1O1ZXh//AJBS
/wC+1avatFsaLYig/wBQv+e9c342bbaWn/XQ/wAq6SD/AFC/571zHjo4s7T/AK6H+VXHcir8LOPD
04OagDU4NxWtjguTh/enhqrhqcGpiuWA3vTw3FVw1PDUBcnDe9ODVAGpwagVywHpwYVAGpwagLlg
NTg3vUAanhqAJg1OBqIGnBqQEoNPDcCoQ1ODc0ATBqkSQqcg8VADxTg1IDQS4VgM9alDqRwR+dZY
b3pwcjuRUOKKUmagce3507NZiyMDnP61djcMuc81Mo2KUrkrfMpFVgxRj1qxn3pjIHojKwSjfYWO
4I4PSpw4YZHSqoh561MgCrgUpW6DjfqTbqY6b+c803dQH7Uk7DaHplRg08tUQb3pd1JgPLVWckuf
WpS1JxnOKadgauPThcU0opJ9aN1IWqbsuysOUbRjNG6mk0w53ZoCxIMA5703cxb2ppcA4zzRu96A
HlqYWpC1ML/NilYocWpC1MLAGk3cUAKetMOSwoLUhai47CFyjipRICOtQnGc00tzSaTLjJonZxtP
NUpFLNnNSFvemFutEdByfMIihTmpd1VWkA+tIsrZ9qGm9QhJLQsE1BM5xgUrzbRz1qEuGPWkl1Kl
JNWRCWOTmkDc091BpuMCquZKLudj4d50pf8AfatXtWR4c/5BK/77Vr9q0WxutiKD/Ur+P865Xx6c
WVn/ANdG/lXVQf6lfx/nXK+Pv+PG0/66N/Krj8RnV+BnDhqeDUINPB4rY4CYGnA9KiBp4NIRMDTg
aiBp4agZKrU8GoQaeGz9aBEgang1EDTgaAJgeKcGqINTwaBkwang5qANipAaQEgNODcVEDinBqBE
wanBqgDc08NSGS5pc1GGoDUCJQ1SxSlWHPFVw1KGoauNaGmjhhkGnbqzUkKng1YFzge9ZuL6FqSL
YanBqprcAnkVMrhhkYqWmilJMmz7/rVZ3KyZ5qXd70x1DfWlF66hJNj0nDDk81IGBqgVZSeDUsTO
D3xTcVuJN9S3upC1M3cdaC3vUFjt3OKC3pUZalLUMaG723YxxTy3FMzTJWbHFPcNh7qGOc0udq1C
jNjk04sCMZpPsC7iiUM2KUtUYUA5psjlSMUrXeg1otRzgsc5oLBRk00Plc00nIxR6jS6ocHDDikL
UwALSFqGNeY8tTC3NNLYzmo265BosFyUtTGPFJu4ppNIohY8mhetPOPSmnAptkqOoyU8VDux3qZj
kVCV+b2oT0CSdx4Ykc00mgmmk0jQ7Lw1/wAghf8AfatjtWP4Z/5BC/77Vsdq1WxotiK3/wBSv4/z
rlPH3/HlZ/8AXRv5V1dv/qV/H+dcp4+/48rP/ro38quPxGdX4GcJSg80gorY4CQHFPDVEDTgaQEw
ang1CGp4agCUNTw1Qg04NQBODSg1EGp4agRKDTwahBpwagEShqeG96hDU4NQOxOGpwNQhqcGpASg
+9ODVCGpwagCbdTs+9QhuKcGpBYk3Uoaot1KGoAlB96cG4qINxShqAsShvenxSkMOTioN1Lmh6j2
NINuGRS7jWekzKeOlWEnDdetYuNi1JMn3Uu70qMuPWjzB60rFEu7pSFtozUYagnIIpDHCVWp273q
i2VbvilWZlPWm49iVLuXC1IWqJJN9Lu5qWrFrUeTUJ3bupxTi1IWoTsFrji+1cmkDq4qOTLDg01F
2jk80aWHrclJwOKjUtnnpTTL82KcWpbD3FzTS1NP3s5pC1A0DEEYNJ0prYJz3pN3vQCQpakLUxn2
00tkcGgdxxNNJpCTjrzTS1BQFqaTSFxn3ppNAXAtzSZpKKBna+Gf+QQv++1bHasfwz/yCF/32rY7
VotjRbEcH+pX8f51T1TSLXV440ugxVGJG1sc9KsQyosQUtgjPY+tP8+L+/8AoaoGk9GYP/CFaR/c
m/7+Uv8AwhWj/wByb/v4a3vPi/v/AKGjz4v7/wChouyfZx7GD/whekf3Jv8Av5S/8IZpH9yb/v5W
758X9/8AQ0efF/f/AENF2Hs49jD/AOEM0n+7N/38NA8HaSP4Jf8Avutzz4v7/wCho8+L+/8AoaLs
PZx7GJ/wiGlf3Jf++6X/AIRDSv7sv/fw1tefF/f/AENHnxf3/wBDRdh7OPYxh4S0sfwy/wDfw0Dw
npY/hl/77rZ8+L+/+ho8+L+/+houw9nHsY//AAimmf3Jf++6X/hFtM/uyf8Afda/nxf3/wBDR58X
9/8AQ0XYezj2Mj/hFtNH8Mn/AH3S/wDCL6b/AHZf++61vPi/v/oaPPi/v/oaLsPZx7GV/wAIxp39
2T/vuj/hGdO/uyf99Vq+fF/f/Q0efF/f/Q0XYezj2Mv/AIRrT/7sn/fdH/CN6f8A3ZP++61PPi/v
/oaPPi/v/oaLsPZx7GYPDlgP4ZP++qX/AIR2w/uyf99VpefF/f8A0NHnxf3/ANDRdh7OPYzf+Eds
P7sn/fVH/CO2H91/++q0vPi/v/oaPPi/v/oaLsPZx7GaPD1iP4ZP++qX/hH7H+7J/wB9Vo+fF/f/
AENHnxf3/wBDRdh7OPYzv+Efsf7sn/fVH/CP2P8Adk/76rR8+L+/+ho8+L+/+houw9nHsZ/9gWP9
1/8Avqj+wbIfwv8A99VoefF/f/Q0efF/f/Q0XYezj2KB0KzPUSf99Uf2FZ+kn/fVXvPj/v8A6Gjz
4/7/AOhpahyR7FQaPaAYAf8A76pf7HtfR/8Avqrfnxf3/wBDR58X9/8AQ0WHyx7FM6LZt1Vv++qZ
/YVl/df/AL6q/wCfF/f/AENHnxf3/wBDQHIuxSGi2ijhX/76pf7GtP7r/wDfVXPPi/v/AKGjz4v7
/wChpWDlj2Kf9jWn91/++qQ6Naf3X/76q758f9/9DR58f9/9DRYOVdil/Ytp6P8A99Uf2JZ/3X/7
6q758f8Af/Q0efH/AH/0NFg5V2KH9g2Oc7X/AO+qU6HZEcq//fVXvPi/v/oaPPi/v/oadg5V2KP9
hWf92T/vqk/sGz/uyf8AfVX/AD4v7/6Gjz4v7/6GlZBZGf8A2DYn+GT/AL6pD4fsT/DJ/wB9Voef
F/f/AENHnxf3/wBDRYOVdjOPh2wIwVk/76oHhywH8Mv/AH1Wj58X9/8AQ0efF/f/AENFg5V2M7/h
HLD+7J/33Sf8I3p/92T/AL7rT8+L+/8AoaPPi/v/AKGiw7Iyz4Z07Odsn/fdH/CM6d/ck/77rU8+
P+/+ho8+L+/+hosgsjL/AOEZ07+5L/33R/wjOnf3JP8AvutTz4/7/wCho8+P+/8AoaLILIjs7OKx
gEMIIQEnk55NWai8+L+/+ho8+L+/+hpjP//Z
', '2016-12-13 05:59:06.987188-04', '2016-12-13 05:59:06.98721-04', 3);


--
-- TOC entry 2306 (class 0 OID 0)
-- Dependencies: 198
-- Name: categoria_categoria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('categoria_categoria_id_seq', 2, true);


--
-- TOC entry 2262 (class 0 OID 237082)
-- Dependencies: 201
-- Data for Name: ciudad_ciudad; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO ciudad_ciudad (id, ciudad) VALUES (1, 'Lima');
INSERT INTO ciudad_ciudad (id, ciudad) VALUES (2, 'Arequipa');


--
-- TOC entry 2307 (class 0 OID 0)
-- Dependencies: 200
-- Name: ciudad_ciudad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('ciudad_ciudad_id_seq', 2, true);


--
-- TOC entry 2264 (class 0 OID 237093)
-- Dependencies: 203
-- Data for Name: coleccion_coleccion; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2308 (class 0 OID 0)
-- Dependencies: 202
-- Name: coleccion_coleccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('coleccion_coleccion_id_seq', 1, false);


--
-- TOC entry 2251 (class 0 OID 226205)
-- Dependencies: 190
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2309 (class 0 OID 0)
-- Dependencies: 189
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 1, false);


--
-- TOC entry 2237 (class 0 OID 226091)
-- Dependencies: 176
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO django_content_type (id, app_label, model) VALUES (1, 'admin', 'logentry');
INSERT INTO django_content_type (id, app_label, model) VALUES (2, 'auth', 'permission');
INSERT INTO django_content_type (id, app_label, model) VALUES (3, 'auth', 'user');
INSERT INTO django_content_type (id, app_label, model) VALUES (4, 'auth', 'group');
INSERT INTO django_content_type (id, app_label, model) VALUES (5, 'contenttypes', 'contenttype');
INSERT INTO django_content_type (id, app_label, model) VALUES (6, 'sessions', 'session');
INSERT INTO django_content_type (id, app_label, model) VALUES (7, 'categories', 'categories');
INSERT INTO django_content_type (id, app_label, model) VALUES (8, 'tienda', 'tienda');
INSERT INTO django_content_type (id, app_label, model) VALUES (9, 'direccion', 'direccion');
INSERT INTO django_content_type (id, app_label, model) VALUES (10, 'categoria', 'categoria');
INSERT INTO django_content_type (id, app_label, model) VALUES (11, 'ciudad', 'ciudad');
INSERT INTO django_content_type (id, app_label, model) VALUES (12, 'tipo_negocio', 'tiponegocio');
INSERT INTO django_content_type (id, app_label, model) VALUES (13, 'coleccion', 'coleccion');
INSERT INTO django_content_type (id, app_label, model) VALUES (14, 'banner', 'banner');
INSERT INTO django_content_type (id, app_label, model) VALUES (15, 'productos', 'productos');
INSERT INTO django_content_type (id, app_label, model) VALUES (16, 'local', 'local');
INSERT INTO django_content_type (id, app_label, model) VALUES (17, 'telefono_local', 'localtelefono');
INSERT INTO django_content_type (id, app_label, model) VALUES (18, 'corsheaders', 'corsmodel');
INSERT INTO django_content_type (id, app_label, model) VALUES (19, 'negocio', 'negocio');
INSERT INTO django_content_type (id, app_label, model) VALUES (20, 'fidelizacion', 'fidelizacion');


--
-- TOC entry 2310 (class 0 OID 0)
-- Dependencies: 175
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('django_content_type_id_seq', 20, true);


--
-- TOC entry 2235 (class 0 OID 226080)
-- Dependencies: 174
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO django_migrations (id, app, name, applied) VALUES (1, 'contenttypes', '0001_initial', '2016-11-27 07:15:25.125456-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (2, 'auth', '0001_initial', '2016-11-27 07:15:26.010348-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (3, 'admin', '0001_initial', '2016-11-27 07:15:26.25207-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (4, 'admin', '0002_logentry_remove_auto_add', '2016-11-27 07:15:26.34354-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (5, 'contenttypes', '0002_remove_content_type_name', '2016-11-27 07:15:26.401633-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (6, 'auth', '0002_alter_permission_name_max_length', '2016-11-27 07:15:26.426585-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (7, 'auth', '0003_alter_user_email_max_length', '2016-11-27 07:15:26.460089-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (8, 'auth', '0004_alter_user_username_opts', '2016-11-27 07:15:26.485113-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (9, 'auth', '0005_alter_user_last_login_null', '2016-11-27 07:15:26.510075-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (10, 'auth', '0006_require_contenttypes_0002', '2016-11-27 07:15:26.518284-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (11, 'auth', '0007_alter_validators_add_error_messages', '2016-11-27 07:15:26.536448-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (12, 'auth', '0008_alter_user_username_max_length', '2016-11-27 07:15:26.610395-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (13, 'sessions', '0001_initial', '2016-11-27 07:15:26.785664-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (74, 'tipo_negocio', '0001_initial', '2016-12-10 07:00:22.233072-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (75, 'negocio', '0001_initial', '2016-12-10 07:00:22.391822-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (76, 'banner', '0001_initial', '2016-12-10 07:00:22.575336-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (77, 'categoria', '0001_initial', '2016-12-10 07:00:22.725487-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (78, 'ciudad', '0001_initial', '2016-12-10 07:00:22.825987-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (79, 'coleccion', '0001_initial', '2016-12-10 07:00:22.9759-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (80, 'local', '0001_initial', '2016-12-10 07:00:23.184598-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (81, 'productos', '0001_initial', '2016-12-10 07:00:23.393233-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (82, 'telefono_local', '0001_initial', '2016-12-10 07:00:23.518028-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (83, 'local', '0002_auto_20161211_0918', '2016-12-11 05:18:58.500414-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (84, 'local', '0003_remove_local_telefono', '2016-12-11 07:43:09.077083-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (85, 'telefono_local', '0002_auto_20161211_1227', '2016-12-11 08:28:00.091148-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (86, 'fidelizacion', '0001_initial', '2016-12-13 02:27:49.487607-04');
INSERT INTO django_migrations (id, app, name, applied) VALUES (87, 'productos', '0002_auto_20161213_0929', '2016-12-13 05:29:12.360294-04');


--
-- TOC entry 2311 (class 0 OID 0)
-- Dependencies: 173
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('django_migrations_id_seq', 87, true);


--
-- TOC entry 2252 (class 0 OID 226234)
-- Dependencies: 191
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2272 (class 0 OID 237247)
-- Dependencies: 211
-- Data for Name: fidelizacion_fidelizacion; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO fidelizacion_fidelizacion (id, moneda, puntos, date_create, date_update, negocio_id) VALUES (5, 10, 20, '2016-12-13 02:54:19.521177-04', '2016-12-13 02:54:19.52121-04', 3);


--
-- TOC entry 2312 (class 0 OID 0)
-- Dependencies: 210
-- Name: fidelizacion_fidelizacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('fidelizacion_fidelizacion_id_seq', 5, true);


--
-- TOC entry 2266 (class 0 OID 237110)
-- Dependencies: 205
-- Data for Name: local_local; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO local_local (id, nombre, correo, direccion, date_create, date_update, ciudad_id, negocio_id) VALUES (24, 'fhfh', 'fsf@gmail.com', 'fhfhf', '2016-12-11 07:59:16.136817-04', '2016-12-11 07:59:16.136849-04', 1, 3);
INSERT INTO local_local (id, nombre, correo, direccion, date_create, date_update, ciudad_id, negocio_id) VALUES (25, 'uuuuu', 'ggg@gmail.com', 'ggdfgd', '2016-12-11 09:28:11.882558-04', '2016-12-11 09:28:11.882598-04', 1, 3);
INSERT INTO local_local (id, nombre, correo, direccion, date_create, date_update, ciudad_id, negocio_id) VALUES (26, 'YYYYY', 'pedro@gmail.com', 'fghfhf', '2016-12-11 11:21:31.9586-04', '2016-12-11 11:21:31.958626-04', 1, 3);


--
-- TOC entry 2313 (class 0 OID 0)
-- Dependencies: 204
-- Name: local_local_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('local_local_id_seq', 26, true);


--
-- TOC entry 2256 (class 0 OID 237031)
-- Dependencies: 195
-- Data for Name: negocio_negocio; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO negocio_negocio (id, nombre, pagina_facebook, status, logo, logo_ba, date_create, date_update, tipo_negocio_id) VALUES (2, 'Prueba 1', 'prueba_1_facebook', true, 'logos/52b70a7e7128a97ac1dcdcca4c73356d.jpg', '/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0a
HBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIy
MjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAHgAeADASIA
AhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQA
AAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3
ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWm
p6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEA
AwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSEx
BhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElK
U1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3
uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDxalop
9WcAZoopaRIUUUuKBCYpaXFOAqkFxopRRin0xNiUooAp1BAUUuKSmIXFJT6XFCFcbS0uKXFPcVxt
OApcUUCuAGadgUgGaNvvRYQ6kpaXFIVxKTHtUlLigVyPBoqSjFAXI8UYp+KXFILkeKMVJRQFyPFG
KfijFMLjMUVJikxSC4w0mKWimMZSEVJiimO5HijFPpuKB3GUU7FIRSHcjop9FO47jKSloosMZQaf
SUh3GYNIadikIoZQ2kp1FTsMbSEU6koGNptPpKBoZSGnUmKRQ+jFFLSJCiiloEFLRTgKqwriYzS9
KWimTcWl60AUtAgAxRiin00JsZUmKAtO+goJbExS4p2KbTQrhS7TTqXbRcm4lGKdtp2KTE2M20u2
nYp+2mhXGbaNtPxRimTcbtpdtOxS0xXGbaNvNOxRigLjMU4D2paXFA7jcUmPan4opCuMpMU+jFML
jMUYp9JSsO4zFGKkxSEUWC5HijFOwaTFA7kZFGKkpMUFXGYpCKeQabQMjxRipMUmKTQ7kVFPIplI
q43FJin0ymikxMUh606kxQxpjCM0lPIxSVFihtJTjSUDG02n0mKY0xtFLSUhi0UU7FSISloxS1SE
FPpKWqJCnCkxmnUCYlPpmKfRYli06kxTgKCWC04CgUoGaEiRKKkAoxTsK4gFLSgU/bRYlsbSYzT8
Uu2qFcQCinBaXYaCWxoFGKftNAWgVwA6UmKeFpdtArjMUbfen4pNtAXGbaNtSEUYoC43FJS4NOUU
BcZtpMU8iigLjMCkqXFN20BcjoxT6KB3I6TFSYz1ppWgdyPFGKfikxQVcZTcVLTcUhpkeKSpKQig
q5HijFKRzRQO5HtptTU0ilYpMjpuKeRikpFJkRFJUhFMoKuJTadRikUMopaQ0DEppp9JigaClpKd
UiCnCkAxS9aoQtKBQBTqCWwp1NFOqiWFOxmlAp+KRLYgHSlpQM4p2OKCWxoBp+KUClpohsSnYoUU
/GaolsaBS4JpQM9qeB2oE2MAzTttPC0BaCeYYFNO5p2KXbRYVxCDQFNOxTgKdibjcUU7FBGaLCG4
5oxT8Yo20WC4wjjpSYzUgWgrRYLjCBRUgFBWiwrjMCkp22lxRYdyPFG2n0pFFguQ7aTFTUwrRYaZ
FRUuKbiixVxmKZipqaRikUmR4ptS4puKBpkWKKkIxTcUrFXIzTSKkNJigpMiINFPYU3FBVyM02pT
TCKVikxtNp1FIoi70lPptIoQ02nUhoKuNopaQ0DDFLRTqSQmxaUCm0+nYlhSinUYpom47GaeBTQu
KeBQQ2FApcc08CglsAKKUDFOUc1dibgKdilpCDSsRcVRQOD0pR1pwHemJsTn0pwHNSClxmghsQUu
KAMU7tmgkTFJ3p56Ck6VVguLjI6U7aKBjipBiixNyPaKRh3FOIyeKVhxTFcYOeadgmnBc9KXFITk
N20bafikp3FcTFMIqTFLii4XIjSYzUu2n7KLjTK4XBpCOanI5ppTmncfMRbaCKkxTGB60mhpjMe1
IfpT+tJUjRFijFSUYoK5iKm49qkIpKRVyMimVKRTCKCkxmKQin02gdyOmsKeRSUi0yKl/Cnbeaa2
c0F3GbaaRUtNNA0yDvRipKSpZdyMim4p5FNpFJjcUlOptBQ4DFFLQBmhCFApaQVJ2pkthUi0gHNP
oIbAUUoFOFBDYU4UDpSlcVSRNx3elA5oHan4pkNjacD7UYpxwO1ArhjgU7FIo46VLQRJjQDkGnAZ
NLtNSAYppXJuMYdKB92g8mpAOKaViWxAuRSlKfSVVyXITA9KNvFLipQOADSJuRqPzpWB6dqd3waU
jjFArjAKftFaWmeH9S1ZWeztzJGrhGYnABIJ/p29qalrEts32iK5jlWcI7HARRjlSOu7+VDKcJWT
7meRQFro4fDNxe3EJUw2tvLIsQlkbAZiTjC9eg6nHSpbrwokbTm11BZre3OJbox7YgceoJJ6H6Y9
xSuivZTavY5cimEEjAFb9p4d+1Wq3Rud1tkLJLAhYRknABzj/wCt+NXLzwjJZ3Z+zXNvqMCospMT
bSUbODgnnoeAe1F0CpTtexye0jtTsmtWS2twLsbJWeNQQY8bI277gecdenSnXPhvVbbThfyWjrb7
VfceoBJAyOvb8sUE8rexk4zTSMGn0EVViLkJFIeRUoXnmkYcUFJkRXim4qXFG2kUmQBSTSkCpCtI
BinYq5GwpvantTce1S0O4zFNK1LTDSsUmRYppFPNJSNExnFRmpSMfSkoKTIqDzSkEUGgojNJUlRm
gpMYRTKlphFS0WmNphp9NNIpDKSlpKRY+nCm0o60Ei06gU+mS2KKXFAoAppEC08UUU0S2PC96XFA
6U+mQ2IKeKAKUDFBDHAZpdtOHal20ENiDGKcBS7aUU0iWw5pRzS496XFVsS2HXoKkVeKRV4qQdKC
GxpHqKAvApeaevIp2JuR45p6jkCnYBp6rTJuRsMtSbeakK/MKU/e6UCuXLHU73T9wtbmWJXGHVHI
DD0NaEOtQwSPdx2ttDIFYRW0UZKBzxvO7OAOuB1wB2rH98U0jNJxKVVpWOpstasTpFtHdO5nikZ5
93ztMy7miODwRl23DqeK52W9uZYXhLlYpG3tGhIQnOfu5x/+oVCo96fgdzS5UVOvKVkFvdT2ySxR
Suscv30DEBvqOh/Gumstd0+3s2mmUGZrfZJCq7A8qEiIqB2+ZixJ5rlSMHpxTSuafKgp1nA2f7WW
4063S4CST24MbGRCRcRnGAzD5sjGOeoA9Kr32s3d7AbfeYLTdlbaJiI0+gP1PX1NZ4U0uMDmjlE6
0n1IQlIOalwMdKbimRcZjmmleKm+g5prJQNMhBBABo29xT9tNx2oZdxlIeaeRTaQ7kZoUDFP20Y5
zQUmRkVHtqVgexpq9DSsWiIio8c1OcelNx7UmikyFhxTSKlfoKaaktMjxTSMU+koKTIqQjNPpGoL
uRd6Q040hoLRGRTKkNNIpFpkeDTakphqShy06mipBRYTACnim4p4pohiDrUwFRgdDUlURIWlxilF
KBQS2KOgp45pFGRz0p4AoIbBetOxQvWnYoJbEHWn5oAp9Oxk2Cc8U4cNigHFKvWruS2A6VIi00Cp
RjtSIbFxxQopy07GDTSIbG7M96QLg1IPmqQLmmTcjXbTiwxQYvSgR+tAhEG45PSgffowd1SCM00J
iN0xQBx0qQLtHBzQOnPWi4rkPIPSpF96kZAVBNN28UXC4wrmoyMVZppXNFxXIVGRSE54p+3J9qdt
oHcjAoZKftoK8UbhchxSHNSBaHAoZSZEp5xTWGDUnSm55pFXIyM0ir1p5GTRtOKCkyMrTalpjCgt
SIyKjIIPHSp/xpGFIpMg4ppwKkZPQU3bSZaZEetN71IRg0zHvSLTGkcVGRgVMRiozikykyI0lSHp
TCMUjRDKiIqWmnqKCkxlMbrT6CKRaIqaafSUmi7iAU8DNNqQCgTEp1IOadimSx46CnCkApwHNMzb
H0oGaSn4oIbHAYFOHUUi08ZFBDFxSilHSnLTsQ2KOlSAZpoqQdaozkxpXFORe5pxGRSqKCGxo4NS
oMr9aTbT0G04zVIlseFAHNIvzdsCnE5+lLxtGKDNibce1KAaAeeakHpQJsTBoQfNzTycUznNNIm4
5FGc4pTlmxiljGDzTtuDmnYlsZwDjvTSOaeVOelNPakFxVPGDT16VGBTgppiYBOMmm7SWx0qyBxy
OKY6k9FOKLhcjICj1NLx3FOC5GfSmEtSsNSFK8ZptPX7uTUeOc0WHcNvy0xlqYDKikI9qLBcrlMc
01Uz1qd+RUa+lItMjIwaO1PZc9sUwDaeelOxSE24GcVH97tUrDjrxTCQRxSLTImT0FBFOzzSE4oZ
SZER7Uz+Kpm461CevFTctCNnNRt6VLTGWkWmRY9ajbpUrCo2BoaNEMpKU02ky0NIphqWmHpSKTIa
DT6bSNCMim1LUdKxSEFTVGBUlIGwp4pgqUCmjNi0opFFSKKoljhS0L0FOoM2KlSgZqMLUgWmjNj6
F+akK46U+MYqkS2SKAKcMdqRcY6Uv3TkUGbFx81PxTVNLuoIYrcDilGTTsh1x6UAEGqIuOIpyrSD
PWpVOetBLY1hjGKUPjqKeOufSk2k807EXFAMgyelOEeOacg4AFPZcJVEtkXXkVIrA9aRImIz2oK4
7YpCuP2+lIRjtSLnPFSCPPJOaCWxgXI9qXYuc07y8ipBFkcCgV7DSuRQxwMAVJyKHG7AzSArIwBx
TigPSgxc00DnFMdxrAd6j68Cp5IwBkUxF55plXADbRUjKoxionOORSBMjdQSKaVxSg7myRSMcjpR
YtDH+6Oai+9UvDcHpTMYNItCEDGKj2nFSZxSZzQUmRMuBTWPAyKn6LULjNItMZ96mlBT1HBFDcCk
y0yu3PSinFajIxSNEIRTGFOJppoLTIiKZjNS4puM1LNEM7UxuKkIxTG5pFIjNMxUuKibrSNExMVH
UpFR7aC0PFKM4ooFSIcop460i+tOHtVJEMfTlPOKYKcAaZmyUU4DkU1aeO1Bm2OxTsUg6CgVSIZJ
ipAuKag9ak+lMhsTvgU7GeDSA5I45pW60IzY8KfSnhM9qSPJrfXwh4iIBGi3pB5HyD/GndIIxlL4
Vcw1TApcHOK1b3w/rGm2/n3umXNvFkLvkTAyelUAB6U0ZyjKLtJWBUO3GKft+XgU4EAVsweF9dni
SWLSLt0ZQysE4IPQ9ad0tyVCU37quYYBXtnNO2fnW2fCPiEHJ0a8A7kp/wDXptl4c1i+t0uLbTLi
aJvuui5B5xRzLuDo1b25X93/AADLiHqKkPJHpWyvhLxAD/yBrv8A74qC80LVbCLzrywnt4sgb5Fw
MntQpJ9SJUqqV5Rf3GeGPYcUybcTyMVbVMCtBfDusXVvHNb6XcyxyLlXVOCPXrTatuZQhOfwq5jR
RZG6nouWOegraXwtr4X/AJBF2P8AgH/16hj0HV5biWGHTrh5oSBKgXlMjIz+FF13LdCqvsv7n/kZ
jHABpVfoBWt/wimvnrpF0P8AgH/16yZbea2meOWNkkQkMrDBBFCaexE6c4q8otDyRio8YbNKnzDr
irdnpd9qLOllay3DIAWEYyRT2JinJ2W5UZc4xULrg8VvHwtr20f8Sm7/AO+B/jWZf6ZeadKkd5bS
wM4yokGCRSunsy5UakVeUWkVj93FNCcZNSKhI5qVIZZpFihjeSQ9ERSzH8BTIV3oii4NM+tdVB4G
8R3a7k01ox6zOqfp1p1x8PfEsagrZRP7JOuf1xU88e51Rwte1+RnJFMd6QxeprUvNB1bTl332n3E
CA43Ony/mOKo556U99iJRlB2krFYJUbA+lW8c1Gy7eaGgTIAvFBHyVr6toOoaILY38IjFwm+PDZ4
4yD6HkcVl55PFSn2NZRlF2krMgxx1qJhxk1M4FIQODimNMiAprjtUveomOWqSkMqNx7VPgYqFyMc
0GiIsU09TUqimHrxSNERHioyalYGo2WpNExtMPSnmkPSkWmRmo3qUjAqNhmguIykoNBpMsWikGTT
gKkB9OWmgU9QfSrIHr1p47U1QQaevUGgzbAcGph0phpw6UGbZJjIox0pVHrTu1WiGOGeOlOVjTR2
p4FOxmxRksM04gGkXng08jA4osQ2PTgV3PhXxfr83iLTLObVJpLeWZY3RwpyvpnGa4aOug8Ij/is
dI/6+VpSV0aUKko1Eou12j3m5hstWtrqwmEcqY8uaM9sjP8ALBrwjxH4euPD2rSWkpLRN80MuPvp
/iOhrrda8RzeG/ihdzglrWRIluIx/Eu0cj3Hb8q7bX9HsvF+gBY5EJZRLbXA52nHB+h6EVjBuD8j
1sTCOLjKMfjieEKuBiuy8JeJdYbXdM09tQla1eQRmJgCNuDx0yOlcndW09hdS2tzGY5omKOp7Ef0
rW8HKT4w0pu3nj+RreSTizxMPOdOsktNV+Z0XjLxNrNp4nvbG31CWG3j2BUQAdVBPOM1zllr+r6d
biK01O4hiUHait8o79DV3x7HnxnqBz/zz/8AQBXNFiFIpQS5VoViq9RYiVpPRs941u8uLfwbcXkM
pS4W2DiQYyDgc14/da1qmo/u72/uLiMHIR2yAfXFeteIv+Sf3X/Xmv8AIV4yv3vxqcOk0zqzipOM
oRT0aHs2AMdTWjp+va3aNBFBqlzHCrKojDDaBnpg1k/O7YA6VNCriaPuN6/zFbySaPIo1JQmuV2P
RviDrGpaZcWKWV5LAkkblghAyciuFtvEGrWs0s8GoTpLMQZG3ZLY4Gc11nxPUte6bj/nk/8A6EK4
MRtwMcVnSinA7cxrVI4lpSelvyPZPCGrTX3hqC71GcNK8zR72wMndgDisjx74Z+0xnV7OP8AfIv7
9APvqP4vqP5VnW67PhM+Tgi4zkdQfMFdJ4P8SLrVibe4I+2QDD5/5aL2Yf1rDWD5onrRqU8RTjh6
u7imn5nj7DaQe1XbHU77THaSxuXgZwAxTHI/Gt7xx4b/ALIu/tdqh+xTscAdI39Poe35Vyq8gAV1
xanE+cq06mGq8rdmj0qTXtUHw2TUxdkXpl2mUKucb8dMY6V53qGp32qSrJfXUlw6ghS56D2rtJB/
xaOIH/nv/wC1DXEMuB8oyfSsqUVqzsx9ao/Zxu9Yo1PDPh268Q3vlxny7ePBmmP8I9B711uq67pn
gpDpuiWscl7jMsj87f8AePUn2retIIvCXgt5Ao8yKEyyf7UhH+OBXjs5eeV5pXLSSMXcnuSck0l+
8k+x0VbYClGMfjlrfsXL3xPrl9IWm1S5AP8ADG+xR+Aplv4h1q0YNBqt2hHrKWB/A5rPABbkcUOM
LweK25I9jzfrNW9+Z/eeoeINQbVPhXDfStuklWIu2MZYNg/qDXlbDPNaI1+/OhjRjIpsg2Qmznru
6/WqVRCDirHRi8TGvKMl2SZAc1teENI/tvxPaW7rmGNvNl9Ni84/E4H41jSDmu78Oj/hHPAupa6+
FuLv9xbZ645A/XJ/Cio7LQMHBSqXlstX8jqfiPpQ1Hws88a5ls284YH8PRv05/CvE2C17f4Fv11r
welrOd7QA20u7qy44z9VIrxvV9Nk0vWLuxkzmCUoD6jsfyxWVJte6z0MxjGahXjtJGc23NJxTjH7
1GeK2PNTAjnNRkYapM5FNbrSZaIn4FQlCfpUrEk4xTCDQaRG9BUJ9alqPBNI0Q2mNT9tI3NSWiE0
09Kc3amUmaIQ8iozUv4VG1ItERpDTz1FMNI0HCnU0DpTqkTHDpUlRg1JVkMeo4pwNNHGKcKDJjxT
1pi81ItBDHg06mipAOBVozbAVKvNRqCTUy9aZDYYw2KdjtSN9+nd6Zmx6rtre8IH/isdH/6+VrFH
TpzW34R/5HDR/wDr5Wk9mVS/ix9UafxDGfHF9/uR/wDoArV+Hniw6fcro99J/oszfuXY8ROf4fof
0P1rM+IX/I733+7H/wCgCuX2iko80EjapiJUMVKce5678QfCX9q2/wDallHm8gXEiqOZUH9R2/Ku
B8IE/wDCXaSB0+0D+Rr0L4f+Kv7Xsv7OvJM30C/KxPMqev1HQ/nVLUfCzaZ450rVLJP9Dnuh5qqP
9U5B/Q/z+tZRlypwkdtehGtKGJo91f7zlvHR/wCK1vx/uf8AoArm5lGwkehro/HfPjS/+qf+gCsB
yPJP0NdMPgR42Lf+0y9We1eIf+SfXX/Xmv8AIV4yvzHivZvEI3fD67H/AE5r/IV41GecVlh9md2d
fHD0JVUYzSRv/pEYH99f5ijG4+1PjRTPCo+8ZFA/MV0PZnjQfvr+up2/xLyL/TSO0L/zFcUmxu+D
XZ/EqULqWnrxkQMT/wB9CuJIRuQcGoor3EdWaP8A2qXy/JHcooHwnlHbz/8A2oK5KxvptMvYru2f
bJGcjPQ+oPsa6qMf8WllGf8Alv8A+1BXDng8mpppO6fcrGycXSlF6qKPabO50/xboBLIGhmBSWM9
UbuPqOx+leUazos2ham9pNlh1jk7OvY/X1qz4X8QyaFqYkO42suFnQenZh7ivTNf0a38SaQPKZPM
A8y3l7Z/wNZL9zOz2Z6UlHMsPzL+JH+vxOQlP/Fpo/8Arv8A+1DXH6Wgm1eyRh8rTxg/99Cu0vYJ
bf4WiCZCksdxtZT1B8w1w9tIbeaKcZzE4cY9jmtKeqbRwYz3alLm/lieteP/APkTb3HTKZ+m8V46
ATgnpXuGsWya54YuYY8MLmDdH9cZX9cV4mRhlUjBHUehqcO9GjpzuL9rGfRoryLtIqNj8u2rNwOA
arOORxXQeOhpiI5pVOBU3GOTzUTCkxofa2kt9ew2sIzJM4RfqeK6z4h3kNs1h4ftmAhsIgW56uRx
n8Of+BU74d2MZ1G51e54t7CMnJ6biD/Jc/nU0/j6ymmaWXwxYyuxyXkILH6nbWEm3LRbHr0YQhhn
zy5XLyvov+CUvhjrBs/Ej2LuBFeptAzxvXkfpkVb+KulGDUrXVI0wtyvlSEf3l6fpn8qjj+IVjBM
skfhSwR1OVZCAQfY7etdv4ptI/E3geWS2G4tCLmD6gbgPyyPxrN3U+Zo7aUIVcLKjGXM1qeEZxjP
Q0x1yOKfncuQMUhxs5roR4qIRTOuTTwuc009cUjRDSKiY1KRmonAxSZaZCQaMA07dimHBoRqhrLT
T0pxFMpFkT0ynvSVLNVsMNRnrUhqM9aRSGNTDUuKjPUUGiCpKjqSoBjwKUUi04VSM2OBp4pq1Kq0
yGxQKkAptPWmjJi96enIxSDmlAxVESJVz0xUiDHJqINinbs9KZmx/wB5qkAy+Kjj45NSIfnzTIZP
91RWz4QH/FYaP/18rWLncD2rvtG1rwTpYtLk6TfNfQhWMv3hvA5I+bHXNTJ2Wxrh4p1E5SSt3M74
hn/iuL7/AHY//QBXNAmu+1bxJ4H1i5kurvStQa6dQDIBtJwMDo9cAG6AdKdO/LZonGxj7RzjJO/Y
s2N3cadexXlrIY5om3K3+e1e7eG9et/EWlJdR4WQfLLF1KN6f1HtXggAxW14Z12fw7qqXKbngfCz
xD+JfX6jqP8A69KrT5kaZfjfYT5ZfCy546/5HXUPqn/oArnm+ZSvqK1/E+o2+reI7u+tixhk27dy
4PCgdKyACWyeK0gvdSOHFSUq8pR7ntelyR+I/AyQqwzLbGB+fuuBg5/EV4/cWs1lcvb3KNHNGcOp
HQ1qeHvEN5oNyXtyHic/vIWPyt7+x966+58SeE9eiU6tZyRSgYDFCSPoy81klKnJ9Uz0Ks6ONpxv
JRnFW12Z56Oe9bvhPR5NW1uDEZaCBxJM3YAHIH1J/rWu0HgBDv8AtN3Lj+AF+f0FM1LxjFbWH9n+
HrMWcJ4MuMN+A7H3PNW5OS5Yo5aeGpUZKpWqJpdE7tlLxreJqXiKYxsGW3UQgjpkdf1Nc5EmHIPS
iEndmtfRZdHiuZW1e3mnjKjyxEeh755FaL3Y2Rxzm8RXcpNK76nQDH/Cp5cf89//AGoK4lVJ6ivQ
B4m8K/2WdN/s66+yE5MW3jOc5+9nrXM6qdIknjOk280MW071lOcnPbk9qzpt3aaZ1Y9U3CEo1E+V
JaX/AMjJEe0dK7fwP4i+zTLpV058pz+4Y9FP936HtXHleKjDbO+MdxWs4KSszjwmKnh6qqRPUfiC
P+KSmOP+Wsf868lhGTtPFdjf+JxqnhH7Bdbvtgdfmxw6g9T6GuSKYORWVGLirM7c1xEK9WM4Poei
eBfEKtAuj3bYkjH7hmP3l/u/UfyrM8ZeFZLS7k1SzjL20h3Sqoz5bdz9D+lcetyUIPII5BB6Gus0
r4iXNoiwahbm7j6eYpAfHv2P6VMqcoy5oG9HF0sRQVDE6NbM4mbDOqg5HtTJPvACvQJrnwFqrtNK
stnKxydqMmT+GRVbyvh5A+9rm8uCP4fnI/QCq9r3TMfqKvpUjb1HXOj2lt8L4b1rWL7W+xjLsG75
n9fpiuCbGM13PiTxhp2p6EdI060mSIbQruAoUKeBjk1S0278HW1rA17YX81yqguQ2ULDrgbh3qYu
STbTNsRCjUqRjCSSSWvmXtVH/CN/Dyz0wELd6kxlm9QvBI/Lav51wbFgK7/WvEfg/XJ45r6y1NpE
TYpQ7QBnPQNiuDnaIzyeSGEW4+WH67c8Z98UU72swx3K2nCSaSSVvIquDndXrvww1X7ZoEmnyNmS
zfC57xtyP1yK8rtvs4u4WuVdrcOvmhD8xXPOPeu60bxR4M8PTPPY2GppJImxmb5sjOehbFKqm1ZI
0y2ap1OeUkltqcd4q0o6L4mvbMDEW/zIv9xuR/UfhWKxwAK9J1rxV4J1+RZ9Q0vUXmVdiyKApA9O
Hrz3UjaPqE7WCSpaF/3Ql+8F96cG7WaIxNOEZOUJJp9iqDgYqFsipsc1HIKswRGaYcU45ph6VJoh
CoNQkEGpe4qNjzSNIsaaiYZqU1HQaIYQKZUh61G1Sy0MNR4qSmGkaIa3So6kqM0GiFFPpg6Zp461
AMcKctNqQdqszY5amFQfxVMOlBnIkFKOtNHNSLx1pozY5OtPHSmL96nAdKozZJtGKcgAoC/KM11P
hGw8P6zf2ul3ttqH2uZ2HnQzqI+hI+XGegocrK4U6bqS5Uzl1XLVOoC44rt9Z03wXoWtzabcW+tM
0W3dJHKhHIz0P1p1x4IsNT0R9W8M6jLcpHnzLadQJBgZIyMYOOxHPrS50aSwk9Umm10ucUMU9XwM
E1r+Ho9EvbuCx1G2vnmnmWNJYJlVVBwBkEZ61s+MNF8OeHLo2EVvqMly0IkSQzrsUnIGRjJ6U+ez
sYrDNwdS6scicFc0qrXT+ErDQdbvLbTLuC/F3IrEyxzKIzgZ6YyOKs6pbeENI1W50+W21h3gfYzJ
MmCcA/1p+01tYn6rJw9pzK3qckOKk5b2xXXR6P4c1Hw3quoafHqEU1kgbbPKCGz06dutUfDlnomo
3trp17b3puZpCglimAQdSOMZ7U+dWbIeFfNGN1rt+RhLxzUma67VtO8J6Nq0mnzQ6sWQKWkjkUjk
Zpt94Rtn0Q6xod695bKCXjlXDqB1/EelCqRFLA1VdJptbpPU5dalxlRUlibT7SjXaSyQc5WJgrH0
wTxXXy6J4ci8Mwa2YtSMMpAEYlXcMkj0x2qpT5dGYUsNKsm4tab3OOVQSAelPkUjGK34B4VnAUvq
drno7lXUfXFGr+Gp9PtlvIJUu7FhkTx9h2yP601NXsyJ4SpyuUbSS7O5zZcrxihCQwI61LLHnJqW
ziMjIkalndgij1JOBWidtTl1eiIVG5+e9W1T5BW14u8Px6K9k8AOyWPa5z/y0HU/jWLG25ABSjJS
V0ViaE6E/Zz3Qh3Y6VCyMxzjiui0vRoZbCbU9RkeKwhOPkHzyN6D8eKfbXHh24uI4H0q5jR3CiX7
VkjJxkipdRLY1hg5NRcpJc21+v5nNBx3pDjt0Naev6fDpus3FnAWMcZG0ucnkA1mbKtO6uY1Kbpz
cHuiMotRhBvArsvDei6Hr0zW7RXscscQdm84bSc4444rntUbSknaPT4LpCjlWaaQMCBkcAdKzVS8
uWx1Swko0lV5lZ7GQ6neQOlBCjrUicsTTGXc/tVswGHFR7tvWuv8MeFF1vSdTupFO5F8u2OcDzAM
k/yH4muOkyBhgQRwQe1QpJtpHRKjOEIze0heGj+lMAyKar4GM0u4fjQT1Iz8tMJzya9A8L+HvDfi
QXQEGowtborNuuFIOc9MD2rFZ/Bo4NlrX4TpWftOh3fVGoqbkrPzOYIH5VEx5rsdb0rQrPSNG1Wy
ivTb3kjCWOSUbwoByAcYByK0rPw94Wu/Cd1r/kaokVuWDReepY4x04x3qedWuaRwc3JxurrXc86P
amHpXVNP4ICkmx1z/v8Ax1H400TT9FutPGmtM0N3aifMz7jyeP0pqd3YHh2o8yaduzOVPAqMjipM
cdaawwKZCITwaYamYVD1oZaGHrRSN1oqTQY1REdalao26UmaRI6Q0ppDSLQw1FUxHFRHrQaRFWng
c01TxTlPtUAxwGacKQGlFWQx9PFR9akXpQZskHapAaip600ZskHWnr1qPntTxmqsQyUnFdJ8Pf8A
kfdJ/wCujf8AoDVzS9Mmup+Hw/4rrST/ANNG/wDQGpS2Lw/8aPqWviMwHj3UckdI+p/2BXV/C9W0
3RtX1a8PlWTbSrPwG2g5I/MCs3xj4ml0vxpfQjStJuvLEe17i0DPkoP4s5Nctq3izV9cjWK9nVbd
PuwQrsjH4d/xqEm4pHRKpTo15VL3eugaDtfxTprqMK15GQPQF66P4q8+Lk/69U/m1c14bYHxNpf/
AF9R/wDoQrpviqceLU/69U/m1X9tHPF3wk35opfDgf8AFbWP+5L/AOgGm+LLG6l8aamUtpyrT/KV
iYg8D2p/w5/5Hey/3JP/AEA1e8T+JdZsfFeoQW+qXUcMUwCxq+ABgcUO/Pp2KhyfVFz7cxmanHce
Hr/UdIt7pmicLHMSoG8YB/Dr2p3hBT/wl+lH/pv/AENQeIdQh1fX7u+tw/lSsCu8YP3QDx9RU/hH
/kcNK/67j/0E1b+E44tfWIpbJ6feaPjxSfGV3gEkxx9B/s1veGC/hzwZqF5qQaJLgkwxOMM/y7Rw
fU/pUfiLxfqOieNJoV8uazRIyYWQA8qCcN1BqLxvYJfWVr4is5pZbWYDcrMSI89CB29CPWs1qoxe
x3uMadSrWg7yV9O3n5nDxDYoB613t5/ySOx/31/9DauADgc4zXf3hz8I7A/9NF/9DNa1d4+p5+B1
VX/CzjEOVwK7rwBcm5jvtKufngZN6q3vww/lXCAlOldl8OQz61cy4+WO3wT7lhj+Rp1rcjMcrk1i
opdb3+45e+h+y31zasc+VI0efocVqeGYlt5rjU5QDFYRF1H96Q8KKzNWnW51i7nX7rzOwPsSa6F3
07SfDtpY38F08t3/AKVIIHVSB0UHPtRJvlSDD04+3lNuyjf77u39eRqRM/iLwDMkpL3doxfJOSSO
R+hIrhY2AbdjK12/hLVtHi1M2lrBdxG6G3M0qspIyQMDuea5XXLD+y9aubID5Ucsh9UPI/w/ClSd
pOJpmEXVo06102tHb8Ds9BW21/wlJo4l8ueMk89R82Q2O47Vymq6LqejHdc27BAflmj+ZPbnt+NZ
sU09vIs0LSRuD8jrxz7Gux0HxtcNLHZ6uiSwyEJ523kZ4G4dxSkpQu46oqnUoYuMKdW8ZpWT6eRx
11eXF7ctc3L+ZK/3mwBnHHaoSeQBXR+N9Hg0vVYzaoFinQtsHRSDzj2rlGDKRzW0JJxujz8RSnSq
yhN3aO6+HQ/4nd16fZ//AGauIvcm/uO371//AEI12fw1JOs3eT/y7j/0KuUvFX7VcZ/56v8A+hGs
1/FZ21f9xperKK/I3sacQSwCjcScADuT0qMkuOh4rc8KQxNqxvbof6Lp8ZuZfw+6Pz/lVzdlc5qN
P2lRQ7nSWGqjQfEuk6Ikn7iGLybnngzSckn6HA/GuX8b6X/Zfie6RV2xTfv4/T5uv65qe41Lw1dX
cl1LZav50jmRiLlPvE59K3/Goh1/wjYeILVWxEdrhvvBScEH6MBXMvdkm+p7U0q1CcU0+XVW7bHm
eKUKTz6U4c9RTi3HStzx7noXwu/1ur/9cY//AGauG0nSZdb1aKxjk8tpNx3suQMAn+ldx8K2/e6u
R1EUf82rJ0jx5qUeqxHVL0Gxw4kCwLn7px0GeuKwbfM7HrpU5UKSqOy1OTfULufT4LGSUm2gZmij
wPlJ6n1rvNDjeT4O6wiKzsZJMKoyTyvavNwxU+ua9M8N3c9p8I9UubaVopo5JCjpwVOV6U5/D8xY
J3qTvtZnD2GhT3dpqVxMk0C2VsZxviI3nIGOazrzUbq/8hbuZpRbxCGHIHyoOg9/xroLfxjevp2r
Weq391cpdWpihDncFkyMfQYzzXKNyehoSd9TOagor2b33G5qM5qRjgDimE1RkiJz2plSt1qImg0i
Mb3qOntyKYTikzVAx4qEmnk9qYRUlpDDTKfTDSNEI3aozT6jagtCjrT6jp46VA2PB6U6mDtUgqkZ
sctSKOKYtOA6UyGSdqkWowOKkHFNGbHA8Cng1qt4cuU0VdWN3p/2RmKKftHzFwMlMY+9jtWdb28t
zcRwQRPLNI21EQZLE9gKfMKcJJ2a3FQZFdt4D09YNb0/WJ9S06C2hdiyTXKrL0I+7+NYkXhmdrn7
J/aOlC9zt+zG6+bd/d3Y27vbNVYdGvptVfTfspS7TdvSXC+WBySxPAAHOaG09B04ypyUnG51vjzT
Ev8AXb7WbXU9Lmt2VCEju1aQ4UA4XvXEdDWlPojW+nvfQ3ljdQRyLHIbeQkqzZxwVHBx1FQ6bps+
q3q2sEtukzkBFmk2byew460R0RFe9Spfls2bPhPTGuNTs9Qa+0+3ht7lGdbi4COQCCcL34rpPiDa
RaxqzalZalpskEdsFKC6XeSCScL361wmoaZcaVqEtldxhJ4ThgOR0yCD3HNX18PXkeiR6vKbaG1l
yIzLJhnPOABjvg4otd81ylNqlKjyeb1Oj8C2MdnrFnq9xqOnw26o4KPcqJOQQPlqXX/Dy6r4hvr+
DXNGWKeTcge7AOMAc8e1c63hydbCC9e901LeclY5GucAkdR06jvQmgXL39tZieyL3MYkgYTZWQEk
AKcdcg0W15kxKTVJUpU9N9zqtH0u00PT9ZlutV0yaSWzaOJYbgMScEn+lZvg2wxqtjqk99YQQQy7
mSW4CvgAj7tYraRcDU/7PhEVzcgldtu28AjqCcDGMc+lWYvD8083k215p1xc/wDPGOfLH1CkgBj9
DRbTfczjJ80bU/h8+t7m740sEvdYutVt9R0+W38tPkW5UyHAwcL3q54I1eyl0m90HVZoo4GUtG0j
ADDdQCfQ81yVho9xqOofYojDHc5KiOZtjEjOQOOvFTS6DdLay3EbW11FB/rTbzCQx+5HXHvinyq3
K2Ea1X2rrwhve6/MsT+HnjvI7ddS02QSMwWRbkbQBzlvTNdjPa2z+BbXRV1jS/tMTKzN9pGw4Ynr
+NcRZaPNf2c1xDNaKkI3SiSXaUGcAkY6U+TR5orUXCSW06GUQ/uJN53HkDGKclfd7EU6ns1Jxp6S
XfoaX/CPQREG68QaVHH3KSlz+AAqe516y0rSJdM0EyO0/wDx8XjrtZ+3yjtWS2g3CySRC5sxcxqz
Pb+b84AGT2xkDtmsrIQ/MeDzVWUt2c8qsqPwQ5W+u7NzRNJS+limub2ygthJhxLOFcqOuBUnilJn
1iS7a5tZYpWKxLBMH2IBgA46VRTSJPLjluZLe0SQboxcPhmHqFAJx7mpJtKmgnhhRUmeZQ0RgbcH
BJAI/KmvivcUrqjyKG/W/wCgujJNLfoYZ7eB4SJA88ojXIPqa67xbZ2usXFrd2eo6d5yoUlDXKjP
cYPtzXKx6JI032f7VZfac4EJm+bPpnGM/jVaLSbibUfsTiOC43bdkx25b0+tKSvLmTKpTlTpOjKF
1Lz6nSWtmmr+DLe0t7q1W9guGfZJKFypJ/xBqva+GWtJ0uNXvrK2tUYM22YOzYOcACsS9sZtNuns
7lAJExnByORnIPcVY/sa6OkjUW8mO3JKq0kmCx54Ax3waLab6MFUjKS5qV5RXft3/wCHLHifXV1z
U/NiVkgiXZEG6kdyfrWAxGOuTWjLpE8WnLfme1MDEquJslmAyVAx1p76JNHaQ3JurBYZiRG5uMBi
OvbtVRlGKsjGrCtVm6klq9fkdR4Igh0m6kurvULBI5oAFUXClgc55Hase68NmWaV11nRwHdmGbn1
JPpWfDoN1LexWoltDLMgki/fDEgOcbTjk8GqdzYzWl+1nchIJUbaxkOFHvn096lL3m7nVKp+5jTl
S0T79TppdKgtfCD6cNR06S8nvFcsk42hcdz26VH/AGV9k8KS2Fvqelm6upg9wftagCNR8qj15rGk
0O6FzNbmeyBhj82VvO+VFyOpx15FMg8N3V6ZBa3enSmNC7hbgfKo6k8dKlpdzWM5XsqWtrb9DEJ4
zXpHhWK3t/Dl/pWq6npot7obogt0pK7hzkducH65rgrHTbi+WSSMIsMYzJNI4VIwemW9/TqasPok
zW0t1bzW15FCMyfZpNzRj1KkA496JJPS5nhZyoy51G+5Dqmlvpjor3Vncb84a2mEg49fSs2r9vpk
tzam6ea3trbf5YlnfaGbHQAAk+9V9Qs5dOuFhleNy0ayK8TblZWGQQapPoYzpv4krI9D8CQW+g/b
3vtV0tftMaKgS6UkEZ6+nWuA1TS5NNljD3dlcCTODaziTGPXHTrU0GiTz6VLqST2X2eLAkzN8yE9
ARjgntVWy0uTUFmaKe2hEKeY/ny7ML3PToMj86hK0m7nZObnTjT5LW21KkNsbi4jhV40MjbQ0j7V
HuSegr0rS7S2tfh5f6HLrOkLd3LOUIu1KgErjJ/CuGTQJ5tNk1BLvTzbRNskY3H3WPQEY6nHFMTw
9PPpY1AXmmpbFxGWluNu1yM7SMdcUpWkty8O5Uvs3un16F9vBUjf8x/QPX/j9/8ArVL41SztdE8O
2Nrd2t1JbW8izNbyBxuypPI9TmshvD04ktE+1aeVvN3kyi4+RipAI3Y65NVtV0W90XUWsb6JYphg
5z8pB6EHuP8AA0t3qy/hhK0LX87m74i8N6NpnhPTtRstU8+7n27o94IcEZbAHI2+9cW3Stqfw5cQ
3VrA17pnmXShoiLnIIP3TnHG7t61I3hG9XUv7NN/pX23f5fkfa/n3emMdaE7LUJ03J3jGxzxPNRt
nNbdt4Y1K+0a41SzEFxBbDMyRS5ljHuuPbP0qtpukS6sZVhuLOIxIZGFxNs+UDJYccgd6fMT7KSt
puZOaaelTOmyRl3K20kZU5B9x7VA1MaG0yn0w9KlloYetMNP702kaIZTW4p1NbqKC0NooFOFQUyR
aeKjFPqkZMetSDtUQqReRTIZIOlOyaaPSn9qaMzrJP8Akldof+oxJ/6Kqz4AQG81maLm6g0uZ7f1
D4xke+CfzqhHr2knw3Bo02nXrRR3BuTIt2gJcrtP8BwPb9aaniX7Bd6ZPo1u9qbFHX99IJDNvbJ3
kAZHbFRZ2Z0OUVKMm9kYkZ4HJx6/1r0nUtch02/0PUr62+0nUNGEN9GDhnU8Bh78VyUl94fnuTc/
2Rdxsx3NbR3QEBPsSu8D2/CpovEn2jVL251S38+K6tTa+XCwTyU427MggYxxVNNkQmoXV9zS1LRL
UaDcap4f1OS501ZEF1bTDbJCc/KWHcDPX371keGzv8T6b2HnjP5GnT6zbRaTc6ZpltPFFdshuJri
QM7hTkKAoAAzz6modGvbbTdRivJ4JZzEQyLHIE598g5H5U0nZ3Mpyh7SMl8zpb20fxRpGhahER9p
Zhp94391lGVc/wDAcn8Kk1a9W+8CySwjbbjVvLgX0jWLCj8hn8awLHX5NMsdVsbJJFgvowiiRwzR
kcFsgDJKkjgDtU39t2H/AAjP9jiyuh+++0CX7Qv+s27em37vt196STRbqwaeurWptJDYTeCfD66h
cTQx/bphmOMPkZGc8jAx3rP0gg+NNNjjlLxQ3ojhG7cFQSHAB9KYdb0yfRbPS5LC88q2d5FdbpQW
Zuv8HSr+iyQ3mtaXcW1s1rYaVhriaaQNgbi2WOByegGKaVtSJSUpRSa0t/Xy3LliMW/jS4jOLlMo
pHUI0p34/AVySu0LK8TFWQ7lI6qRyMVet9dn0/XLnUbVEZLh3EkMgyskbEna1TJf6LHKtxDptyWB
3LBLcAxA5yBkDcR7VUU0YVZRnazta/5t3Ouk2H4paY4UK8scckoH98xnP9KzLVIdJ0nVdWsZ2vnm
32ki7NgtwzHlgTk+gxxWRp3iA2/iAazexSXV0HLqFkCLnBHPB4x0Ap2n61DY3t4UtJJbK7iaOa3l
lGTkk53AdieOKTgzX6xSd3e12/ldWRoeF1jbRvECzO6R/ZUyyLuIG70yM1UmuotPvLN9PuGkiRY5
2D8bpAT95QTg02w1ewsbO/tvsd06XaCNj9oUFVByP4evvRbatY2ihbWwl3faIpmaaYNwhJ2jCjGc
1Vnduxg5Q9nGKkrq+uve5tvbWHiK4nutFupLPUnVpJLOXo+R82xvf+tc5o8UVzrdhFcD9y86K4Pp
noauW+q2NjePfafaXC3TbvLM0qlIi2QSAoyeCcZrHLlQCGII6EcGnGLs0zKtUhzRlpe+ttv6fU2d
fMreJNSabO8Tso9lH3cfhitPwrI01zeuWLTW2nyfZwTyp9vzP51nS6zbaoqyalaSm6VQrXFtIqmT
HTcpGM+4pDrRt7+zudPjNv8AZYvKUOwYyDJzu4A5zTs3HlsJShGs6vNdX+et/wArmaOnDGuxvXWb
WvDNw/8Ax8TQxGU9zhuCa5+W80iSXz/7PuUZjuMKTjy8/UjcB7U+LWfN1mPUryJnaJlMcUThFXb0
XoeBRK71sTSlGleLle7X4O5o3cUmvaTpt3Cc3Ucn2Gcnr1+Rj+Gakv7lLjwjctF/qI9QSKH/AHFj
wPz6/jWTa6y+nQ30NmjrFdx7PnYMUPrnHXkj8acNXs00BtM+yzndKJvM84ffC46belTyvT1Nvb05
JtPVpp+vT7ye7OfAtgef+P2X/wBBq2sdlL4Y0Fb6eWJTcSj5E3AjcM5ORge9ZMms2cmhRaatrOPL
kaVZDMCNxGDkbentUkmradPptpYvZ3ey3Z2RhcLkluuflp2f4gpwTvdP3UvmvkN0qQt4otVDsyRy
OkeWztUBsAe1XTjxZopAJ/tqxi49bmL+rD/PWqkWrWMF9ZzpYzbLWLYi+cMscnJY7efvVlQXj2Wo
pdWTPE0bbo9zbiPr0zQ4t6ihWjTXLJ3i3r/XkaOuNMPEF3DDvJlKRlE6v8q4XH1xU2tTpoel/wBg
wMDdSEPqMqtnntEPYd6bF4jii8R3WrzWJkkl5iUSAeWxGNwyDk+lVINQ0i3v47z7DezSo/mES3Sk
M3v8nNKzurotSp+84y1b+5f8EuXyCPwDpHkn5JbmZpsd3HC5/Cqvg6eRPF+nhORI5ikHUMhU5Bqv
Za39mtJ7G7t/tNjM3mGLdtaN/wC8jdj+lNg1a10wSSaXbXC3UiFFnuJFYxA9doUAbsdzSs7NWBSh
7SFRPa2np29TUhv9DntH0PVDLDDb3Ur2l5AM7AWPDDuP89qyPEWkXOk3kEc90t1E8KtbzKcq0X8O
PTHpSRX2nyaba2d7a3Ra3ZystvKq7gxzhtwPTHWoNV1VtSlgAjEMFtEIYIg27ag9T3J6k0RVmVUq
xnTd9+lvyZpeD5kmv7nR7hsQ6nC0HJ6SDlD+dUpkbTNCeGQFbm9mKuO6xxHB/N8/981mJNLbTR3E
LbZY3Do3owORV7xBrLa7q8t80flK+Akf90D/AOuSfxoa94UakfZW6rT5Mv2Zx8ONY9r63/lT7QWs
nw3uBeXE0MY1RcNDEJGz5XTBI/nVK31mxh8OXGkyWly5uJFmeRZ1ADqOMDb09qSLWtNTw6NIlsLp
1acXLyJcqpLhdvA2HAqGmdMJx0d/s2+YxjDH4rtrS0naSyhvU+zjfuUAupJH171upKni6G60C8kA
1S0mlbTZ3P8ArF3HMLH6Dj6D0rAXVtMiOnJHp1ysVnM0x3XCl5GJUgE7MADb6d6y7y9V9Ve9slmt
iZPNXdIGZHznIIA70WuONRQ0vdPf+vI1tRjkh8Q6BDMjRyJbWaurDBBDcg1d8WrpsOveIbyC/uF1
OC6Ro1KeWIzvwxRgx3ED2GOtZmoeIl1jxFb6xqFs7PHHGGSGQLudP4skHAPpU1xr+jXPiNtal0m8
aZ7j7Q0RvEMZbOcY2Zx7UrM1U4WaT6/oL4f1m70Dw/JqFm2Xj1OIMjfdkUxPlT7GtS60a2SS51/R
gTpF9p118mObaXZ80benPT/9Vcv/AGrYnQp9PNncCaa4+0eaJ12hwGAG3bnGG9eoo0jxHdaTp2p6
eoMlpqEDRvGTjaxGA4/r6ihxe441IpKL/r/hzEaoj1p7cUyqRzjKYaeajNDLQ002lJpCaRohlMan
1GeaC0FFIKWoKZKKeDxUYp4qkZsfTwajBp9UnYhkuaeKhFSihGbR1Pg7TtN1a9uLS/tHlKW8twki
TshGxc7cDg59aTSYNB1++i077NPpVzcfJbzC4M0Rc9A6sMjJ4yDU/wAPP+Rguj/1Dbn/ANArO8L6
ddXuuWDRxusMMscs07KQkSKQxZmPAGBUdzoivdjpvc2fDui2Ul3q9hq1izXGn2805ZLlk+ZMDYcd
veqG7SbrS5mTT3sbp2j+zMbh5EkBfa/UY4rotB1RL/xj4s1O1G6KWxuZIt65BAxjIPXOOhrnZ5r3
xBZRv9nhQadDiUoixqRJL8uFUADlsfrTWr1FJRUVZX36GveabouieJBouo6e/kKQsl9JM6u2VzvV
R8u3PbBqDR7HS7jwxq1/dWDSzaeIyCty6CXexHIHTA9K19E1S9uNbj8JeIbWPUoEka3LOuZIMdWV
+uBjqe3eqWitFaeGvGASOO5hjaFVEoJV1EpAJwR25ou7ByRck0tNfloVpdO0uTw2mv2ltMiRXX2e
4s5pyQxK5BVwAR24pniywsNK1b7DY2zxhY0kLvMzlt6g4wemM1nX+tXN/ZR2myC3s4iWS3t02IGP
Vj1JPuTW146ydfiugMwXFpA0Un8LgIAcH2NUr31MKnLKnJwXb9SvdjTLXTdMuo9K+e5SR5FN0+Pl
crgfXrmtjWItM0nUxpSadNLaAR3MgN4/G5RuYgDnAP8A+qsPWR5OlaNbSKVmjtZHdCMFQ8hK5HbI
5/EV0Hi7V57fW7mzWCJ0nsYoQREu8FkXnfjJGe2aOw42UZO1tuhRk0uxh0m+1iOB761S9a3hj3sq
qnUO5HPoB0681l37WDpZyWERhLw5nhEjOEk3EYBPOMYP41etbjW/CRluIpEWM3DWs0TfPG7qASGH
0PB69aseJ4bJrfStRtrRbKe+haSe1ThVIOAwHYHmmm72MakE6TaVrbr9UxPD9np97Y6m91ZtJJZW
xuFYTsu7nG0gfzpdOstN16f7FbRyWN86kw7pvMikIGdpyMqfel8K5OneI+D/AMg5v/Qqi8J28g16
2v5VaO1s3M00zqQiKAep9c8Ypt6vUUErU1be9/vH6Ra2M2latNe2TtNYRh+J2TcS20ggcDHtVa4b
T5tM3RWr2t15qlF81nDxFTlufcda0tFvS1l4rvY0Qh4lkVXQMMGQnkHg1m3ktxqUAv2iSOO3SK3f
aAoLfNjCgYA46dqIu71JqxjGmlFa2fTz3JvDdvbX2t21ldQNJHO23IkKlOCe3Wqd+YftEhgi8qNW
Khd5boSM5NX/AAlgeL9LP/TX/wBlNUDazXl7cxQqCU82Rt3ACrkmrvaRzcl6EbLqzSls7XRre2+2
QG6vbiMSmIyFEiQ/dzjksevYCpxY2mp6Pc31hE0FxZgPcW5curIf4kJ547g1J4rT7eLPXLUFrae3
SJ9o/wBXIowVb0qPQJv7N0XV9QuAVjltjbQg/wDLR2PQeuByai75ea+pu6cfaum17tv0vf8Ar0Jb
yzs9FmsoLuxNwtxEkslw0rKPm6hAOOB65rFu/KS7nW2YtAsjCNvVc8H8q3tP1W/0+5g0S/hjv7ST
YRbuu8hWAIKHr36e1Y2uW8Flrd7a2rloIpSqEnPHpn25H4VUHrZmWIgvZ80NFfa2q0/H1NSaLTIf
D1lqB055HnmeJlF0wAC9xxTpNIsI77SZQsstnqaqVikk2vGSwB5HWkNwsHgnSi9rBOPtc/Eykgce
xFVbS/udR8RadLcODsmjRFVdqooYcADoKlX1fqay9nFxi1q+Xp6XLVxbaPBrNzZyafItvBK0TTi4
clcZAJGO5FUYtPgstIi1O/VpPOcx29urbd+PvMzdQB0wOtaOq3d3fa5qGkRwxEz3uI2WNUOVY43E
Dn8abex/2p4LsXtlLyaa8iXCLyQrHIfHpxRfYbppylZXte2nW/42RBptrYa+XtYIDZ6gELwBZWeO
XHJUhuQcd6TS7WxuND1S7ubJ3msAhAE7Lv3NgggdMe1Q+D/k12LUJPktbJWlmkPQDBAGfUkjir+i
XWPD/ie6EUb7vKcJKm5cGQ8Ed+tEna9h0IxkoyktdenRK9yhqGm2knh+DWbISwq87W8lvK+/DAZy
rdxUlzbaXDoOm3y6W0kl48iFPtTgAqcDHc59Kn18/wBoaJp+p2q+XaLmGS1j+5BKOpA7BhzmnvqU
2neEfDs0UUT4uJ2YSRK5IDg4BIOPqKL6L1K5IqctNLLW2/minZaTYareyNaxS+Xb2JuJrVJCztKC
QYwxGfSs25k0yfS3kitVtb1JlUIkrsrRkHJw3cEAVPbw6paytq1m5t3ETXgKt8wjMm08Y557elaW
o3sGveE7jVL2zhg1CCdI1uIV2i43dQR3IpdQUVOD0s9em/8AkzG8O21pe65a2V3AZUuJBHkSFCme
4x1/Gp5P7G+0XkEmnSQLGsqRTC5dh5i52AgjHOKZ4TZf+Eu0r/r5Wo9a1c3H2qyNtBEVvXlDQxhM
gbl+b1PfNNv3rCp2jR5n3fTyLGp22mQ+GNK1CPTys15JIrZuXITYQOB3z79Ks3vhyxvPDsWo6Osi
3cUC3F3aNIXIjbOGUnk4wc1W1f8A5ELw3wf9dc/+hCo21a50PUtFvrbiRNPhDIejqd2VPsahXexu
lBaTWll+PUqeRYjwqLv7Gftf2kweb57Y+5u3beme2Km8KaNba1qMkd5L5UAXyo2yRumcERj9Cfwr
U8UW2mp4Zt73SpP9Evr5plhPWBvLwyH6Gstry30aw0+1ltZZblGW+YpcGMpI33ARg5wgB/4EafM2
h+yjGoubZJfM55wbS5KXMO8wuVkjLFckHBGRyOlbnivT9P02SxgsbRo2ntIrlpGnZ8bwTtAPGPfr
TvHMMUuqxaxbLi11aAXSAD7r4w4/OpPHGf7Q0rAY/wDEqthwP9k0r6oHT5YzSXaxB4cstNvNP1eS
9smlksbVrlXW5ZN/IG0gduetVmj0q/sRHBZPYXss8SQFrh5EkjZirNyOxA/Wtjw5BcaM3iuGVI/t
FtprBlKiRd25eMEYP41i3H23xGkEqwQxi0WK1coojUtJKwQhQABy3OPTNK+pqo2hHTX/AIJfvLTQ
dI8Sz6FqGmyRQxbk+3STyCViFJDgD5dpOOMdDXH5yAT1xzXouhavc6zqw8KeJrWLUIYjJEbhx+9t
SgOW8z0GOp9q4Cazmjg+0rDKbRpGjjnKnY5Hoemcc0ReuoVYppOOxVbr1ptLTGNUYoYx4qM0rUhp
GiDFRtTjTW5pFojpKKDQ2aCUtNHWnCpBjxTwaYKcKpEMeKeKjBp9Mhki04fWo6eOlNEM0rDWNS05
GSxv7q1VzuYQSlAx9Tin3es6nqEfl3mo3dxGP4JZmZfy6VmJ1qXNBPM9rmjYaxqenRGKy1C5to2O
4rFKVBPrxU82vavdwtBc6pdzRNjckkzFTg5HH1rKU08daLE88trmzL4k1q5gaCfVbqSJhhg0p+Ye
hPU/jTLfXNVtLcQWuo3UEI6RxylVHfpWWDTg1UoozlOV73JA7FixJLE5JPrWnZ67qljCIbXULiKI
HIRX4H0B6fhWWMNT1wDTsZczWxbW9ulu/tYuJvtO7d52878+uetXv+Ek1zH/ACGL3/v+1Y+45FOU
ZOaegueS2ZqW2uatbmVodQuE81y8nz5Dsf4jnv70ya6nuZWmuJZJZW6vIxYn8TVIGpFNNJIic5Pd
l+21jUbGLy7W+uYIyc7I5Coz9KS41K+1AAXd3POoOQJZSwH4GqB+Y49Kehxx0osTzyta5pWmq6jZ
xeTbX9xBHknZFIVX8qWfVdQvE8q6vbiaPOQskhYZ9cVQ79aeDgU7Ih1JWtdlqC/u7Fi9pdTW7NwT
E5Ump5db1WVTHJqV08bAqytKSCD2NZudzClJy2KZKnOKsmaFnqF3YFjaXMsO/wC8EbAb6joaZd3t
xeuGup5JmAwN7ZwPQDoKq5wtB+5QkiXOVuW+hpx61qiwCEahcCMLtAD8gemetUjUCtzips+9OyQp
SlLdlx9Y1KSEwyahcPERgxtISuPTFVoby4tJPNtp5IXxjdG2049M1ARnGKYWxxiiyHzybvc1V8Qa
wwx/al59fONVre7ntJRNbzywyDo8bFTVNMmpHbAABpDdSd7tsuXGp3t6gW5u5pUByFZuM+uBxSQ6
zqNrGIbfULiKIdEjkKj8qo5JpppWQe0le99S7DqV5bRypDdzRpKcyKjkBz7jvUjeINYVAqapdgDo
BMeKzSRimF/aiyLjUn3Zd/tvVluRc/2jcedt2eYZSW29cfT2qC81S81Bla8uZZiv3Q7ZA+g6Cq4I
bjFM6NQ0i/aSatcsWt9d2DtJaXM0DsMFonKkiorq8nu5vNuZpJpSMF5GycfWo2aosZpeYJu1i/Nr
erTReVLqNzJFx+7aYkcdOKiu9Tv78ILy8uLgISVEshbaT6Zqtjimk8ilY055PRsel5cW6oIZ5I9s
nmrsbG18Y3D3xxRd6je6g6PeXU1w6DCtK5YioDmo8880WLUmaUmu6zKqrJql46qwZQ0xIBHQinHx
Nrx/5jV8f+3hqyyPQ0zpU8qNFUn3NFfEGswbzDqt5GZGLybZyNzHqT6mobrXtXuoxHc6pdzR7gwV
5mI3A5Bx6g1QZs01ulKyNFUn3NG68R6zewNb3OqXcsT8MrSfeHv3P41Se+unso7JriU2sbmRIS3y
qx6kD1qI8CojRZFc0n1EJpjUpNIaQ0iM9aYae9RmkaIDTKKRulItIZSE0lDdKGaCZp4qNafmpBjg
afUdSVRLHU8HFMHSnLTIH5p6nNRCpAcUIzaJVOKfUQqQGmZtDlNS1EKeOtUkQ0PHWnUgopkMkXIP
WnK1Q04ZpkNEwqZOnSoFPODUoxgUENEh6UgpoyfpTsVSIaJKfUIbbwakDChk2FDMtG5mo3+1CsM0
E2HLJt7U4HnNRAZapS/amS0PDU8EEVCG9RRv2/SgnlJWHQ0BiKbuylAPFArEm/8ACozzTO9LnFDC
w4HFHfNNyRSFqB2Je1RsaZuNIWzQOwZJ5pGxxil7U1qVygHB60wtlqCajpXLSHFqBxTD1pc/LQVY
cTUXfmjk9aRqC0hWqM0pNR7qRSQhJFMYk08tmmUmWhhOKYTSnrSMaRohCc0ynE8VGTSbLSBulR0/
OajJpXLQ0mozTzTDSNEJTGpTTTSLSG0jHinZppouWhlPU5pmactSNj6Wk7UCqRJKKUHmoxyaeKZD
RKKcDzTKWghokBp2aavSndhTRDJRTwaiFOWqIZMDTwc1EDin0yGSEYoBpm7HFKGxTM2h54xUkbbj
1qLI70/NCZLROTTg1Q7iR0pcnFNENE2c0vGahzinA8ii5NixkYzxTA1MznpSYoJsToacPvZqLdtI
qQHPIouKwhPNITxQSDSHgdaoVhQ5FLvJ+lR1IFFAWJaYx5pCeKaelArDsk9OlJmhW60hoCw8HIpm
aTPFN3YpDSJAcrSFh0pgJwOaSixVhWximg8e9ITSMcd6TQ0hHNMDZ4pTjGaZuouWkOJxTc8UhNMz
SKSFJppxSE4ozQXYKiJpzHIqM1LLSEzzSNRnFIaVy0hpNRk8U4mmNSZokNyaSkPNJUlpBTDSmmmh
lpDWpKKQ0FiUynGmVJSGU+mU+mymPBp4NRinii5DFHXFSVHT1PFUSx6mn1GKcDighokBxTg3So80
ooIaJt1OFR05TzVJkNEwpQ2OKjzS5p3JsS7qcDUINP5pkNE1KDzUYOaXdQQ0Sing9KhHQU+miGiX
+VITTA2BS7s03qTYmQnFL2qIHilzRsS0PzTtxXpTAR6UFsUCsSbzSFiajHNPHWiwrWHq3HSl3Zpm
aAadwJA3rSMabTe9MBcsOlLvNIOKTPvSAGJoXtSE+9IMmmOxIabuxTSaaTU3BIUvmm5zTN1GaRdh
xbIqKlyaa1K5SQuaTik7UlBVgNMajNNNJlpAKQniimGluUkBNMNB9qax4pFpCMeKYWJpKKRokFMJ
xSmmmgtISmGnGmGkUkIabupxNNPNBaGk5phNOppqS0JS0gooGPp4NMpaaJZLRSCk700RYlpwNRg8
in0yWiQHNKDzUYNPBoIZIKeGqKnZoIsSZp2aiHJHNPFUS0SKal6VCKduxTIaJKfUQYU7NMhoeKcD
UYpwNMlokzyKdUeaUGlYmxIDS55qMGlB5pisSU2jNJup3FYlFG6lt4ZrmTZFGzsMZx0GfU9q1h4f
nkkmthhruJGdfJdZUk2/eXK9GwDj1xjuKVxqm2ZG6jdXR2Ph62uNKs53nYz3avtCc7HwfKTHq+1j
knAxXPPDJGSJEKspwQTyDSuOdGUd0Af2pC1CKWBIGce9dJD4biewkEsrxXaW/nMH+ULIclI/Rtyh
iCDxjmhysKFJy2Ob3cdKaWrUXR5BY2k0gKyXal4g7CNFQdGZm9ecAdh7iqF5ZXFjM0dzHghiu9Tu
QkejDg0cwOlJboiJoPTFMyKNwppkpAzUwmgmhqVykgp3aowcUZpjsOzUdBNFIaQUlNzTSaCkhDQT
mkJpu6pLSA00mhjTSaL2LSCoieaeTTTyaktIbTSaXOKaTQWhCaaTS5pjUi0hCc0UUhoKGk02ikNB
aQ2kpaaaktCU6ohT6BtD804UylBxTRLRJSg02lBqiGPzTgcUylFJEkoNOBzTKWmQ0SZpajFOFBLR
IKeDxUfel3UXJaJQ1PzUANSA07kNEtOBHrUQOTS5p3IaJN1O3VFmlzTTuKxMGNKDUYNBOKZNiXNK
p5qEHNPzQS0SZpd1RbqcTQKxvaP4qv8ARLV7aBIXikbcwdc5GCMfTnPOen1qrDcr9mRmuxHJ55Ji
jjKMoOPmDjgDPb2FZeaM0rF870TOth8SpBPDFqFlFeWyTLMVDfMSAww3OG+8T260XfiHTfInsrW2
uG0+Z9/kSAKyHHZufr+Arkw1KDilZD9tK1jpLPXrO30/+z2hmhtpGV59gEjSEHnnjAP6YHpVm58T
2b3MsWk6clpbSqikTHJ+RWH03HcRkmuQJzQDinyjVWSVjYuroSi9Zr0722gI6mRpDkEgN0AGP0FW
LzxXqF7oy6U6QLbqqgbEAIxnOABgZz2rA3ZoJ96LE88ugpakphPNLupk2HZFITTc0ZpBYM0uajJo
LUx2HFvSkpmTSFqVyrClqaWpM0hqSkgLUzNKaYTQWkLmmE5NJSGgtIWmmikNIaG54pDRSGkWMpOl
LQaCwPSmGkbrSUFJBTDSGiky0hTTKKaTUlJCU6mZpaBtD6dTB1pwpoli0+mUtUJoeDT6ZS5oIZID
xRTRTqZJJSg1GDin0EtEtGaizTwelBFiTNOzUdLQTYlBpc1DTw1MlolBp2R61FmlzQTYkz0OaXOR
UeacDxTuJocDTwaizTgadyWiWkqPdS7jimKxIDS7sVDupwOaQuUk3UbqjooCw/fS7qjpN1AWJC1J
uJqPdSg0x2H7qXNMzSZpBYeTTc4pC9ITSCwpakzSUlBVhc+9NJpM000irBRmkzTS1BSQpIpmaTIo
zSuVYKSjdTSaCrCk0yjNFBSQnFR0ppCaRaQZpCaCaZQNIKTNFMpNlpAabSk0lSyhDTaU0lIoQUtN
FLTGOFLTaKBWHinUylzTuS0S5paZS/jTRNiQGnVHS7qZLRJRmk60tBJJRUdPzTJaH5pc0ynZoJaH
A0/NRU6glofupQaZmlzRcViWlzUWaUNTJcSXNLmmUZoJsSbqM1HmnZoCw7NLmo80bqYrEhNKDUea
N1AWJM0gNMzmlzQFh1JmkzRTuA7NJmmbqN1AWHZozTC1NzSbKsPJoyaZmkzSbuOw8mm76aTTc0hp
Ck0nNJSE0FJDqaWFJnNNoKsKTSUVHmgaQpNJmjNIaTZYuaYTSE0h5pXGkKaaaKZRctIKQmlppNSU
hDSUE0UFAelJRTTUjQUoptApoY6lpM0UxDqWm0opiHA06mUtBI7mn0zrRTE0TU7NR0lMixJTqjBp
9MTFzT81FTg2KCWiTNLmoqeDQJokpajzS5oJsSZpaZmlBoJsOpc02jNFwsPDU7dUeaM+9FybEm6l
qKimHKS5pM1FmlouHKS5ozUe6jdRcLEmaTNR0UByjy1JuptFFx2FzSE5opKQ7C0hpCaaTQOw+mmm
5ozSHYM0lFITQNIWkzTKKCrAabmnGm0XGhc8U0mhjimVJSQUUU0mgpATTaM5pN1BVgpCaM02kNBS
UUUFAabRSUmMKWmU6gYtKDTaWgB1LTaWmSOpabSigQtPplFMRJup1RU/NFyWh9LuqPqadVXJsSZp
aip2aBWJKM0lLmgkfmjNMzRmgViTNKDUWafTE0PzRmo91OoFYdmlplLQKw/NGaZnmjNAWH5ozTM0
ZoFYfSZpuaM0x2H5ozTc0maQrD80lM3UpNA7C5xRuptJuoHYdmkpmaTNA+Ufmk3Cm9qbQOw/dSHF
JRmkMXNJmm5pM0XHYXdTaTNFBVgplLmkpIpIKQ0maM0DCkpD1pDSKSCikoqB2ENJRRTYxKKOKbQM
SnCmDpS0FMmiRZJAryrGD/EwOB+VWfslv/0Ebf8A75f/AOJqjTqLiLn2SD/oI2//AHy//wATR9kg
/wCgjb/98v8A/E1TpaLiLn2S3/6CNv8A98yf/E077Jb/APQRt/8Avl//AImqOaWmJl37Jb/9BG3/
AO+X/wDiaX7JB/0Ebf8A75f/AOJqlS0riLv2SD/oIW//AHy//wATS/ZIP+gjb/8AfL//ABNUh0pa
Yi79kg/6CNv/AN8v/wDE0ptYD/zEbf8A75f/AOJqjRTEaH2WH/oIW3/fL/8AxNH2WH/oIW3/AHy/
/wATVLNOoJLv2SD/AKCFv/3y/wD8TS/ZIP8AoIW3/fMn/wATVKii4i99nh/5/wC3/wC+X/8AiaPs
8P8Az/2//fL/AOFUs0u6kIu/ZYf+f+D/AL5f/Cj7LD/z/wAH/fL/AOFVM0Zp3JLv2WH/AJ/oP++X
/wAKPs0H/P8AQf8AfL/4VTozTQtC59ng/wCf+D/vl/8ACj7PD/z/AMH/AHy//wATVPNLmmGhcFtD
/wA/8H/fL/8AxNRSqsb7VlWQY+8oIH61XzS0A0PzRuFM/GimKxJSZplGaAsTQ+W0mJZDGv8AeC7v
0qcw2X/P6/8A4Dn/ABqlxRmgaRb8my/5/n/8Bz/jS+VY/wDP8/8A4Dn/ABqjmimOxd8qy/5/X/78
H/Gk8iy/5/n/AO/B/wAap0pNSwLfk2X/AD/P/wCA5/xo8my/5/n/APAc/wCNVM00mkwLnk2X/P8A
P/34P+NJ5Vj/AM/z/wDgOf8AGqeabT3KLvlWX/P+/wD4Dn/GjybH/n+f/wABz/jVOkzQBc8ix/5/
n/8AAc/403yLL/n+f/wHP+NUzSZqCi75Fj/z/P8A+A5/xpvkWH/P+/8A4Dn/ABqmTmm0DsXTBY/8
/wA//gOf8ab5Fj/z/v8A+A5/xqpSUFIueRY/8/7/APgOf8aPJsf+f5//AAHP+NUqKALfkWP/AD/P
/wCA5/xpPIsf+f8Af/wHP+NU6bSRSLvk2P8Az/P/AOA5/wAaTyLH/n/f/wABz/jVOimykXPJsf8A
n+f/AMBz/jVeYRI+IpDIuPvFdv6VHTKkLBRRRQM//9k=
', '2016-12-10 19:50:12.8191-04', '2016-12-10 19:50:12.819129-04', 2);
INSERT INTO negocio_negocio (id, nombre, pagina_facebook, status, logo, logo_ba, date_create, date_update, tipo_negocio_id) VALUES (3, 'ttutyu', 'ghjghhj', true, 'logos/4b3ef79dc4cbbaffe44a3efd9d60a207.jpg', '/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0a
HBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIy
MjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADWAOwDASIA
AhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQA
AAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3
ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWm
p6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEA
AwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSEx
BhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElK
U1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3
uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD0uSUK
TkmqctyTkCqsk7MeWOPSqzzYJxXJyXPTo0Gyw8g6k1Vmn4wDmoXkZuc1XZzznmqjA9CFGwrNkEmo
mIbnNI7E9OKjCkdXrTkN1THFQQPzqBoflzg5qwFzxnmgo2OpoUWWlbQotEfTFRshHuKvlCepzioW
j9RirjEdrlMpk1GUweRVooR60hU1aiHKVDED0FMMPtV0Jk9KPLJ6CnaxMkUjF7U0xZxWiIGY8ilN
oT060zKRlmL2pDBk+9aT2rAAYNMa3K00kYyZmNAO4phgHYVolMcEE1GyDPTFOzRF2ZxtgfWontiO
gzWqYqBADyRkUDjqYr2754WsorcPetKpxaRhVII/1hbPzD2Bx+tbuoSI9w1opBjiVZbtQxDGNs/K
oHXgZPtx3rLNndXGmzRB1UQwIrFgcuqyEow9CQMHNJvoTKTcrImFuyHlcGnCJj2zW3PbLvbHrUBt
wD0qrHRbuYzRMGI20GLJwBWq1sNx9aT7MM54osh2MvywO3NJs9q0jb+1ILbjpRYVj0SR9xxzio8V
Y8guehxThZZ6E1g4pGkYxRTKHvULoTWmbUgc1A8BBORTVuhtGUTMZDjmmBcsBgnJx0q+0PPasnWr
n+z9NZw+15GWNSCqtzndtzxu2hsD1xV2NXJRi5PoZEeu48SqhucWZmNokK4ZZG7uSDkEHPbGMetd
Pcz21o0aTyrG8zBUVs5Yk4/mRyeK4DRbFT5t+fN3Rt5ChucKFOAPq2VJ6Eqa7K0UyQ/bZnMr3Kwx
KjEnac5bHpliTj0UUoxucdGUnFt9TQMYHaomiDdauFQxJ4OaQpntVqOp1ctiiYcjGKYbfjgc1pGP
I6Ugh9qq1hSZn/ZB2zn6VItuF7Ve2DHFOWE8YB5qZGM2UhAP8ilEPHA/StJbcA+tPMC4yABUXOWU
mZJgyeaieBcEVsNEOeBzVd7cEHBoTM3Ixntwc8VA8JHbj3Fa0kWOMVAyYPStE7iUjMKAMFYhS3AH
c8dqq3DLY2slzcsuYYiWkOVDDqeOcZOPWtWcmGGSUI7hELbUGWbHOAO5rm/EunatqPhZxGkiXDsG
e0txuJBP3WYnLYzkgcZHSiTsglPli2tzAMkPiNodXXTGWWFgCqzjc6DoxDAAjOQOecEVqWmopeTT
W6yKJN8QwylW2rlipU89cDvxnk9azdGjmsNFiRdMLXR/dxvPCAAxY7gByWwdo3H0wAeanXQ10WZ9
WvLmaSfBEzxgHDEEkAYOQNpH4H0rK7MYSkmpfedR59o7FBcw+Zk/K0gB/I04wcZAOPXFUbM3N+y3
MdwZFwGjiUq2FYD5mO3AGOq5BGOK1baxe3DNI4LMBuWNdsYI7qvr79+K0TO+MubWxVMHt+lBiHTa
K0TAQM4zmmGAjgg4oNFEofZwegNNMPNXzFjpnj1puyrQ+U7yODdjFWVtwBzgmnRfKMYqXdXDK9zh
lNlSSAAdKrPCDxitIjJqJoQc8HNVF2KhNrc4XxVrQ0qSKxt3jS6mAkLyH7qbtvyjB3MSCAMf0rA1
DTdW1rRri3uZ3kmV0kjjG1toGCpyBwWyeDj0NXfiNpVwNZ07UijG08s2zurBfJcltrFiDtU7jz3I
A4yDUWneIxHaiSa7ZQ2IFmMRZZZMfdZmxtZRnBHGAc9hWqZDrOUpRm9CazZNJ8PRwCBbYIpiZMEk
NgsSCezckDtk07Q7lrjT47lB+5hXzA2Cw3MAoOB1Cgs3HtXG6pcTvNNHJqDTQu2PNVQxZSCpIA4O
MnngZ+tekacsNlZQ2dlp97EI1CqJ7dlOcdTjOT9PpxWkXdm1KteXKtEkSaddNcMQVkEbbinmqVbj
qCCB7HPvitALkUy3sX80StuZtuMsMHJxkn8gMdgKui3YV0qNzqdSy1K+Me9KEyeKnEGBzmpEj2jF
TKNkS6lyFIQF5WpkTFShPTpTgOPWuaWhnKVyPYAOlJt5wR1qbHHAApCvFZmMpIrOnPT6VAVzyKuM
uOT0qAqBwKuJhJ2KrxgjkZqu0GT0wK0CvFN2cEgMQBngZNXFmPtGjIlsw0MyeVIRISCFf5iGwCQc
jb36elRvpxezkshJcQgKI1kjf5wBjDBjn9ea1JFUTCQzsgRTviZgo+YjBbPQjBA59etOKOrsXcKp
ZQgC4PuMngg+3vVtoaq30OE8S6BPrk+mXtjrEUUUZCRfMzGSQtjKjoeAc/Q9q1dM0NtKlZ5r2W8k
b5VVYkjUD7u7aOpAJyc/xHjJrpjbwPMr+WjPFkIxTlcjnae2R1xTxAjMGKKWAIBKjIB7Z/CsWknc
1pqPNzMzordEjCoiqg6KqhQPwFSCAEdK0RbgnpTvs49OlDZ3xloZZtxjpSG3GOlav2cHoKQ2y45x
RzWNIySMhrZc5x1qI2Yz0Na72/oKj8k915q1IvmTOhCsKcQe1WCuTj+lMK4zXOpXPIjUvqMAp4Ht
SDjg05T2FDixyfYbJEkiMkiK6sMMrKCGHoQetZsWh6dZZNnY21uT2jjCj8hWtQUFOKaYlKxgahpI
v7WW2DmB5BgTIqkq3ZsEYOPQ9q5bwbFreu6RPcz+ILpbmK6lgGLeFo2CYAJUrn/x6vR0izIDjPI/
nXH/AAtiz4cvfX+1Lr/0IVopbCnU99Eltqd5p2rwaTrsUSyXRIs76AFYZ2H8DKclHxzjJB7Gt6SB
lGeoPpWd8R7QS+AtVnyEms41uoZO6yRsCCPfqPxqe81uUzWVjY2a3GoXVuLlklfy44Y8DLOwBPLH
aAASSD6V0Qq20ZCrPms2PcqpAIHPvR0rlLjWY7t9Mn1DRyHg1YWbsJjthnLFAUIHzjvzjAPrVq48
Qzrp51G3s4pLAXi2sbNMVaUNKIy6gKQF3Zxk5OM8VUqsWjojWjbU6RFLMBg4q0kAIGVqSOAKTjsc
Vnz6pctrS6TpttDNOkK3Fw8zlViVmKqvygksxDY7ADJ9K5JsmdWzLjwYHAAFVWXbkmue8KeNbvxB
era3thFbmS3lnjeNyQ3lzGNhg/zzzg9OKs3HiKKODXrm7Ty4dKuDBuTlpBsRl4P8RZwoFRHzIVaL
VzVONvNU7ieC1hkuLiRYoY1LPIxwqqOpJqhb3+qJq9jZ6lZW0S3ySsggkZmhZFVir5GCMHG5eAeO
9J47tZR4G1hrd4lK2zGTzFJyn8QGDw3TBPFap2RhOqjWCgjgbgehB61Su1LqHeKUmOUeV5JO5SRt
3EZwwG4nByMDpms2LXb+D+xI54rSdby5S2neFHTymZW2qAxOWG0lue4x1rSlv3HiuLSBGnlvYNd7
+dwYSBdvpjnNCaOdzuyxLEht3yFYKpY7lBDYGeR9RXNeD7fzdIttZllnlvdQgElw7zsysxYtwpOB
jpxjgYrpbuOb7DOsLRrLsYIzqWUHHcAgkfjXG+HL+803wRo2fs9zJdLHb2MCIyHcdxJdiTkAAsSA
OBjHNNvUqLV1c7AYqVFzjA/GsbTru/8A7Zu9MvltpGhgjmWa3RlUhyw2srE4b5SRzyK6aCAMoODQ
3dG8aiWpHHCzHAB+tS/ZW9c1eiiAHSrKQ5U1jOVjX29jJNsw6VGyEHJBrWePHXFV3QEEVCldmsa1
zMdO+KiKc1ddeSB2qAjmumJupm7IBk46CoSDUz88DimlfasIRaPOjoRbfWgLUm0Ube2K1L5hmKcB
x15pduOtPAHpRYlyI3kjt1aeaRI4k+Z3Zgqqo6kk9BXAfDnxBoll4UunvNYsLcNqNzIPNuFUlS+Q
QCckEV6FJBFPE0M0aSxONrI6hlYehB4IpsOhaTbsHg0nT4mHRltY1I/ELSt2MpN3ujjdVvX8eKmj
aTFKdFeVTf6k6MkckQIPlxAgFiSACwGAO9O1G0kt/H8sr6kdPtL3T4o0lVVUs0TsWjV24U7WDdMk
Zx0rvWiwBkk4HGT0qtPZxXMbRzwxSxtglZVDKSOhwRimkSnfXqee+IYbMeHtEXTgv2Qa3aBGAOGz
Kctk8tk5O7+LrzWOb21g+GWlafLNHHe2t5bwTQFxvRo7gbyw6quBu3Hjkc810/jC7tLyCwtLe4il
uLfXrGKWFDlo235wQP8AZBP0HtXWG0t2klc28TNINrsyA7h6HPUfWk9WXfmbsR2l1FdoZ4SXjLMq
sB8rYOMqe49++K5uC6h0j4j6q17NFBHf2NtLA8rBVbymZXUE8ZG4Njrg11oUIMBdoHQDoKhlhhuJ
IhNDHJtcMvmIG2tnqMjg+9DVypO55P4PvbTT7/TL28uYra2lsb2KOWZgis32xm25PG7bzjrirM5O
tWHjCHThLJOdQhvI0ERDMkaxMTtYfxbTtyPmNbvge6tB4WMc7xAw39yrCQA/MZ3K4z1Pp+Ndb92T
phmxk+uOmTTjEwlJqJhabNYyXcVxpNy+tX8yhp52fcyQ9QpIAWL5sfLgEkHIOM1c8XW1zP8ADPWl
e3/0yTT5CYo1JIbGcAdcgfyroIHVUZgnJJZtqgbj/U+9Z+k6teTytaXlrL56MzSyHaojU8qvHU4q
nE5JTdzlPEWqadd2mgXdrMj2djq9rLdXEa5jjUxtliR6EjcegJ5PWqx1KyuviPaT2jyzwNpT26Sr
E2yR/OUnaxGGAGSSOBg12qaebfUbi5juZFimO5oBghmxgliecegHFOlck8Fhxjk04xJUyi65RhwM
qfx4rzqxspm8E+Eb3fcQpYOWuGgXMiRMrIzAEH7uQTwSASRXppTcKpR3cFxfXNlHNm4tlRpVAIKq
4JU599p6elOUToi9DO0RtPIlGmq0kJYM9ySzCZz1O9uXIAGT0HAFdDEMAUxI1JwSzHtmp0Q7sVEt
EW5E0Z7CrIYBOvNRxxnPJqUoAMYrKSuy1JPQruckioXHBqd15NREURibxkkUpUIyR3qvtPpWiUzU
fkCt47G6qGi646UzkdRU7Ln/ABqMg9ACahI4lMjNAPHNO2Hv1+lLsxjIFUkVzIaAOtOC7uetP246
Y/Gud1vWpLPXNI083ken2975u67dFILLt2xKW+VWbJOT2XA5oeiJcjp0X5QAKlVQBXDnXvEUMNrp
t1ZxQaveX8lraSthkaBV3G4ZVOMhR90EZOOgzVeXxHrWn6jd6OLuLULiS6trSzvDCECSyqzSB1U4
bYq7uMfeANTchu56Ft9KRkBGK5bRr3Ux4y1PSbvUWu7e2sba4jLwpG26QuDnaBnhRXR3S3MtuVtp
BHKxADkZC88nHfA7etDZKZTXw/pSak+orYW4vnOWnCDexxjOfXHGeuOKuGLACgkAV53BrfimX4dT
eK21eBXtIZZBbLaKVnWN2U72PILBf4cAe9a93fa7pWp+HZrnU4riLVrxbaa0FsqJEGjLgo33srtx
lic56ClcalY29U1Kz0mFXuXYuxwkKKzySn0VVBLH/JxT/OUwpMQ0WVD4fCleM4bPQjv6Vh3Wp6hq
Go6+1pdtZ22jRGINGis005i8xtxYHCqCo2jGSSSa5q51u5vdH0S31PVBp0N/pEdx9qKKq3NySC0Z
YqQuF+YKANxPtincrnOui0DTY5lljtY4mEhlwgwrMeuR05Jz9eaq6rDfJqtlPZB5Cd0bIB8uM5O4
9v8A61c9omua5D4X8q+RRcqxRZom3Kqk8c+uMfTI6VYV76CDS9Ht74276xLPM10nzNHFFGGYDI+8
xwMnoM9afMraESlodB4aa5upr65uZSTuEXlg5VSMng5xjBxWoL6M6p9gjTdIsfmSOGGF6AKfc15b
Hq+taYuo6Vp19PFbLp0upJcNCryptDKVVuAqkqpywYjBXvxrXq6ovhuDX9Su4ri7v4rWO2tBAqLF
cTFQCSvLgBgcHn5TzggU1J7HNKN3c9CfAPWqszKzZByK51Z7+18ZRaX/AGpc3MLaRJPi4CsDKsqo
GwqjjBPy9KzbCPVNRj8QeZr99GmnXs8EBhWNWJWNWBZip3Lk8LgDHXNa81kEY6nYcEHPSud0wD/h
PPEXOP8ARbH/ANBkquNdv73wt4YaNlh1DW/JjadEH7sFC8rqp4ztU4zwC2e1S6VBHYeMfExknnkj
hsLOVpJ33ttVZSecZPAPWolJX0N4rQ6hBk5H0q3CmFOe9cBbXOp3llo2oR6rcjVtQnhmTTkceTHb
MwLKyYzhY+S7HO7p6V6QiAdOlRJ3HZAi4pzUE4ppapsNKxDJjpgfWomXLZqY4I7D3pjDAPrVRiNS
sRHaOKMCnFQOeKTj1q7FKoXtuQabsOakANLjPes0YojCnrTtmRUqjnFAUjNO5SkRFPxrA1x7XebL
VtKa70ueH5mWBpwHDHhkUEgYwQwHXI44ro8H0xQVyetLcdzyi18N3CRWl99m1IaVY6nMbaz3utyt
lJGFOADvwGBYLndtJ+lbmpwBotFv9H0a4jsdI1JJ/LWAo8sTIyyOsRG47dwPIy2DjNdyBt6D8acF
y3v15osFjhtPurpvHWp3iaZeCO8tLWC3kmhaNGCM+5mJ5UDcDhsMeMDmu7iG0rk9D1oVQCcgZznO
KOrUrBc85h03UP8AhRN3pwsrgX72dwgtvLPmFmlYgbeuSDmtnxZa3TL4XuobWeddP1GKe4SGMu6p
5TKSFHJwSMgV14AHQ0EDHIGKLEN2PO7ePVbNvFUDaLey/wBsu1zaeUAw3PEI2SRsgIVKgnPGDwSR
T9Je4j8NWek6z4YvbnT4rGCHy1tRKzOqBXV4y2du4fKw4PPTGT6Dgegrk9ZuptO8U21xGTIGjCmF
VOSuTuHp1yfwpWsCdxPBugy6f4V/s6+tfKjeeZ4rSRgxghZyyRswJyVGO5x0zxXKeKruW417TLTS
Hdp7WdXtFtNu+CNQVkcK5CtkZU7iFbPfpXZ6FqF/qurTXb/LZqrIqBhhTkEZHXPvXM6/osun+LL2
+Wz/ANA1C3gjWdIyywPGzblcKCVDbgwbGMgg4pvbQm+tiRXsL6w8RafE93L4ivtOmUx3caI0iBGV
ViCEptDNyFOctk1d1nSL+98FafbW0OdRshZXKQOwXc8W1mQk8AnDD0ziq2naMl14t0u/sYrn7Hp6
zPLdyxtGsrugQRxhgGZR8zFsY6DJruQox0FXFXJ6nCvHq03jSDVU0S6jtzpslmnnOilZGlVwZAGO
1cAjIyTg4AyKueHtGvbWLxJHdw+Ub3UrmWBmIwyPGqq2ATgEg8HmuwCL6D8qztZ1aPRksC8DS/a7
2KzTaQNrPnDHPYY6U27dS0cPa6XriaB4XEekyi58PtD5tvI6K1xhGikEZ3YwFJYFiM9K0bax1eXx
Tq91JpEkVpqdnBbq8sqfIq71beqsSGKt8qjPbJHNd2VGOn6UgAA6VFjRI4/wtpl7oNjbaRHosMTR
ERS36sixzRr0fg72crgbSAAcnOOvXDIAzjOO1KTjoBSE5oSBsa3WmkEkY6d6cQPxpDxVJEuQxlzw
BSbSeoqQU4LkcUzLmaZCY8jpQIlA6CpwnrS7fYUy0xwORQOtJR0rPmGSLwaC1IGx1pDg0wHE0dqb
wDzRuyMAUr6jFCkjIyfpS4KnmuQiB1/xlrdrcT3CWukJAkUMMzRBpZELtIxUgsQNqqDwOTjmuW+F
FxdpfwW817cTwzaItyVlkLKr/aZUBAPT5RjjrU3Hc9ZLD0pBu4yCMjjIrzi51q707QfEjwXflXMv
iNrGGeYgrAJGjXd83ACgsR2ziruh2ljB4shudDvJn0sW7wXVxLO8kd5OxXywpYkM42sWZeOQPYNS
QmdJ4m1pvDvhy81VbcXBtgp8ouVzucL1wcY3Z/CtkgjqDx14riPidBZ6h8OdTnIimEKrLEwO4Kwc
LkYPPBYVmnRrrw7rPhCAzokZ1CS3WK3ZlRkMUjMzAsQzMQp6cYAFO5LR6Tg1E8KGZZTGpkVSqsV5
APUA1wPidNTvfFur2OmSt9ofw/CyIJSm4/aW3AN/CzLlQ3bIpfC8+n3fiqWXTpp9MMNqIrrQJ7fy
3Vt3EuScNj7u5c57nmi/QhJnbwWkFo0ptolQyuXcL/E1Sgdypz1BxWN4rt0ufDGqLIWCpazSbVYj
cVjYgHHbODj2FcLf6Rb23gXw9q8ct0urzS6crX/2hjNiUqrANnAXaxAGMDjvTvZha56gwyTkkHOC
T2qlpdzeXdvO17YNZOk7xorSBjJGpwsnHQMOcHkVyk2k2mi/EPR7TTEe1hvdPvRciORsylNm1mJJ
ywJJDHnms6z1OXQfCvj64szt+xalMtopJZYzsjUHnsGbcffNVzBbsemIAzFcjKjJGeR+Fcx452+X
4fXI3DXrI7c8jLEdKw7PTbGO703UNEhkkXTY5Z7/AFUhh9u/dNmMs3MpZvmJ+6uODk4qj9hgPhDw
drM8aS6pf6tYXdzdMMu7SsWI3ddoBChegAFQ3ctLU9QmdIQGkdVBO0FmCgnsMnvTJJY4V3yyJGCd
oLsFGfTnvXIWNla+JPE3iJ9Wgju7fTp1sLW2mXcqDyw7vtPG5i2N3XCgDFcrpq31lqt/JcadPruk
aRPPpkcagSzWyAq6yBH4kwrBD/EAo60rl3PWSwPekrnfBUdjF4Ts007U7jUbTLmOedcOBuOUK4G3
ByMHpiuhNNMLXEyM+9B5NKAfekJA781aBxFHFSqMCoQ4wPWneZzjNMhxsSk4FNz7ioGkGetNMvP3
qpK+5Ldi03yIWIwKrmdNqncOc8Z6VVGovLEwLKWPTrmqzvMxyynOOmK41I6lC3xGh9sUsVHP41Is
pKF3BVR+JP0rGNx5CkMMFhhjtztq3aXV1JGxClk/hY/Lkd8GtlJFSp6XReSVpHI2OuBnNQXN8lox
QvufuRwo9vrUklyYbdHiKMhOGGenrzVGee2YCVo4cL1VmyzH8KSldkRjdmFH9vs/FWo6pYWyXEGp
wQpLG0wjaOaMFVbkfMrKQDjkFelc34GttTsdL0jW7G3tboTaY1jLFNcGHyytxIwYHa25eSMda9Et
rlJXja3hYJEcKFXC56Ek9DXn2heHvFul6Rb2kbR2zMgiaOZwzQjzXYyKM7c7WHGTnPtRJWY+WLlY
0o/D2qXthrVlezWRa81A38EsKll84FGXKtyFUoQRklgxPFdGLG81xRDrltb21vGVZYLednYyKchg
4C7VAzhRzzyR0rN1PxFBoup6fbz2ci29ypzds2FRgcLntg859Mg0/S/EFv4iutRighdbeHCCcvtE
u7Ibae2McHr34qHbYp049CbUNNste+HVzpGi3McdtLAYLeR1IXcrDk8ZOWU5PvmshrzV/Et9aymG
2ttV0fUTcLZylvK8vyyhVpQuNzbiw44AAxWnpWhWmjXk81qkshdSsSSOWWCMkEqoyc5IJLdTWnGJ
WfasLHcTtVRgE+taRi2tTCUWjDfRPEj+Kf7cbUNKgkazS3MKb2VQspcIxIBZWDEFvlIPQHFXbbTd
RvPE1treoLZRmyt5ILeC0d3LeYV3MzMq8ALwoHUk1otZToqmZwm7nYzAtV9GSCBXt9gjYfM59e9U
4pLQhMq6zBc6jomoWUARJri2lhQs33WZSoLYzxk1kX/hm8uvCGjaSk0InspbJ3Y7trCEqWAOM5O3
jIreivbVFJEoZifm+br9M1FbXs80spTYVXk7uMD69+9RJ9RpIztZ0XULrxDpmrWLwCe0juIXS4Db
SsoX5gV6lSBxxnJ5FVLTwhdJba5pt5Nb3OmatNLO8h3LMGkVVYEAbQAV3Aj2GOK62O48yHeAOThf
eohqMGCJMKwHI64rPmdw5TIsrTWNscOqXlrNbRRGNzbxtuuBjaGbdwoxnhc5PcDisNPBuujQNL0m
PUrE22k3MM9pI8Um+VYm3IknZcKSMrknA6V081whUyx3DK3OF4xTbXV3dikzqTxtycU22xXszH/s
LU9O1O61LTNTtBPfhRex3cLGMyLkLIgUgggHbtOQwUZINRafol9oIdNH1KCZ52aW4/tAMRJMxJaV
dpBUtnlenyjBHNbU01y5HHy5Kg4xmq8kjoytIoBPI3L1oSNItMTw/pf9gaabZvMuJ5p5LmecJtDy
yMWYhecDPAHoK1mkYAMY2APrWcl7dMuIVdh0wq8D+lU5L3UWmKCGVio3FQucCnFm8Ipm+Jcrkjb9
TTWkQj7wqrAJHshPcRSKM8JnBx6nPSqrXXkOY5Yz8wDRhVySD2J7Gmpa2KUE3ZGhvHqB+NNMyHID
KD9arNPbRmJblUjaY4jDMAZGALFVycscDPFSRyWLKrC3YLt4ODtznuR0qlIzlC+w7zAff6Um4+v6
VVl1nR43aKZTFIvXdkqCfcdvemNq+ipgNEznHWNmZfwOK0U7GToPex0EbWSNtQRrJjqAATWVcSyN
M2HC9sKPesQ6gWYYdsEYJB5xV1LtWUYkZSD371yWZtGNncljk2P+8XPTDHpj3q5JcIxGIy5xjkHB
FUXv028856kCprSfzZgEGB0Zj0Ge1LUp6k4a280CWJI12liM7fm9auRWtmylxDEc9WAz/Os65gtp
5VV3aQq2WbPQjpgVds0gicmKaR2AIwzZ/SmpCkk1dEWv3UlhoF5dWsO+e2hZ4UVdxLYwPl74zn8K
85tvG0d9os17qcqrf2pCPEo2tN6FR29/SvSL2Lzg6l3VXUqSPlIBGOD2NcN/wrfSl0mSzFxK967b
kunTkNngbfTGQeeck+lPmaCF46ozra5utd8Mf23fac93JZXDpbwxAsFBA+ZgT8yrxk+vpW54C0qF
PCyMbhJJppWneGFwSm7ouAfl4HTtmmWHiCBNRk8HSJP5FrZtBJduxWV2AALKOQBycHntWl4f0Lw9
o909zYwyC5EQi8yaTd8uOcdBk96FJt3C83rYoWXh/X4PF124hjltLnYZbp2K7EAYhEXJ3EcZPTJr
vrW1W2t0jVxvUYLBeT35rMfVmhLbkLHHUdwKF1tWhLY/i+ZQMYH1rSKkzOUZsp3czTzMjkHacZ7H
/GqD20jKUWdlU9g3FJqk9t5jPbuy8ev3vwPeqUN/wRuZjXWoe7ock4u5qJZW32cvMzefuGCrcY9c
Ve0e3bzXJl3RZyAQRuPasNTJKw2sPm9TWza2d8salANjYILNisKkbLVjjJmvNZxyrmRmwo4AO0D3
4rHu7fT7eMuk0rMf9rj+VakrSC1CSNmTkHFc1e2ziRgz5UdqwSN0rmhBBYvhCz9c7j1YGteBbCNY
0CoGHRmUZ/OuNila3kUl8qTgYqabUwB+7c7u2O30puLE4m9qNzM11iORUUBvmVdzduDXOXF1dRXG
bgPIi8qx4bPb8KvW19H5RAldWAyCeuatQa3DCzecxkYcbtmSfzpJ2E4voVYNXe8lEZa52gDBSAsM
+hx0/KugfUDFG0cVrIG2nA2DGffmqMeuxSlfLDKO4YY/lVh9TiCFQoI9qlu5Ub9SnZ6ZcX0LXNxP
JFIzH5Qoxx6Vn6/pV9dzo8F1IZrcRkQghUKO+1mbB5wAT7YOKvXut3MMAW2QF2BIzjnGOATwCcgA
ngVz3iLXbmz0tru5heD7RH5DLIRlSGO3JH+8R74BoTaZtGc0zlvEeoagdY0+6LS3NojR/Z2CDcjq
d3BAJO8ZXj2GOtesR2Gn31qJ4ctFMgYBXIDKeRx+NeU+MXjl0Kx/s9W+0xSq0cYywmJBVuhGBnuf
Tgck1s+HdcW3+xaTJetb3YVIgZFwjuy5AXBOAScLn6VTb3Gr3bbsdXd6QqKRbRYZVwpchsD/AIFz
UEcepqgAaDH+/t/TFQajqF3CwhkQzSEZxET+tZ/nai/ItZMfjUuTOynHTVoW2hubkF44WcLgEquc
VdGn344a2ZcjI3EA/lXV2jW1tDHbwkbI1wOc/nVHU3eYsscbM3ZUPJocmeapMxbexuGDvIjAL3Xn
NbGkQ74JQV2kNgblO7nuaBp7x265kKnA3hT909/qfrUs+taZpqLFLdwwssLTbZHCny1+8/P8Izyf
epvcpyZHPbLbTSM6blK/u0xx/vE1SttiyrkiNSR8u4n8M1HrCLr9hPA0jpa3cZRngYZCMByDz1B6
+9Ul0y/0jSIUXUWvblTmSaQqD9MDgAdAB096EruxcZdGUvE3jC90TxjaW837rS5oAp34xuYnLZ6j
acD6Zp3g/wAYnX9XvrRYRDp8MQMc7A72ZmIznp7gViyx2V9qq3uoQNczohiDyMcEZ9PXk8+9W9Gh
ttE08WVvNK8YkaRfMIyNx6D2FacjRSjpY57xB4dvPC2oWiaFcXklxelonmZQzKpIPJAwvr+BrV0K
y1WytG/tTUnmneQtt37lQDjAPv1NbL6g0rBFZmY8AAZNXrLQG1C2kubuQwRMPkbOCeeTj04pxilu
UpKJWTXkijKTu0u0AKOOPxqUS/bU32h3HJ3DIAHpjmuW1KBLS6ljSbzUBwGK4zVeDVBaScI7bTkl
SP0rdRS1RMpXO+stIfymu76JdoU7Y3bBY+uPSrF9ofmxLLaWCrIw5KyBQOOw6VkW3iq2khV5maXa
B8jA7jjtVv8A4TaKaRRJbMidGXdnj0FClO+hk4sWLQr+KIPKFUM20KX5+uelbumWl3bLJ57ZiK8f
Pux9KwX1hLuMeROWQsAItpBGe1bKqlnBkt++25bc/APpjpRJtqzFy2FlIubgrFcop7BmwawNUme3
ujFJMr+47U8XD6pIUkcq6/c2qMD3qnc6DeXN3HHbyO5YfMzjCj1OfSp5YrcpRsVWYScdvrT0gluM
JFCzlRwFGcV0EHg9YVZ578uAOgQKB655rU04W1nGLeMqQW3M2Rz+VZykuhLszlE0TU3AItnXPA3s
F/nV2y8OXMtyFuiqoBkkMCfpXUXZ3RkJk8Z+XrVSDTNw824kcHsm7H5nr+ArPmuIqX+kW1nZsUX5
T1ZidwPsBWV9ldLFbgXKMARhTwce5rYvdRjFncQh2aRBwzDtXOXWoQXdsbW4TbGBj93gMPof6U4x
bKii8LJ5oHaVA2RgFWDYJGOg7YPSuf8AFVjq9z4VurdraK5gbaEHnMsytuG0jIIJB5ySOM881kXe
raxb6mbDw5PBFFaqtxP9oXJm3EhYs44yAxJGO1dBe+KXlsIGTUoNOW62os0yB2Vm42qh4LZ45yBi
rcWldD3OP0VNT0ic/wBsedApKxxw3Fo7BieABtH94kYHqOxqtrF7davcWNnpsapcTz+UvlDczMzf
MO6qF+9g8qOexr0nRNTvh4dt4NQuBdXaBopmZOJdrMAxHuAD+NW9Kt7K2ummstKs7OZ875I0Clge
SCevJ7UuV9galY3Vsra0BYIpdvvueSxHc08TwY525qhcaiqBU3Bj0OOfwqqdRt0OD1qOVgoye5DB
pGo+cZMqgU8bm27voKuWSTPcMp2uqfe+brjr061l6hqv2qYuS6EDCsrAD8R/hXHaD4z8USajY6fe
6aqsWlea6dNgeJSQNqjoxJXr1GD3oUWxa9T1O+JWEqNqKAMD19c1xOuaTpWtSRtrFoHS3RhvLMux
TySSD6KDVW58ZQXPiZ9CmcR3caLINzfK2V3EA+oHJz/SsuPxJZ+I7LULWzkkkt4z5Erqdu4MOSpH
ODyAfahRZcYrY7zRVsda06C/tmYWtwoeFl+UFRwOO3Q8Vy+neKrhtU1XS9W065SeK4cq8aqYoocL
sUsD8zEHcSPWsDw9ZW/hu7muYZZZGdWigVnYi3iODtUEnvzmtdtQQRSPIf3rdHL4/OrjTd9QUerL
F7IizFt6lW5AB4//AF1QNwp3FVYgEDg8ZNZ1xO02cNuAJ5L54qtNqSGL7NHuZhj7tbqLQ7HU+Hp7
dr1pDuMkall+bGfwrYv/ABJKrFCFJYdv8K5bSJmhtC7RhCxxyOSP5/hXReHrG21C8a4ulLqrAKuO
MkZyT/SolHqxtdTFv3j1XaJFWMKcZVcfjxVBo7eyLRxhiDyDkHdXYaje2BvkAsIWjjUr8w4xVO3G
mTalasLKIeSwbbH8u7HTJ784NNSstga0uYKW128XmR2dwyeojbHr6c1CWbaR5bFhwRg5z6V6JrMk
7Sq73BCZDKFyAD9KwmL3mtxXztEyrt3K7bSxB+lCn1ErtXKNtpesW8kTfZZInZgFDYyT1GR2/GtK
+nu4XUXyhJCv94H+XetjUru3kl3qwEg5Yhtoz6+1cHez3WtakswExAHz4GQRnjFJSb1Y4q+50lpe
OjM8KlsKWbauePeuq0O5F2rSkfMoA9sGuJs5Xs4CQjED73HT/Gur0CWMwAwRldoxhuM+vvWc2xTj
oa00bsWYQM+7II3BQOK5xdL1BrsqoCBT1Y4H5jrW9c6kkEJIba4GQvrXN6lq/wBscI/mBV6OpCkV
EUyIxbNm0NzFerHIyHA+YB+PpWuHLx4kVUDAjk9K4OLV7iJSpkVlBPLAZxUl74keWEK7gL6L61XI
3sN02zJ+JDa7pOkC/wBHcLHBKDdM0QkCxHjd9AcE49awS+oW4El3HBeRMoYT2m5WwRw2xicjv8p/
Cti/8Yrp6RWsgaZr0lEgXGH453E8AY9evoawYp3sYzFZuPsbdLRmJ8k56Rt/d5+6eB2I6VrCLTBR
dyrFcxR6hqV2ZMxP5cgZeflEeP5huOtZF7py3l1ZXl4XN610jqgf5YUB3MoHTOBknuTWtepbhWfb
zMyq5A6gNk5/z3rNubwITJtLkd1/x7V0xgmrMUkzS1TXtTsZ7Z7SeGRp5xH5RiIyDySGDZGAM10q
eISpP78/Vq8+tnDztdtcJLcBSqqhysSnqAOuT3J61eFzMyHzAmMZBB5FUqMXdlRkzvbbXIZ9wklT
OODnBzVCZnllLi4JB77hXDG4cEkOQPeoxfyAY81qFRiti1Ludt4p8TWmjWwuZVeTdIse1Dzzk559
AOlZ0via2g0/7fHcq1sV3bs9foPXPGK2PF3hCy1Lw7fDT9OvL26jUywJIzAbh1xjGeM4HfFefz/C
3xXH4LjvWikZlYS/2avMiqcjO3+8OCV64b1BrgTXQylJpjtRu9I1u1g8RaiXUKjwpbZ2mWQE4yw5
HH9Kn8JF7bw5HlGUSu0igcErnC59e9dDbeH9AtLCHw492kGtNZmeS0cGVIZSuSGPY5IOOuBXNWfh
nU9IcMdYW4CJ81vHISueem7HA9AM1pCzkCve6IbvUb+08SPN9m+0LLEEiRJGVYlBGS2RgE960p9S
80HDbR7dqouzXDEEPu5JVQTVCdJI3COGXscmt1FLqNXL93cTeWjpcL8x2lQORV+xtpoFRvtfzcNt
CZNZdjaJcXCoHLdyFXNdHLbRIijeQAAeozj1HpVKzLSNUwE2qzSXAIz9wnlh3rT/AOEgSLTHELtG
xfAGMYXHAFcqb23VdpcgAcbutZr3Etwx8vITPDM3FTyX3G5I6aDVIGjmN0jSyMTsO8gDirVjqa20
I2uok3Ft/pxjGa4+wvra3mZ7yOSZU/hX7ufc1v8Ah5LfXdY2SRG3tFX5VTBZieAMYx780pJJNsV2
zeOszXmFL52rjJPXFQPqLQqXBwBzgd6s634etrKUR2LSMFGXXeML+PbPpWTAtskoS+uPs25scjPH
881muVq6GkzVge8uLWa7MEnlqMZHQfX/AAqGPxAFhNukKqegYcc/h1rTvfEumppZsrVjiP5fmXbu
z1OPeuKkuojIXjjCA5xgcZ9qIpPdFJPqdPFqMSxF5fvBskdM1p2nidROjKqoqjGB3Pqa4mztb7WJ
JI7OEzmJdzncFwOnU1r2ng/WBMdyw2xXaD5kobOfTFKUYdQdup2t3rdjd2h3q3mYzlegrldR1GBG
ILbWB61fGgLbvjUNQRFKnDqMA/gT1preGtInnBiiv7hyAOWO0n1yBj9ahcqBJI5ebV058s5qGLUJ
Hfcyhl7e1dn/AMILb3IjyWtY1BBUEMx/wok8B6TGqmS/mQ7c/MwwffH9K0U4CckcJcJHdlluVDox
ztI4B9RVGS0ggU+XcXKgdF80sP8Ax7Nbl/oixzyRW97C+PuDcQzfUHpXNXVrdXl49pGpiEbbJZUG
5gf7qju2Op6D61tFxauKXoZx1CVppEjfdFGCGZm3Fn/ug+w/nT0uVYBkbIYZBFX77QxZWsawW0sS
9VMjDle5I65zVyDwxbNZJPY38NzcO3z27ZUE+x5w31GD7VXMoq7J5WcxdXFqzEvMsUq8q+SjqfUH
Gf6VFpurSXMLrKCxQ4EgUgOPX6+1eiWngvPhmB72B4dQLMHRpVcAbjtUAdflxXD6vp02mzFMsYWO
ASu3BH8JojJSd0wcWveGPcZB+bmoPPJ54/Oq7N8oJIH0qEyjNap2EfQ3/CVqk6aRbxSBwow8h3DG
T3znPFTWK3l/e3cT3RjS3UswQY3D0HpRRXkxLa0PALPxPHb+MH8V3OnxmPzX3W0Tt8zEEAncT+PP
4V1On+L4PGuqTR3ejRW6Rxl90cxL5yBjoOOaKK0iYQeqNX+ydPtJZJIo5TLwMtISBn2qq2m2aStM
8bSyf3pDkfl0oorVM6UtDFvBGJj5aBAD0Ubc/lWTcTtGHO6Tnj75oorVEMjllm+zpK20LnYQpOen
WrunX8UKDMbOrY+Vjxx60UVVyVuetaL4J0PUPDtlqV1bSStNEspjEzIgOc4wKks7Owm1+FYNOt4V
3eWqxkoBjgdP59aKK43Ju9yluzo7iJLFYppFRmkO3CoMAA471h33hS2v7hLiS5lRQ7EJGoHQZ6+t
FFRB6jWxk2vg22uNRW3luZSWJO7A4AGfzrZ8V21rB4YlhhgjURQ4jygJXt160UVd3dDe6OI0GbUY
7Ii2mRBKQ57E47EgVOmoeIJyZEvkTaeTySTnHpRRWskrmiMphNq19Fbve3Ek80yqrsQqqpIzwM5N
ekP4hg8P26WdvFMwLbAztu59Tk0UVlV6EtXFikvtQ1q2sxciISrnci9Op/HpWrqmnWtrpO8xiaVs
AySZJJ9ueB9KKKnaxlJu6Oc1Oex1MwwTafHtGANp2kAehHNZXh7w3owt7O8eC4efyy2PtDBS+Tli
Bz196KKu7SLZ1Q8GaVdTpdXsb3L/AHhHI52L3xjuPrS634f0WJGV9NhjLHdm3Gw5xjqMUUVipy7k
XfMUrDSNJW5MiW0h2KMI0rFU7ZUZ6nuTmqviHwppF7YSxubiNV5CxlQM+vSiitot8xTep43r2jjS
mZ4pN0Zk2qrDnGM5J9azIXtPL/exSl88lWGP5UUV309SXuf/2Q==
', '2016-12-11 07:05:09.020397-04', '2016-12-11 07:05:09.020428-04', 1);


--
-- TOC entry 2314 (class 0 OID 0)
-- Dependencies: 194
-- Name: negocio_negocio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('negocio_negocio_id_seq', 3, true);


--
-- TOC entry 2268 (class 0 OID 237133)
-- Dependencies: 207
-- Data for Name: productos_productos; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2315 (class 0 OID 0)
-- Dependencies: 206
-- Name: productos_productos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('productos_productos_id_seq', 1, false);


--
-- TOC entry 2270 (class 0 OID 237156)
-- Dependencies: 209
-- Data for Name: telefono_local_localtelefono; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO telefono_local_localtelefono (id, telefono, date_create, date_update, local_id) VALUES (7, '123', '2016-12-11 07:59:16.152273-04', '2016-12-11 07:59:16.152303-04', 24);
INSERT INTO telefono_local_localtelefono (id, telefono, date_create, date_update, local_id) VALUES (8, '456', '2016-12-11 07:59:16.160635-04', '2016-12-11 07:59:16.160664-04', 24);
INSERT INTO telefono_local_localtelefono (id, telefono, date_create, date_update, local_id) VALUES (9, '789', '2016-12-11 09:28:11.896738-04', '2016-12-11 09:28:11.896772-04', 25);
INSERT INTO telefono_local_localtelefono (id, telefono, date_create, date_update, local_id) VALUES (10, '654', '2016-12-11 09:28:11.904663-04', '2016-12-11 09:28:11.904689-04', 25);
INSERT INTO telefono_local_localtelefono (id, telefono, date_create, date_update, local_id) VALUES (11, '789', '2016-12-11 11:21:31.980396-04', '2016-12-11 11:21:31.98042-04', 26);


--
-- TOC entry 2316 (class 0 OID 0)
-- Dependencies: 208
-- Name: telefono_local_localtelefono_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('telefono_local_localtelefono_id_seq', 11, true);


--
-- TOC entry 2254 (class 0 OID 237020)
-- Dependencies: 193
-- Data for Name: tipo_negocio_tiponegocio; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO tipo_negocio_tiponegocio (id, tipo_negocio) VALUES (1, 'Ropa y Accesorios');
INSERT INTO tipo_negocio_tiponegocio (id, tipo_negocio) VALUES (2, 'Tienda de Zapatos');
INSERT INTO tipo_negocio_tiponegocio (id, tipo_negocio) VALUES (3, 'Bolsos y Carteras');


--
-- TOC entry 2317 (class 0 OID 0)
-- Dependencies: 192
-- Name: tipo_negocio_tiponegocio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('tipo_negocio_tiponegocio_id_seq', 3, true);


--
-- TOC entry 2042 (class 2606 OID 226116)
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- TOC entry 2048 (class 2606 OID 226171)
-- Name: auth_group_permissions_group_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- TOC entry 2050 (class 2606 OID 226124)
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2044 (class 2606 OID 226114)
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- TOC entry 2037 (class 2606 OID 226157)
-- Name: auth_permission_content_type_id_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- TOC entry 2039 (class 2606 OID 226106)
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 2059 (class 2606 OID 226142)
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 2061 (class 2606 OID 226186)
-- Name: auth_user_groups_user_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- TOC entry 2052 (class 2606 OID 226132)
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2065 (class 2606 OID 226150)
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2067 (class 2606 OID 226200)
-- Name: auth_user_user_permissions_user_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- TOC entry 2055 (class 2606 OID 226229)
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- TOC entry 2083 (class 2606 OID 237056)
-- Name: banner_banner_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY banner_banner
    ADD CONSTRAINT banner_banner_pkey PRIMARY KEY (id);


--
-- TOC entry 2086 (class 2606 OID 237073)
-- Name: categoria_categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY categoria_categoria
    ADD CONSTRAINT categoria_categoria_pkey PRIMARY KEY (id);


--
-- TOC entry 2088 (class 2606 OID 237090)
-- Name: ciudad_ciudad_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ciudad_ciudad
    ADD CONSTRAINT ciudad_ciudad_pkey PRIMARY KEY (id);


--
-- TOC entry 2091 (class 2606 OID 237101)
-- Name: coleccion_coleccion_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY coleccion_coleccion
    ADD CONSTRAINT coleccion_coleccion_pkey PRIMARY KEY (id);


--
-- TOC entry 2071 (class 2606 OID 226214)
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- TOC entry 2032 (class 2606 OID 226098)
-- Name: django_content_type_app_label_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- TOC entry 2034 (class 2606 OID 226096)
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- TOC entry 2030 (class 2606 OID 226088)
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2074 (class 2606 OID 226241)
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- TOC entry 2105 (class 2606 OID 237252)
-- Name: fidelizacion_fidelizacion_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY fidelizacion_fidelizacion
    ADD CONSTRAINT fidelizacion_fidelizacion_pkey PRIMARY KEY (id);


--
-- TOC entry 2095 (class 2606 OID 237118)
-- Name: local_local_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY local_local
    ADD CONSTRAINT local_local_pkey PRIMARY KEY (id);


--
-- TOC entry 2080 (class 2606 OID 237039)
-- Name: negocio_negocio_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY negocio_negocio
    ADD CONSTRAINT negocio_negocio_pkey PRIMARY KEY (id);


--
-- TOC entry 2099 (class 2606 OID 237141)
-- Name: productos_productos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY productos_productos
    ADD CONSTRAINT productos_productos_pkey PRIMARY KEY (id);


--
-- TOC entry 2102 (class 2606 OID 237161)
-- Name: telefono_local_localtelefono_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY telefono_local_localtelefono
    ADD CONSTRAINT telefono_local_localtelefono_pkey PRIMARY KEY (id);


--
-- TOC entry 2077 (class 2606 OID 237028)
-- Name: tipo_negocio_tiponegocio_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tipo_negocio_tiponegocio
    ADD CONSTRAINT tipo_negocio_tiponegocio_pkey PRIMARY KEY (id);


--
-- TOC entry 2040 (class 1259 OID 226159)
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- TOC entry 2045 (class 1259 OID 226172)
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- TOC entry 2046 (class 1259 OID 226173)
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- TOC entry 2035 (class 1259 OID 226158)
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- TOC entry 2056 (class 1259 OID 226188)
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- TOC entry 2057 (class 1259 OID 226187)
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- TOC entry 2062 (class 1259 OID 226202)
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- TOC entry 2063 (class 1259 OID 226201)
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- TOC entry 2053 (class 1259 OID 226230)
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- TOC entry 2081 (class 1259 OID 237062)
-- Name: banner_banner_50d56ad8; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX banner_banner_50d56ad8 ON banner_banner USING btree (negocio_id);


--
-- TOC entry 2084 (class 1259 OID 237079)
-- Name: categoria_categoria_50d56ad8; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX categoria_categoria_50d56ad8 ON categoria_categoria USING btree (negocio_id);


--
-- TOC entry 2089 (class 1259 OID 237107)
-- Name: coleccion_coleccion_50d56ad8; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX coleccion_coleccion_50d56ad8 ON coleccion_coleccion USING btree (negocio_id);


--
-- TOC entry 2068 (class 1259 OID 226225)
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- TOC entry 2069 (class 1259 OID 226226)
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- TOC entry 2072 (class 1259 OID 226242)
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- TOC entry 2075 (class 1259 OID 226243)
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- TOC entry 2103 (class 1259 OID 237258)
-- Name: fidelizacion_fidelizacion_50d56ad8; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fidelizacion_fidelizacion_50d56ad8 ON fidelizacion_fidelizacion USING btree (negocio_id);


--
-- TOC entry 2092 (class 1259 OID 237129)
-- Name: local_local_0201ed81; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX local_local_0201ed81 ON local_local USING btree (ciudad_id);


--
-- TOC entry 2093 (class 1259 OID 237130)
-- Name: local_local_50d56ad8; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX local_local_50d56ad8 ON local_local USING btree (negocio_id);


--
-- TOC entry 2078 (class 1259 OID 237045)
-- Name: negocio_negocio_f4da63e1; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX negocio_negocio_f4da63e1 ON negocio_negocio USING btree (tipo_negocio_id);


--
-- TOC entry 2096 (class 1259 OID 237153)
-- Name: productos_productos_6a60a14e; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX productos_productos_6a60a14e ON productos_productos USING btree (coleccion_id);


--
-- TOC entry 2097 (class 1259 OID 237152)
-- Name: productos_productos_daf3833b; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX productos_productos_daf3833b ON productos_productos USING btree (categoria_id);


--
-- TOC entry 2100 (class 1259 OID 237167)
-- Name: telefono_local_localtelefono_64a24e67; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX telefono_local_localtelefono_64a24e67 ON telefono_local_localtelefono USING btree (local_id);


--
-- TOC entry 2108 (class 2606 OID 226165)
-- Name: auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2107 (class 2606 OID 226160)
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2106 (class 2606 OID 226151)
-- Name: auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2110 (class 2606 OID 226180)
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2109 (class 2606 OID 226175)
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2112 (class 2606 OID 226194)
-- Name: auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2111 (class 2606 OID 226189)
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2116 (class 2606 OID 237057)
-- Name: banner_banner_negocio_id_00b2c1da_fk_negocio_negocio_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY banner_banner
    ADD CONSTRAINT banner_banner_negocio_id_00b2c1da_fk_negocio_negocio_id FOREIGN KEY (negocio_id) REFERENCES negocio_negocio(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2117 (class 2606 OID 237074)
-- Name: categoria_categoria_negocio_id_76683825_fk_negocio_negocio_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categoria_categoria
    ADD CONSTRAINT categoria_categoria_negocio_id_76683825_fk_negocio_negocio_id FOREIGN KEY (negocio_id) REFERENCES negocio_negocio(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2118 (class 2606 OID 237102)
-- Name: coleccion_coleccion_negocio_id_dca6ead9_fk_negocio_negocio_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY coleccion_coleccion
    ADD CONSTRAINT coleccion_coleccion_negocio_id_dca6ead9_fk_negocio_negocio_id FOREIGN KEY (negocio_id) REFERENCES negocio_negocio(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2113 (class 2606 OID 226215)
-- Name: django_admin_content_type_id_c4bce8eb_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_c4bce8eb_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2114 (class 2606 OID 226220)
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2124 (class 2606 OID 237253)
-- Name: fidelizacion_fideliza_negocio_id_960bf430_fk_negocio_negocio_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY fidelizacion_fidelizacion
    ADD CONSTRAINT fidelizacion_fideliza_negocio_id_960bf430_fk_negocio_negocio_id FOREIGN KEY (negocio_id) REFERENCES negocio_negocio(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2119 (class 2606 OID 237171)
-- Name: local_local_ciudad_id_39ce95cf_fk_ciudad_ciudad_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY local_local
    ADD CONSTRAINT local_local_ciudad_id_39ce95cf_fk_ciudad_ciudad_id FOREIGN KEY (ciudad_id) REFERENCES ciudad_ciudad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2120 (class 2606 OID 237176)
-- Name: local_local_negocio_id_1668c62c_fk_negocio_negocio_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY local_local
    ADD CONSTRAINT local_local_negocio_id_1668c62c_fk_negocio_negocio_id FOREIGN KEY (negocio_id) REFERENCES negocio_negocio(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2115 (class 2606 OID 237040)
-- Name: negocio_tipo_negocio_id_465f1f28_fk_tipo_negocio_tiponegocio_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY negocio_negocio
    ADD CONSTRAINT negocio_tipo_negocio_id_465f1f28_fk_tipo_negocio_tiponegocio_id FOREIGN KEY (tipo_negocio_id) REFERENCES tipo_negocio_tiponegocio(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2121 (class 2606 OID 237142)
-- Name: productos_produ_categoria_id_9891623f_fk_categoria_categoria_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY productos_productos
    ADD CONSTRAINT productos_produ_categoria_id_9891623f_fk_categoria_categoria_id FOREIGN KEY (categoria_id) REFERENCES categoria_categoria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2122 (class 2606 OID 237147)
-- Name: productos_produ_coleccion_id_53a2ec07_fk_coleccion_coleccion_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY productos_productos
    ADD CONSTRAINT productos_produ_coleccion_id_53a2ec07_fk_coleccion_coleccion_id FOREIGN KEY (coleccion_id) REFERENCES coleccion_coleccion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2123 (class 2606 OID 237182)
-- Name: telefono_local_localtelefon_local_id_40f4afcf_fk_local_local_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY telefono_local_localtelefono
    ADD CONSTRAINT telefono_local_localtelefon_local_id_40f4afcf_fk_local_local_id FOREIGN KEY (local_id) REFERENCES local_local(id) DEFERRABLE INITIALLY DEFERRED;


-- Completed on 2016-12-13 07:05:47 VET

--
-- PostgreSQL database dump complete
--

