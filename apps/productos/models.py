from django.db import models

from django.db import models
from ..categoria.models import Categoria
from ..coleccion.models import Coleccion


class Productos(models.Model):
    """
    Description: Model Description
    """
    nombre = models.CharField(max_length=100, )
    descripcion = models.TextField(null=True, blank=True)
    orden = models.IntegerField(null=True, blank=True)
    status = models.BooleanField(null=False, default=True)
    destacado = models.BooleanField(null=False, default=True)
    precio_real = models.FloatField(null=True, blank=True)
    precio_lista = models.FloatField(null=True, blank=True)
    fidelizacion = models.BooleanField(null=False, default=True)
    monto = models.FloatField(null=True, blank=True)
    puntos = models.IntegerField()
    img_pro = models.ImageField(upload_to='categoria')
    img_pro_bin = models.TextField(null=True, blank=True)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, related_name='categoria_producto', null=True, blank=False,)
    coleccion = models.ForeignKey(Coleccion, on_delete=models.CASCADE, related_name='coleccion_producto', null=True, blank=False,)
    # categoria = models.ForeignKey('self', on_delete=models.CASCADE, related_name='categoria_categoria', null=True, blank=False,)
    date_create = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)
    date_update = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __unicode__(self):
        return self.nombre
