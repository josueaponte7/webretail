from rest_framework import viewsets
from .models import Productos
from .serializers import ProductosSerializer


class ProductosViewSet(viewsets.ModelViewSet):

    serializer_class = ProductosSerializer
    queryset = Productos.objects.all()