from rest_framework import serializers
from .models import Productos

class ProductosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Productos
        fields = ('id', 'nombre','precio_real','precio_lista' ,'img_pro_bin', 'img_pro',)