# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-10 11:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('coleccion', '0001_initial'),
        ('categoria', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Productos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('descripcion', models.TextField(blank=True, null=True)),
                ('orden', models.IntegerField(blank=True, null=True)),
                ('status', models.BooleanField(default=True)),
                ('destacado', models.BooleanField(default=True)),
                ('precio_real', models.FloatField(blank=True, null=True)),
                ('precio_lista', models.FloatField(blank=True, null=True)),
                ('fidelizacion', models.BooleanField(default=True)),
                ('img_pro', models.ImageField(upload_to=b'categoria')),
                ('img_pro_bin', models.TextField(blank=True, null=True)),
                ('date_create', models.DateTimeField(auto_now=True, null=True)),
                ('date_update', models.DateTimeField(auto_now=True, null=True)),
                ('categoria', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='categoria_producto', to='categoria.Categoria')),
                ('coleccion', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='coleccion_producto', to='coleccion.Coleccion')),
            ],
        ),
    ]
