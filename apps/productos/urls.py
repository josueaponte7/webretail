from django.conf.urls import  include, url
from .views import ListarProductos, CreateProductos, DetailProductos, DeleteProductos
from rest_framework.routers import DefaultRouter
from .viewsets import ProductosViewSet

router = DefaultRouter()
router.register(r'api', ProductosViewSet)

urlpatterns = [
    url(r'^$', ListarProductos.as_view(), name="listar_productos"),
    url(r'^crear$', CreateProductos.as_view(), name="crear_productos"),
    url(r'^buscar$', DetailProductos.as_view(), name="detail_productos"),
    url(r'^eliminar$', DeleteProductos.as_view(), name="eliminar_productos"),
    url(r'^', include(router.urls)),
]

