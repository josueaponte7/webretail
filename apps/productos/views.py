from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView
from .models import Productos
from ..fidelizacion.models import Fidelizacion
import json
from django.http import HttpResponse
from django.conf import settings
import base64
from PIL import Image
import os


class ListarProductos(ListView):

    template_name = 'productos/listar.html'
    model = Productos
    titulo = 'Listar Productos'

    def get_context_data(self, **kwargs):
        context = super(ListarProductos, self).get_context_data(**kwargs)
        context["productos"] = self.model.objects.all()
        context['titulo'] = self.titulo
        return context


class DetailProductos(DetailView):
    model = Productos

    def get(self, request, *args, **kwargs):
        ids = self.request.GET.get('id')
        queryset = self.model.objects.all()
        queryset= queryset.filter(id=ids)
        queryset= queryset.values('nombre', 'status', 'img_pro')
        data = {}
        for value in queryset:
            data = value
        return HttpResponse(json.dumps(data), status=200, content_type='application/json')


class CreateProductos(CreateView):
    template_name = 'productos/form.html'
    model = Productos
    fields = ['nombre', 'descripcion', 'status','precio_real', 'precio_lista', 'categoria','coleccion', 'monto', 'puntos', 'img_pro']
    titulo = 'Nuevo Producto'

    def get_context_data(self, **kwargs):
        context = super(CreateProductos, self).get_context_data(**kwargs)
        queryset_fidelizacion = Fidelizacion.objects.all()
        queryset_fidelizacion = queryset_fidelizacion.filter(negocio_id=3)
        queryset_fidelizacion = queryset_fidelizacion.values('id', 'moneda', 'puntos');
        moneda = queryset_fidelizacion.moneda
        context['titulo'] = self.titulo
        # context["productos"] = self.model.objects.all()
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        add = form.save(commit=False)
        response_data = {}

        if form.is_valid():

            nombre = request.FILES['img_pro'].name
            ruta_image = settings.MEDIA_ROOT
            archivo = ruta_image+'/productos/'+str(nombre)
            img = Image.open(request.FILES['img_pro'])
            img.thumbnail((480, 480), Image.ANTIALIAS)
            img.save(archivo)

            image = open(archivo)
            fot_c =  image.read()
            img_pro_bin = base64.encodestring(fot_c)
            add.img_pro_bin = img_pro_bin
            os.remove(archivo)

            valor = add.save()
            response_data['success'] = 'error'
            if valor != 'error':
                response_data['success'] = 'ok'
            # return HttpResponse(json.dumps(response_data), status=204, content_type='application/json')
            return HttpResponse(json.dumps(response_data), status=200, content_type='application/json')


class DeleteProductos(DeleteView):
    model = Productos

    def delete(self, request, *args, **kwargs):

        ids = request.POST.get('id')
        tienda_obj = self.model.objects.get(id=ids)
        tienda_obj.delete()
        response_data = {}
        response_data['eliminar'] = 'ok'
        return HttpResponse(json.dumps(response_data), status=200, content_type='application/json')
