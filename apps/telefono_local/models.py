from django.db import models

from ..local.models import Local


class LocalTelefono(models.Model):
    """
    Description: Model Description
    """
    telefono = models.CharField(max_length=100, )
    local = models.ForeignKey(Local, on_delete=models.CASCADE, related_name='local_telefono', null=True)
    date_create = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)
    date_update = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __unicode__(self):
        return self.correo