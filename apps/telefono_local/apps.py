from __future__ import unicode_literals

from django.apps import AppConfig


class TelefonoLocalConfig(AppConfig):
    name = 'telefono_local'
