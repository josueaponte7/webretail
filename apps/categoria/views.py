from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView
from .models import Categoria
import json
from django.http import HttpResponse
from django.conf import settings
import base64
from PIL import Image
import os


class ListarCategoria(ListView):

    template_name = 'categoria/listar.html'
    model = Categoria
    titulo = 'Listar Categorias'

    def get_context_data(self, **kwargs):
        context = super(ListarCategoria, self).get_context_data(**kwargs)
        context["categorias"] = self.model.objects.all()
        context['titulo'] = self.titulo
        return context


class DetailCategoria(DetailView):
    model = Categoria

    def get(self, request, *args, **kwargs):
        ids = self.request.GET.get('id')
        queryset = self.model.objects.all()
        queryset= queryset.filter(id=ids)
        queryset= queryset.values('nombre', 'status', 'img_cat')
        data = {}
        for value in queryset:
            data = value
        return HttpResponse(json.dumps(data), status=200, content_type='application/json')


class CreateCategoria(CreateView):
    template_name = 'categoria/form.html'
    model = Categoria
    fields = ['nombre', 'descripcion', 'status', 'negocio', 'img_cat']
    titulo = 'Nueva Categoria'

    def get_context_data(self, **kwargs):
        context = super(CreateCategoria, self).get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context["categorias"] = self.model.objects.all()
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        add = form.save(commit=False)
        response_data = {}

        if form.is_valid():

            nombre = request.FILES['img_cat'].name
            ruta_image = settings.MEDIA_ROOT
            archivo = ruta_image+'/categoria/'+str(nombre)
            img = Image.open(request.FILES['img_cat'])
            img.thumbnail((480, 480), Image.ANTIALIAS)
            img.save(archivo)

            image = open(archivo)
            fot_c =  image.read()
            img_cat_bin = base64.encodestring(fot_c)
            add.img_cat_bin = img_cat_bin
            os.remove(archivo)

            valor = add.save()
            response_data['success'] = 'error'
            if valor != 'error':
                response_data['success'] = 'ok'
            # return HttpResponse(json.dumps(response_data), status=204, content_type='application/json')
            return HttpResponse(json.dumps(response_data), status=200, content_type='application/json')


class DeleteCategoria(DeleteView):
    model = Categoria

    def delete(self, request, *args, **kwargs):

        ids = request.POST.get('id')
        tienda_obj = self.model.objects.get(id=ids)
        tienda_obj.delete()
        response_data = {}
        response_data['eliminar'] = 'ok'
        return HttpResponse(json.dumps(response_data), status=200, content_type='application/json')
