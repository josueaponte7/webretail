from django.db import models

from django.db import models
from ..negocio.models import Negocio


class Categoria(models.Model):
    """
    Description: Model Description
    """
    nombre = models.CharField(max_length=100, )
    descripcion = models.TextField(null=True, blank=True)
    orden = models.IntegerField(null=True, blank=True)
    status = models.BooleanField(null=False, default=True)
    img_cat = models.ImageField(upload_to='categoria')
    img_cat_bin = models.TextField(null=True, blank=True)
    negocio = models.ForeignKey(Negocio, on_delete=models.CASCADE, related_name='negocio_banner', null=True, blank=False,)
    # categoria = models.ForeignKey('self', on_delete=models.CASCADE, related_name='categoria_categoria', null=True, blank=False,)
    date_create = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)
    date_update = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __unicode__(self):
        return self.nombre
