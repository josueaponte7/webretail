from django.conf.urls import  include, url
from .views import ListarCategoria, CreateCategoria, DetailCategoria, DeleteCategoria
from rest_framework.routers import DefaultRouter
from .viewsets import CategoriaViewSet

router = DefaultRouter()
router.register(r'api', CategoriaViewSet)

urlpatterns = [
    url(r'^$', ListarCategoria.as_view(), name="listar_categoria"),
    url(r'^crear$', CreateCategoria.as_view(), name="crear_categoria"),
    url(r'^buscar$', DetailCategoria.as_view(), name="detail_categoria"),
    url(r'^eliminar$', DeleteCategoria.as_view(), name="eliminar_categoria"),
    url(r'^', include(router.urls)),
]

