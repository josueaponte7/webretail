from __future__ import unicode_literals

from django.db import models


class Ciudad(models.Model):
    """
    Description: Model Description
    """
    ciudad = models.TextField()

    def __unicode__(self):
        return self.ciudad
