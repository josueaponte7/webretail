from django.conf.urls import  include, url
from rest_framework.routers import DefaultRouter
from .views import CrearFidelizacion, ListaFidelizacion, DetailFidelizacion, DeleteFidelizacion
# from .viewsets import LocalViewSet

# router = DefaultRouter()
# router.register(r'api', LocalViewSet)

urlpatterns = [
    url(r'^fidelizacion/(?P<pk>\d+)/$', ListaFidelizacion.as_view(), name="lista_fidelizacion"),
    url(r'^crear/(?P<pk>\d+)/$', CrearFidelizacion.as_view(), name="crear_fidelizacion"),
    url(r'^buscar/$', DetailFidelizacion.as_view(), name="detail_fidelizacion"),
    url(r'^eliminar$', DeleteFidelizacion.as_view(), name="eliminar_fidelizacion"),
    # url(r'^', include(router.urls)),
]
