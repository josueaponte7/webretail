from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView
from .models import Fidelizacion
import json
from django.http import HttpResponse
from django.conf import settings
import base64
from PIL import Image
import os
from django.db.models import F
from django.db.models import  F


class ListaFidelizacion(ListView):

    template_name = 'fidelizacion/listar.html'
    model = Fidelizacion
    titulo = 'Listar Fidelizaciones'

    def get_context_data(self, **kwargs):
        negocio_id = self.kwargs['pk']
        context = super(ListaFidelizacion, self).get_context_data(**kwargs)
        queryset_fidelizaciones = self.model.objects.all()
        queryset_fidelizaciones =  queryset_fidelizaciones.filter(negocio = negocio_id)
        queryset_fidelizaciones = queryset_fidelizaciones.annotate( nombre_negocio=F('negocio__nombre'))
        queryset_fidelizaciones = queryset_fidelizaciones.values('id', 'moneda', 'puntos', 'nombre_negocio' )
        context["fidelizaciones"] = queryset_fidelizaciones
        context['titulo'] = self.titulo
        context['negocio_id'] = negocio_id
        return context


class DetailFidelizacion(DetailView):
    model = Fidelizacion

    def get(self, request, *args, **kwargs):
        ids = self.request.GET.get('id')
        queryset = self.model.objects.all()
        queryset= queryset.filter(id=ids)
        queryset= queryset.values('id', 'moneda', 'puntos', 'descuento')
        data = {}
        for value in queryset:
            data = value
        return HttpResponse(json.dumps(data), status=200, content_type='application/json')



class CrearFidelizacion(CreateView):
    template_name = 'fidelizacion/form.html'
    model = Fidelizacion
    fields = ['id', 'moneda', 'puntos', 'negocio' ,'descuento']
    titulo = 'Crear Fidelizacion'

    def get_context_data(self, **kwargs):
        negocio_id = self.kwargs['pk']
        context = super(CrearFidelizacion, self).get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context["fidelizaciones"] = self.model.objects.all()
        context['negocio_id'] = negocio_id
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        add = form.save(commit=False)
        response_data = {}
        if form.is_valid():
            valor = add.save()
            response_data['success'] = 'error'
            if valor != 'error':
                response_data['success'] = 'ok'
            return HttpResponse(json.dumps(response_data), status=200, content_type='application/json')


class DeleteFidelizacion(DeleteView):
    model = Fidelizacion

    def delete(self, request, *args, **kwargs):

        ids = request.POST.get('id')
        Fidelizacion_obj = self.model.objects.get(id=ids)
        Fidelizacion_obj.delete()
        response_data = {}
        response_data['eliminar'] = 'ok'
        return HttpResponse(json.dumps(response_data), status=200, content_type='application/json')
