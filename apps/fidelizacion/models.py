from django.db import models

from ..negocio.models import Negocio

class Fidelizacion(models.Model):
    """
    Description: Model Description
    """
    moneda = models.FloatField(null=True, blank=True)
    puntos = models.IntegerField()
    descuento = models.FloatField()
    negocio = models.ForeignKey(Negocio, on_delete=models.CASCADE, related_name='negocio_fidelizacion', null=True)
    date_create = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)
    date_update = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __unicode__(self):
        return self.moneda