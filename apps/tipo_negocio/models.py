from __future__ import unicode_literals

from django.db import models


class TipoNegocio(models.Model):
    """
    Description: Model Description
    """
    tipo_negocio = models.TextField()

    def __unicode__(self):
        return self.tipo_negocio
