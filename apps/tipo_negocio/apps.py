from __future__ import unicode_literals

from django.apps import AppConfig


class TipoNegocioConfig(AppConfig):
    name = 'tipo_negocio'
