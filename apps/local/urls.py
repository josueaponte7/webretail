from django.conf.urls import  include, url
from rest_framework.routers import DefaultRouter
from .views import CrearLocal, ListaLocal, DetailLocal, DeleteLocal
# from .viewsets import LocalViewSet

# router = DefaultRouter()
# router.register(r'api', LocalViewSet)

urlpatterns = [
    url(r'^show/(?P<pk>\d+)/$', ListaLocal.as_view(), name="lista_local"),
    url(r'^crear/(?P<pk>\d+)/$', CrearLocal.as_view(), name="crear_local"),
    url(r'^buscar$', DetailLocal.as_view(), name="detail_local"),
    url(r'^eliminar$', DeleteLocal.as_view(), name="eliminar_local"),
    # url(r'^', include(router.urls)),
]
