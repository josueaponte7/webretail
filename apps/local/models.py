from django.db import models

from ..negocio.models import Negocio
from ..ciudad.models import Ciudad

class Local(models.Model):
    """
    Description: Model Description
    """
    nombre = models.CharField(max_length=250, )
    correo = models.EmailField()
    direccion = models.TextField(null=True, blank=True)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE, related_name='ciudad_local', null=True)
    negocio = models.ForeignKey(Negocio, on_delete=models.CASCADE, related_name='negocio_local', null=True)
    date_create = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)
    date_update = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __unicode__(self):
        return self.correo
