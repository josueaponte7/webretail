from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView
from .models import Local
from ..ciudad.models import Ciudad
from ..telefono_local.models import LocalTelefono

import json
from django.http import HttpResponse
from django.conf import settings
import base64
from PIL import Image
import os
from django.db.models import F

class ListaLocal(ListView):

    template_name = 'local/listar.html'
    model = Local
    titulo = 'Listar Locales'

    def get_context_data(self, **kwargs):
        negocio_id = self.kwargs['pk']
        context = super(ListaLocal, self).get_context_data(**kwargs)
        queryset_locales = self.model.objects.all()
        queryset_locales =  queryset_locales.filter(negocio = negocio_id)
        queryset_locales = queryset_locales.values('id', 'nombre', 'correo',)

        json_dict = []
        for locales_dic in queryset_locales:
            json_dict.append(
                {
                'id':locales_dic['id'],
                'nombre':locales_dic['nombre'],
                'telefono': ','.join(LocalTelefono.objects.filter(local_id=locales_dic['id']).values_list('telefono', flat=True)),
                'correo':locales_dic['correo'],
                },
            )

        context["locales"] = json_dict
        context['titulo'] = self.titulo
        context['negocio_id'] = negocio_id
        return context


class DetailLocal(DetailView):
    model = Local

    def get(self, request, *args, **kwargs):
        ids = self.request.GET.get('id')
        queryset = self.model.objects.all()
        queryset= queryset.filter(id=ids)
        queryset= queryset.values('nombre', 'status', 'logo')
        data = {}
        for value in queryset:
            data = value
        return HttpResponse(json.dumps(data), status=200, content_type='application/json')


class CrearLocal(CreateView):
    template_name = 'local/form.html'
    model = Local
    fields = ['id', 'nombre', 'correo', 'ciudad', 'direccion', 'negocio',]
    titulo = 'Crear Local'

    def get_context_data(self, **kwargs):
        negocio_id = self.kwargs['pk']
        context = super(CrearLocal, self).get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['negocio_id'] = negocio_id
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        add = form.save(commit=False)
        response_data = {}
        if form.is_valid():
            telefo = self.request.POST.get('telefono')
            valor = add.save()
            telefonos = telefo.split(',')
            for telefono in telefonos:
                tele = LocalTelefono()
                tele.telefono = telefono
                tele.local_id = add.id
                tele.save();
            response_data['success'] = 'error'
            if valor != 'error':
                response_data['success'] = 'ok'

            return HttpResponse(json.dumps(response_data), status=200, content_type='application/json')


class DeleteLocal(DeleteView):
    model = Local

    def delete(self, request, *args, **kwargs):

        ids = request.POST.get('id')
        Local_obj = self.model.objects.get(id=ids)
        Local_obj.delete()
        response_data = {}
        response_data['eliminar'] = 'ok'
        return HttpResponse(json.dumps(response_data), status=200, content_type='application/json')
