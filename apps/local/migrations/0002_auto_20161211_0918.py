# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-11 09:18
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('local', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='local',
            name='ciudad',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ciudad_local', to='ciudad.Ciudad'),
        ),
        migrations.AlterField(
            model_name='local',
            name='negocio',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='negocio_local', to='negocio.Negocio'),
        ),
    ]
