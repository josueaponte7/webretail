from django.db import models
from ..tipo_negocio.models import TipoNegocio
import os


class Negocio(models.Model):
    """
    Description: Model Description
    """
    tipo_negocio = models.ForeignKey(TipoNegocio, on_delete=models.CASCADE, related_name='tiponegocio_tienda',)
    nombre = models.CharField(max_length=100, )
    pagina_facebook = models.CharField(max_length=200, )
    status = models.BooleanField(null=False,default=True)
    logo = models.ImageField(upload_to='logos')
    logo_ba = models.TextField(null=True, blank=True)
    date_create = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.nombre

