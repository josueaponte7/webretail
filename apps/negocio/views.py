from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView
from .models import Negocio
import json
from django.http import HttpResponse
from django.conf import settings
import base64
from PIL import Image
import os
from django.db.models import Count, F


class ListaNegocio(ListView):

    template_name = 'negocio/listar.html'
    model = Negocio
    titulo = 'Listar Negocios'

    def get_context_data(self, **kwargs):
        context = super(ListaNegocio, self).get_context_data(**kwargs)
        queryset_negocios = self.model.objects.all()
        queryset_negocios = queryset_negocios.annotate(num_local=Count('negocio_local'), negocio_id=F('negocio_fidelizacion'))
        context["negocios"] = queryset_negocios.values('id', 'nombre', 'logo', 'num_local', 'negocio_id',)
        context['titulo'] = self.titulo
        return context


class DetailNegocio(DetailView):
    model = Negocio

    def get(self, request, *args, **kwargs):
        ids = self.request.GET.get('id')
        queryset = self.model.objects.all()
        queryset= queryset.filter(id=ids)
        queryset= queryset.values('nombre', 'status', 'logo')
        data = {}
        for value in queryset:
            data = value
        return HttpResponse(json.dumps(data), status=200, content_type='application/json')



class CrearNegocio(CreateView):
    template_name = 'negocio/form.html'
    model = Negocio
    fields = ['id', 'tipo_negocio', 'nombre', 'pagina_facebook', 'status', 'logo',]
    titulo = 'Crear Negocio'

    def get_context_data(self, **kwargs):
        context = super(CrearNegocio, self).get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context["negocios"] = self.model.objects.all()
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        add = form.save(commit=False)
        response_data = {}

        if form.is_valid():
            nombre = request.FILES['logo'].name
            ruta_image = settings.MEDIA_ROOT
            archivo = ruta_image+'/logos/'+str(nombre)
            img = Image.open(request.FILES['logo'])
            img.thumbnail((480, 480), Image.ANTIALIAS)
            img.save(archivo)

            image = open(archivo)
            fot_c =  image.read()
            logo_ba = base64.encodestring(fot_c)
            add.logo_ba = logo_ba
            os.remove(archivo)
            valor = add.save()
            # Fin Guardar base64
            response_data['success'] = 'error'
            if valor != 'error':
                response_data['success'] = 'ok'

            return HttpResponse(json.dumps(response_data), status=200, content_type='application/json')


class DeleteNegocio(DeleteView):
    model = Negocio

    def delete(self, request, *args, **kwargs):

        ids = request.POST.get('id')
        negocio_obj = self.model.objects.get(id=ids)
        negocio_obj.delete()
        response_data = {}
        response_data['eliminar'] = 'ok'
        return HttpResponse(json.dumps(response_data), status=200, content_type='application/json')
