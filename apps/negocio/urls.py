from django.conf.urls import  include, url
from rest_framework.routers import DefaultRouter
from .views import CrearNegocio, ListaNegocio, DetailNegocio, DeleteNegocio
from .viewsets import NegocioViewSet

router = DefaultRouter()
router.register(r'api', NegocioViewSet)

urlpatterns = [
    url(r'^$', ListaNegocio.as_view(), name="lista_negocio"),
    url(r'^crear$', CrearNegocio.as_view(), name="crear_negocio"),
    url(r'^buscar$', DetailNegocio.as_view(), name="detail_negocio"),
    url(r'^eliminar$', DeleteNegocio.as_view(), name="eliminar_negocio"),
    url(r'^', include(router.urls)),
]
