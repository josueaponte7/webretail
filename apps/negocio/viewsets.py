from rest_framework import viewsets
from .models import Negocio
from .serializers import NegocioSerializer


class NegocioViewSet(viewsets.ModelViewSet):

    serializer_class = NegocioSerializer
    queryset = Negocio.objects.all()