from django.conf.urls import  include, url
from .views import ListarBanner, CreateBanner, DetailBanner, DeleteBanner
from rest_framework.routers import DefaultRouter
from .viewsets import BannerViewSet

router = DefaultRouter()
router.register(r'api', BannerViewSet)

urlpatterns = [
    url(r'^$', ListarBanner.as_view(), name="listar_banner"),
    url(r'^crear$', CreateBanner.as_view(), name="crear_banner"),
    url(r'^buscar$', DetailBanner.as_view(), name="detail_banner"),
    url(r'^eliminar$', DeleteBanner.as_view(), name="eliminar_banner"),
    url(r'^', include(router.urls)),
]

