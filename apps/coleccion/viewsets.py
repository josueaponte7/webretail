from rest_framework import viewsets
from .models import Coleccion
from .serializers import ColeccionSerializer


class ColeccionViewSet(viewsets.ModelViewSet):

    serializer_class = ColeccionSerializer
    queryset = Coleccion.objects.all()