from __future__ import unicode_literals

from django.apps import AppConfig


class ColeccionConfig(AppConfig):
    name = 'coleccion'
