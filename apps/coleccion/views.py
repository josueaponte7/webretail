from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView
from .models import Coleccion
import json
from django.http import HttpResponse
from django.conf import settings
import base64
from PIL import Image
import os


class ListarColeccion(ListView):

    template_name = 'coleccion/listar.html'
    model = Coleccion
    titulo = 'Listar Colecciones'

    def get_context_data(self, **kwargs):
        context = super(ListarColeccion, self).get_context_data(**kwargs)
        context["colecciones"] = self.model.objects.all()
        context['titulo'] = self.titulo
        return context


class DetailColeccion(DetailView):
    model = Coleccion

    def get(self, request, *args, **kwargs):
        ids = self.request.GET.get('id')
        queryset = self.model.objects.all()
        queryset= queryset.filter(id=ids)
        queryset= queryset.values('nombre', 'status', 'img_col')
        data = {}
        for value in queryset:
            data = value
        return HttpResponse(json.dumps(data), status=200, content_type='application/json')


class CreateColeccion(CreateView):
    template_name = 'coleccion/form.html'
    model = Coleccion
    fields = ['nombre', 'descripcion', 'status', 'negocio', 'img_col']
    titulo = 'Nueva Coleccion'

    def get_context_data(self, **kwargs):
        context = super(CreateColeccion, self).get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context["colecciones"] = self.model.objects.all()

        return context

    def post(self, request, *args, **kwargs):

        print request.POST
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        add = form.save(commit=False)
        response_data = {}

        if form.is_valid():

            nombre = request.FILES['img_col'].name
            ruta_image = settings.MEDIA_ROOT
            archivo = ruta_image+'/coleccion/'+str(nombre)
            img = Image.open(request.FILES['img_col'])
            img.thumbnail((480, 480), Image.ANTIALIAS)
            img.save(archivo)

            image = open(archivo)
            fot_c =  image.read()
            img_col_bin = base64.encodestring(fot_c)
            add.img_col_bin = img_col_bin
            os.remove(archivo)

            valor = add.save()
            response_data['success'] = 'error'
            if valor != 'error':
                response_data['success'] = 'ok'
            # return HttpResponse(json.dumps(response_data), status=204, content_type='application/json')
            return HttpResponse(json.dumps(response_data), status=200, content_type='application/json')


class DeleteColeccion(DeleteView):
    model = Coleccion

    def delete(self, request, *args, **kwargs):

        ids = request.POST.get('id')
        tienda_obj = self.model.objects.get(id=ids)
        tienda_obj.delete()
        response_data = {}
        response_data['eliminar'] = 'ok'
        return HttpResponse(json.dumps(response_data), status=200, content_type='application/json')
