from django.conf.urls import  include, url
from .views import ListarColeccion, CreateColeccion, DetailColeccion, DeleteColeccion
from rest_framework.routers import DefaultRouter
from .viewsets import ColeccionViewSet

router = DefaultRouter()
router.register(r'api', ColeccionViewSet)

urlpatterns = [
    url(r'^$', ListarColeccion.as_view(), name="listar_coleccion"),
    url(r'^crear$', CreateColeccion.as_view(), name="crear_coleccion"),
    url(r'^buscar$', DetailColeccion.as_view(), name="detail_coleccion"),
    url(r'^eliminar$', DeleteColeccion.as_view(), name="eliminar_coleccion"),
    url(r'^', include(router.urls)),
]
